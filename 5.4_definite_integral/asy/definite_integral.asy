// areas.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "definite_integral%03d";
real PI = acos(-1);

import graph;


// ==== general function ====
path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);




// ==== solar panel ====

path panel_fcn = (0,-2)..(1,-0.5)..(2,1)..(3,2.25)..(4,2.0)..(5.25,0.25);

picture pic;
int picnum = 0;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-1; ytop=3;

real a = 0; real b = 4.6;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path f = shift(0,1)*panel_fcn;
draw(pic, f, black);

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_0_time = intersect(f, (0,ybot)--(0,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path left_region = ( point(f,f_a_time)&subpath(f, f_a_time, f_0_time)&point(f,f_0_time)--(0,0)--point(f,f_a_time) )--cycle;
path right_region = ( point(f,f_0_time)&subpath(f, f_0_time, f_b_time)&point(f,f_b_time)--(b,0)--(0,0) )--cycle;
filldraw(pic, left_region, gray(0.8)+opacity(0.60), black);
filldraw(pic, right_region, gray(0.8)+opacity(0.60), black);
// draw(pic, subpath(f, f_a_time, f_b_time), green);

// // sweep left to right
// real x = a+(b-a)*(i/9);
// real f_x_time = intersect(f, (x,ybot)--(x,ytop))[0];
// path region_done = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_x_time)&point(f,f_x_time)--(x,ybot)--(a,ybot))--cycle;
// fill(pic, region_done, light_color);
// draw(pic, (x,ybot)--point(f,f_x_time), highlight_color);

real[] T10 = {b};
xaxis(pic, L="\scriptsize $t$",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T10,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$x$", b);
// labelx(pic, "$b$", b);

Label L = Label("\scriptsize kw",EndPoint,W);
yaxis(pic, L=rotate(0)*L,  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



//  ===== General function swept out =======
real a = 2; real b = 4;

for(int i=0; i < 10; ++i) {
  picture pic;
  int picnum = 1+i;
  size(pic,4cm,0,keepAspect=true);

  real xleft, xright, ybot, ytop; // limits of graph
  xleft=0; xright=5;
  ybot=0; ytop=3;

  path f = shift(0,2)*general_fcn;
  draw(pic, f, black);

  // region to be integrated  
  real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
  real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
  path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
  filldraw(pic, region, gray(0.9), black);
  // draw(pic, subpath(f, f_a_time, f_b_time), green);

  // sweep left to right
  real x = a+(b-a)*(i/9);
  real f_x_time = intersect(f, (x,ybot)--(x,ytop))[0];
  path region_done = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_x_time)&point(f,f_x_time)--(x,ybot)--(a,ybot))--cycle;
  fill(pic, region_done, light_color);
  draw(pic, (x,ybot)--point(f,f_x_time), highlight_color);

  real[] T13 = {a,b};
  xaxis(pic, L="$x$",  
	axis=YZero,
	xmin=xleft-0.75, xmax=xright+.75,
	p=currentpen,
	ticks=RightTicks("%",T13,Size=2pt),
	arrow=Arrows(TeXHead));
  labelx(pic, "$a$", a);
  labelx(pic, "$b$", b);
  
  yaxis(pic, L="$y$",  
	axis=XZero,
	ymin=ybot-0.75, ymax=ytop+0.75,
	p=currentpen,
	ticks=NoTicks,
	arrow=Arrows(TeXHead));
  
  shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
}


// ===== usual integration picture ======
picture pic;
int picnum = 11;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = shift(0,2)*general_fcn;
draw(pic, f, black);

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
filldraw(pic, region, gray(0.9), black);
// draw(pic, subpath(f, f_a_time, f_b_time), green);

// Reimann boxes
for(int i=0; i < 9; ++i) {
  real x_i = a+(b-a)*(i/9);
  real x_iplus = a+(b-a)*((i+1)/9);
  real f_x_i_time = intersect(f, (x_i,ybot)--(x_i,ytop))[0];
  real f_x_iplus_time = intersect(f, (x_iplus,ybot)--(x_iplus,ytop))[0];
  path reimann_box = ((x_i,ybot)--point(f,f_x_i_time)--(x_iplus,point(f,f_x_i_time).y)--(x_iplus,ybot)--(x_i,ybot))--cycle;
  filldraw(pic, reimann_box, light_color+opacity(0.5), highlight_color);
}

real[] T23 = {a,b};
xaxis(pic, L="$x$",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T23,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$b$", b);

yaxis(pic, L="$y$",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ===== approximate area below 1+x^2 ======
real f12(real x) {return 1+x^2;}

picture pic;
int picnum = 12;
scale(pic, Linear(3), Linear);
size(pic,0,3.5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2.5;
ybot=0; ytop=5;
real a=1; real b=2;

path f_graph = graph(pic, f12, xleft-0.1, 2+0.1);
draw(pic, f_graph, black);

// region to be integrated  
real f_a_time = intersect(f_graph, Scale(pic,(a,ybot))--Scale(pic,(a,ytop+1)))[0];
real f_b_time = intersect(f_graph, Scale(pic,(b,ybot))--Scale(pic,(b,ytop+1)))[0];
path region = (Scale(pic,(a,ybot))--point(f_graph,f_a_time)&subpath(f_graph, f_a_time, f_b_time)&point(f_graph,f_b_time)--Scale(pic,(b,ybot))--Scale(pic,(a,ybot)))--cycle;
filldraw(pic, region, gray(0.9), black);
// draw(pic, subpath(f, f_a_time, f_b_time), green);

// Reimann boxes, left side
for(int i=0; i < 4; ++i) {
  real x_i = a+(b-a)*(i/4);
  real x_iplus = a+(b-a)*((i+1)/4);
  real f_x_i_time = intersect(f_graph, Scale(pic,(x_i,ybot))--Scale(pic,(x_i,ytop+1)))[0];
  real f_x_iplus_time = intersect(f_graph, Scale(pic,(x_iplus,ybot))--Scale(pic,(x_iplus,ytop+1)))[0];
  path reimann_box = ( Scale(pic,(x_i,ybot))--point(f_graph,f_x_i_time)--Scale(pic,(x_iplus,point(f_graph,f_x_i_time).y))--Scale(pic,(x_iplus,ybot))--Scale(pic,(x_i,ybot)))--cycle;
  filldraw(pic, reimann_box, light_color+opacity(0.7), highlight_color);
}

real[] T12 = {1, 2};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%",T12,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$1$", 1);
labelx(pic, "$2$", 2);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

// ..... same, with midpoints .......
picture pic;
int picnum = 13;
scale(pic, Linear(3), Linear);
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2.5;
ybot=0; ytop=5;
real a=1; real b=2;

path f_graph = graph(pic, f12, xleft-0.1, 2+0.1);
draw(pic, f_graph, black);

// region to be integrated  
real f_a_time = intersect(f_graph, Scale(pic,(a,ybot))--Scale(pic,(a,ytop+1)))[0];
real f_b_time = intersect(f_graph, Scale(pic,(b,ybot))--Scale(pic,(b,ytop+1)))[0];
path region = (Scale(pic,(a,ybot))--point(f_graph,f_a_time)&subpath(f_graph, f_a_time, f_b_time)&point(f_graph,f_b_time)--Scale(pic,(b,ybot))--Scale(pic,(a,ybot)))--cycle;
filldraw(pic, region, gray(0.9), black);
// draw(pic, subpath(f, f_a_time, f_b_time), green);

// Reimann boxes, midpoint
for(int i=0; i < 4; ++i) {
  real x_i = a+(b-a)*(i/4);
  real x_iplus = a+(b-a)*((i+1)/4);
  real m_i = (x_i+x_iplus)/2;
  real f_x_i_time = intersect(f_graph, Scale(pic,(x_i,ybot))--Scale(pic,(x_i,ytop+1)))[0];
  real f_m_i_time = intersect(f_graph, Scale(pic,(m_i,ybot))--Scale(pic,(m_i,ytop+1)))[0];
  real f_x_iplus_time = intersect(f_graph, Scale(pic,(x_iplus,ybot))--Scale(pic,(x_iplus,ytop+1)))[0];
  path reimann_box = (Scale(pic,(x_i,ybot))--Scale(pic,(x_i,point(f_graph,f_m_i_time).y))--Scale(pic,(x_iplus,point(f_graph,f_m_i_time).y))--Scale(pic,(x_iplus,ybot))--Scale(pic,(x_i,ybot)))--cycle;
  filldraw(pic, reimann_box, light_color+opacity(0.7), highlight_color);
}

real[] T13 = {1, 2};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%",T13,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$1$", 1);
labelx(pic, "$2$", 2);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ..... same, with right endpoint .......
picture pic;
int picnum = 14;
scale(pic, Linear(3), Linear);
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2.5;
ybot=0; ytop=5;
real a=1; real b=2;

path f_graph = graph(pic, f12, xleft-0.1, 2+0.1);
draw(pic, f_graph, black);

// region to be integrated  
real f_a_time = intersect(f_graph, Scale(pic,(a,ybot))--Scale(pic,(a,ytop+1)))[0];
real f_b_time = intersect(f_graph, Scale(pic,(b,ybot))--Scale(pic,(b,ytop+1)))[0];
path region = (Scale(pic,(a,ybot))--point(f_graph,f_a_time)&subpath(f_graph, f_a_time, f_b_time)&point(f_graph,f_b_time)--Scale(pic,(b,ybot))--Scale(pic,(a,ybot)))--cycle;
filldraw(pic, region, gray(0.9), black);
// draw(pic, subpath(f, f_a_time, f_b_time), green);

// Reimann boxes, right endpoint
for(int i=0; i < 4; ++i) {
  real x_i = a+(b-a)*(i/4);
  real x_iplus = a+(b-a)*((i+1)/4);
  real m_i = x_iplus;
  real f_x_i_time = intersect(f_graph, Scale(pic,(x_i,ybot))--Scale(pic,(x_i,ytop+1)))[0];
  real f_m_i_time = intersect(f_graph, Scale(pic,(m_i,ybot))--Scale(pic,(m_i,ytop+1)))[0];
  real f_x_iplus_time = intersect(f_graph, Scale(pic,(x_iplus,ybot))--Scale(pic,(x_iplus,ytop+1)))[0];
  path reimann_box = (Scale(pic,(x_i,ybot))--Scale(pic,(x_i,point(f_graph,f_m_i_time).y))--Scale(pic,(x_iplus,point(f_graph,f_m_i_time).y))--Scale(pic,(x_iplus,ybot))--Scale(pic,(x_i,ybot)))--cycle;
  filldraw(pic, reimann_box, light_color+opacity(0.7), highlight_color);
}

real[] T13 = {1, 2};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%",T13,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$1$", 1);
labelx(pic, "$2$", 2);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ..... left side, more boxes ...............
picture pic;
int picnum = 15;
scale(pic, Linear(3), Linear);
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2.5;
ybot=0; ytop=5;
real a=1; real b=2;

path f_graph = graph(pic, f12, xleft-0.1, 2+0.1);
draw(pic, f_graph, black);

// region to be integrated  
real f_a_time = intersect(f_graph, Scale(pic,(a,ybot))--Scale(pic,(a,ytop+1)))[0];
real f_b_time = intersect(f_graph, Scale(pic,(b,ybot))--Scale(pic,(b,ytop+1)))[0];
path region = (Scale(pic,(a,ybot))--point(f_graph,f_a_time)&subpath(f_graph, f_a_time, f_b_time)&point(f_graph,f_b_time)--Scale(pic,(b,ybot))--Scale(pic,(a,ybot)))--cycle;
filldraw(pic, region, gray(0.9), black);
// draw(pic, subpath(f, f_a_time, f_b_time), green);

// Reimann boxes, left side
for(int i=0; i < 8; ++i) {
  real x_i = a+(b-a)*(i/8);
  real x_iplus = a+(b-a)*((i+1)/8);
  real f_x_i_time = intersect(f_graph, Scale(pic,(x_i,ybot))--Scale(pic,(x_i,ytop+1)))[0];
  real f_x_iplus_time = intersect(f_graph, Scale(pic,(x_iplus,ybot))--Scale(pic,(x_iplus,ytop+1)))[0];
  path reimann_box = ( Scale(pic,(x_i,ybot))--point(f_graph,f_x_i_time)--Scale(pic,(x_iplus,point(f_graph,f_x_i_time).y))--Scale(pic,(x_iplus,ybot))--Scale(pic,(x_i,ybot)))--cycle;
  filldraw(pic, reimann_box, light_color+opacity(0.7), highlight_color);
}

xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%",T12,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$1$", 1);
labelx(pic, "$2$", 2);

yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





path general_fcn = (-0.25,0)..(1,0.35)..(2,0)..(3,-0.25)..(4,0)..(5.25,0.25);
real a = 1; real b = 4;


// ==== prototype picture ====
picture pic;
int picnum = 16;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

path f = shift(0,2)*general_fcn;
draw(pic, f, black);

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
filldraw(pic, region, gray(0.9), black);

// Reimann box
real x = a+(b-a)*0.4;
real f_x_time = intersect(f, (x,ybot)--(x,ytop))[0];
draw(pic, (x,ybot)--point(f,f_x_time), highlight_color);

real[] T16 = {a,x,b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",T16,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$x$", x);
labelx(pic, "$b$", b);
  
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... integrals add ...........
picture pic;
int picnum = 17;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3.25;
ybot=0; ytop=3;

a = 0.5; b = 3;
real c = 2;

path f = shift(0,2)*subpath(general_fcn,0,3.1);
draw(pic, f, black);

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_c_time = intersect(f, (c,ybot)--(c,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region_ac = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_c_time)&point(f,f_c_time)--(c,ybot)--(a,ybot))--cycle;
filldraw(pic, region_ac, gray(0.9), black);

// Reimann box
// real x = a+(b-a)*0.7;
// real f_x_time = intersect(f, (x,ybot)--(x,ytop))[0];
// draw(pic, (x,ybot)--point(f,f_x_time), highlight_color);

real[] T16 = {a,c,b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%",T16,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$c$", c);
labelx(pic, "$b$", b);
  
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

// ....... right-hand region ...........
picture pic;
int picnum = 18;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3.25;
ybot=0; ytop=3;


path f = shift(0,2)*subpath(general_fcn,0,3.1);
draw(pic, f, black);

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_c_time = intersect(f, (c,ybot)--(c,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region_cb = ((c,ybot)--point(f,f_c_time)&subpath(f, f_c_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(c,ybot))--cycle;
filldraw(pic, region_cb, gray(0.9), black);

// Reimann box
// real x = a+(b-a)*0.7;
// real f_x_time = intersect(f, (x,ybot)--(x,ytop))[0];
// draw(pic, (x,ybot)--point(f,f_x_time), highlight_color);

real[] T16 = {a,c,b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%",T16,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$c$", c);
labelx(pic, "$b$", b);
  
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... entire region ...........
picture pic;
int picnum = 19;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3.25;
ybot=0; ytop=3;


path f = shift(0,2)*subpath(general_fcn,0,3.1);
draw(pic, f, black);

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_c_time = intersect(f, (c,ybot)--(c,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
path region_ab = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
filldraw(pic, region_ab, gray(0.9), black);

// Reimann box
// real x = a+(b-a)*0.7;
// real f_x_time = intersect(f, (x,ybot)--(x,ytop))[0];
// draw(pic, (x,ybot)--point(f,f_x_time), highlight_color);

real[] T16 = {a,c,b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%",T16,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$c$", c);
labelx(pic, "$b$", b);
  
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));


// ========== sum of two =================
path second_fcn = (-0.25,1)..(1,0.5)..(2,1)..(3,1.25)..(4,2)..(5.25,1.25);
path sum_fcn = (point(general_fcn,0)+point(second_fcn,0))..
  (point(general_fcn,1)+point(second_fcn,1))..
  (point(general_fcn,2)+point(second_fcn,2))..
  (point(general_fcn,3)+point(second_fcn,3))..
  (point(general_fcn,4)+point(second_fcn,4))..
  (point(general_fcn,5)+point(second_fcn,5));
  
real a = 0.25; real b = 3;

picture pic;
int picnum = 20;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3.25;
ybot=0; ytop=3;

path f = shift(0,0.5)*subpath(general_fcn,0,3.1);
draw(pic, f, black);
label(pic,"$f$",shift(0,0.5)*point(general_fcn,3.1), E);
path g = shift(0,1)*subpath(second_fcn,0,3.1);
draw(pic, g, black);
label(pic,"$g$",shift(0,1)*point(second_fcn,3.1), SE);
path sum = shift(0,1.5)*subpath(sum_fcn,0.11,1.55);
draw(pic, sum, highlight_color);
label(pic,"$f+g$",shift(0,1.5)*point(sum_fcn,1.55), NE);

// region to be integrated  
real f_a_time = intersect(f, (a,ybot-1)--(a,ytop+1))[0];
real f_b_time = intersect(f, (b,ybot-1)--(b,ytop+1))[0];
path region_f = ((a,ybot)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,ybot)--(a,ybot))--cycle;
filldraw(pic, region_f, gray(0.6)+opacity(0.5), black);
real g_a_time = intersect(g, (a,ybot-1)--(a,ytop+1))[0];
real g_b_time = intersect(g, (b,ybot-1)--(b,ytop+1))[0];
path region_g = ((a,ybot)--point(g,g_a_time)&subpath(g, g_a_time, g_b_time)&point(g,g_b_time)--(b,ybot)--(a,ybot))--cycle;
filldraw(pic, region_g, gray(0.6)+opacity(0.5), black);
real sum_a_time = intersect(sum, (a,ybot-1)--(a,ytop+1))[0];
real sum_b_time = intersect(sum, (b,ybot-1)--(b,ytop+1))[0];
path region_sum = ((a,ybot)--point(sum,sum_a_time)&subpath(sum, sum_a_time, sum_b_time)&point(sum,sum_b_time)--(b,ybot)--(a,ybot))--cycle;
filldraw(pic, region_sum, gray(0.6)+opacity(0.5), black);
draw(pic, sum, highlight_color);

real[] T20 = {a,b};
xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+.25,
      p=currentpen,
      ticks=RightTicks("%",T20,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", a);
labelx(pic, "$b$", b);
  
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ========== signed areas =================
path wiggle_fcn = (-0.5,1){SE}..(1,-1.75)..(2,-0.25)..(3,1.25)..(4,0.5)..
  (5,-1.5)..(6,0)..(7,1);
  
real a = 0.25; real b = 3;

picture pic;
int picnum = 21;
size(pic,7cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=7;
ybot=-1; ytop=2;

path f = shift(0,0.5)*subpath(wiggle_fcn,0,7);
draw(pic, f, black);

pen region_fillpen = gray(0.6)+opacity(0.5);

real[] zero_times = times(f,(0,0));

path region0_pth = ( (point(f,0).x,0)--point(f,0)&subpath(f,0,zero_times[0])&point(f,zero_times[0])--(point(f,0).x,0) )--cycle;
fill(pic, region0_pth, region_fillpen);
label(pic,"{\scriptsize $A$}",(point(f,0).x+0.15,0.5), filltype=Fill(white));
// next region
path region1_pth = ( point(f,zero_times[0])--point(f,zero_times[1])&subpath(f,zero_times[1],zero_times[0])&point(f,zero_times[0]) )--cycle;
fill(pic, region1_pth, region_fillpen);
label(pic,"{\scriptsize $B$}",((point(f,zero_times[0]).x+point(f,zero_times[1]).x)/2,-0.5), filltype=Fill(white));
// next region
path region2_pth = ( subpath(f,zero_times[1],zero_times[2])&point(f,zero_times[2])--point(f,zero_times[1]) )--cycle;
fill(pic, region2_pth, region_fillpen);
label(pic,"{\scriptsize $C$}",((point(f,zero_times[1]).x+point(f,zero_times[2]).x)/2,0.5), filltype=Fill(white));
// next region
path region3_pth = ( point(f,zero_times[2])--point(f,zero_times[3])&subpath(f,zero_times[3],zero_times[2])&point(f,zero_times[2]) )--cycle;
fill(pic, region3_pth, region_fillpen);
label(pic,"{\scriptsize $D$}",((point(f,zero_times[2]).x+point(f,zero_times[3]).x)/2,-0.5), filltype=Fill(white));
// next region
path region4_pth = ( subpath(f,zero_times[3],7)&point(f,7)--(point(f,7).x,0)--point(f,zero_times[3]) )--cycle;
fill(pic, region4_pth, region_fillpen);
label(pic,"{\scriptsize $E$}",(point(f,7).x-0.15,0.5), filltype=Fill(white));

real[] T21 = {point(f,0).x,
  point(f,zero_times[0]).x,
  point(f,zero_times[1]).x,
  point(f,zero_times[2]).x,
  point(f,zero_times[3]).x,
  point(f,7).x};

xaxis(pic, L="",  
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.35,
      p=currentpen,
      ticks=RightTicks("%",T21,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$a$", point(f,0).x, highlight_color);
labelx(pic, "$b$", point(f,zero_times[0]).x, SW, highlight_color);
labelx(pic, "$c$", point(f,zero_times[1]).x, SE, highlight_color);
labelx(pic, "$d$", point(f,zero_times[2]).x, SW, highlight_color);
labelx(pic, "$e$", point(f,zero_times[3]).x, SE, highlight_color);
labelx(pic, "$f$", point(f,7).x, highlight_color);
  
yaxis(pic, L="",  
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ===== area below x^2+1 ======
real f22(real x) {return 1+x^2;}  // same as f12

picture pic;
int picnum = 22;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=6;

real a = 1; real b = 2;
real x = a+0.681*(b-a);

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path f = graph(pic, f22, a-0.1, b+0.1);
draw(pic, f, black);

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
real f_x_time = intersect(f, (x,ybot)--(x,ytop))[0];
path region = ( (a,0)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,0)--(a,0) )--cycle;
filldraw(pic, region, gray(0.85)+opacity(0.5), black);
draw(pic, (x,0)--point(f,f_x_time), highlight_color);

real[] T22 = {a, b};
xaxis(pic, L="\scriptsize $x$",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks("%",T22,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$1$", a);
labelx(pic, "$2$", b);

yaxis(pic, L="\scriptsize $y$",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ===== area below e^x ======
real f23(real x) {return exp(x);}  

picture pic;
int picnum = 23;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=1;
ybot=0; ytop=3;

real a = -1; real b = 1;
real x = a+0.681*(b-a);

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path f = graph(pic, f23, a-0.1, b+0.1);
draw(pic, f, black);

// region to be integrated  
real f_a_time = intersect(f, (a,ybot)--(a,ytop))[0];
real f_b_time = intersect(f, (b,ybot)--(b,ytop))[0];
real f_x_time = intersect(f, (x,ybot)--(x,ytop))[0];
path region = ( (a,0)--point(f,f_a_time)&subpath(f, f_a_time, f_b_time)&point(f,f_b_time)--(b,0)--(a,0) )--cycle;
filldraw(pic, region, gray(0.85)+opacity(0.5), black);
draw(pic, (x,0)--point(f,f_x_time), highlight_color);

real[] T23 = {a, b};
xaxis(pic, L="\scriptsize $x$",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks("%",T23,Size=2pt),
      arrow=Arrows(TeXHead));
labelx(pic, "$-1$", a);
labelx(pic, "$1$", b);

yaxis(pic, L="\scriptsize $y$",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ===== area below 1-x^2 ======
real f24(real x) {return 1-x^2;}  

picture pic;
int picnum = 24;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=-3; ytop=1;

real a = 0; real b = 2;
real x0 = a+0.33*(b-a);
real x1 = a+0.75*(b-a);

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path f = graph(pic, f24, a-0.1, b+0.1);
draw(pic, f, black);

// regions to be integrated  
real f_a_time = intersect(f, (a,ybot-1)--(a,ytop+1))[0];
real f_1_time = intersect(f, (1,ybot-1)--(1,ytop+1))[0];
real f_b_time = intersect(f, (b,ybot-1)--(b,ytop+1))[0];
real f_x0_time = intersect(f, (x0,ybot)--(x0,ytop))[0];
real f_x1_time = intersect(f, (x1,ybot)--(x1,ytop))[0];
path region_left = ( (a,0)--point(f,f_a_time)&subpath(f, f_a_time, f_1_time)&point(f,f_1_time)--(1,0)--(a,0) )--cycle;
filldraw(pic, region_left, gray(0.85)+opacity(0.5), black);
path region_right = ( (1,0)--point(f,f_1_time)&subpath(f, f_1_time, f_b_time)&point(f,f_b_time)--(b,0)--(1,0) )--cycle;
filldraw(pic, region_right, gray(0.85)+opacity(0.5), black);
draw(pic, (x0,0)--point(f,f_x0_time), highlight_color);
draw(pic, (x1,0)--point(f,f_x1_time), highlight_color);

real[] T24 = {b};
xaxis(pic, L="\scriptsize $x$",  
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=RightTicks("%",T24,Size=2pt),
      arrow=Arrows(TeXHead));
// labelx(pic, "$-1$", a);
// labelx(pic, "$2$", b);

yaxis(pic, L="\scriptsize $y$",  
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");







// // ==== local and absolute max ====
// picture pic;
// int picnum = 0;
// size(pic,0,4cm,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=5;
// ybot=0; ytop=5;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// path grph = (xleft-0.1,1){N}..(2,ytop){E}..(3.5,ytop-2){E}
// ..(4,ytop-1.5){E}..(xright+0.1,2){SE};


// draw(pic, grph, highlight_color);

// filldraw(pic, circle((2,ytop),0.05), highlight_color,  highlight_color);
// label(pic,  "$A$", (2,ytop), S, filltype=Fill(white));
// filldraw(pic, circle((3.5,ytop-2),0.05), highlight_color,  highlight_color);
// label(pic,  "$B$", (3.5,ytop-2), S, filltype=Fill(white));
// filldraw(pic, circle((4,ytop-1.5),0.05), highlight_color,  highlight_color);
// label(pic,  "$C$", (4,ytop-1.5), N, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// // ==== parabola ====
// real f1(real x) {return x^2;}

// picture pic;
// int picnum = 1;
// size(pic,4cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=-2; xright=2;
// ybot=0; ytop=4;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// draw(pic, graph(f1,xleft-0.1,xright+0.1), highlight_color);

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// // label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=x^2$", (0,3), W, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

