\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 5.1\ \ Antiderivatives}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................

\begin{frame}{Definition of antiderivative}

\Df
Given a function~$f$, then the function $F$ is an \alert{antiderivative}
of $f$ if $F'(x)=f(x)$ for all~$x$.

\Ex
Let $f(x)=x^2$.
Then $F(x)=(1/3)x^3$ is an antiderivative, because its derivative is $x^2$.

\pause
Note that $F_1(x)=(1/3)x^3+1$ is also an antiderivative.
So is $F_2(x)=(1/3)x^3+2$, etc.

% \pause
% \Tm
% Two functions $F(x)$ and~$G(x)$ have the same derivative on an open interval
% if and only if they differ by a constant, $F(x)-G(x)=C$ for some 
% number~$C$. 
\end{frame}


\begin{frame}
\Ex
Find all antiderivatives of $f(x)=x$.

\pause
To have a derivative of~$x$ the function must be some type of squaring function.
But the derivative of $x^2$ is $2x$, not~$x$.
So we need $F_1(x)=(1/2)x^2+1$, etc.  

The family of all antiderivatives is $(1/2)x^2+C$.

\pause\medskip
Ex Find all antiderivatives. 
\begin{enumerate}
\item $3x$
\pause\item $x^3$
% \pause\item $2x^2$
\pause\item $-5x^4$
\pause\item $x+x^2$
% \pause\item $x-x^2$
\pause\item $(1/3)x^5$
% \pause\item $2x^3$
\pause\item $-x^2-x+3$
\pause\item $x^3-e^x+5$
\end{enumerate}
\end{frame}

\begin{frame}{What's the point?}

Antiderivatives have two purposes.
First, in practice we very often need to solve equations like the last few,
called \alert{differential equations}, to describe systems.
Second, we will want to find an analog to addition that is continuous,
and to solve these problems requires antiderivative, as is described 
by the Fundamental Theorem of Calculus.
\end{frame}


\begin{frame}{Formulas}
The instruction ``find the family of all antiderivatives of $f$'' is expressed
with the \alert{indefinite integral}.
\begin{equation*}
  \int f(x)\,dx
\end{equation*}
These are the basic antiderivatives.
\begin{enumerate}
\item $\displaystyle \int x^n\,dx = \frac{1}{n+1}x^{n+1}+C$
\item $\displaystyle \int e^x\,dx=e^x+C$
\item $\displaystyle \int\frac{1}{x}\,dx=\ln|x|+C$
\end{enumerate}
All derivative rules reverse to be antiderivative rules.
\begin{enumerate}\setcounter{enumi}{3}
\item $\displaystyle \int k\cdot f(x)\,dx=k\cdot \int f(x)\,dx$
\item $\displaystyle \int f(x)+g(x)\,dx=\int f(x)\,dx+\int g(x)\,dx$
\item $\displaystyle \int f(x)-g(x)\,dx=\int f(x)\,dx-\int g(x)\,dx$
\end{enumerate}
\end{frame}


\begin{frame}{Warnings}
\begin{enumerate}
\item Just as with derivatives, you have to be careful about products.
\begin{equation*}
  \int x\cdot x\,dx\neq \int x\,dx\cdot\int x\,dx    
\end{equation*}
\item Just as with derivatives, you have to be careful about quotients.
\begin{equation*}
  \int \frac{x^2}{x}\,dx\neq \frac{\int x^2\,dx}{\int x\,dx}    
\end{equation*}
\item Just as with derivatives, you have to be careful about composition.
%   For example, where $f(x)=x^3$ and $g(x)=x^2$ the integral of the
% composition is not equal to the composition of the integrals.
\end{enumerate}
There are separate and useful antiderivative rules for these but the above
do not work.
\end{frame}



\begin{frame}{Practice}
\begin{enumerate}
\item $\displaystyle \int 10\,dx$
\pause\item $\displaystyle \int 14x\,dx$
\pause\item $\displaystyle \int 15x^2\,dx$
\pause\item $\displaystyle \int x^8\,dx$
\pause\item $\displaystyle \int x^{-4}\,dx$
\pause\item $\displaystyle \int x^{-1}\,dx$
\pause\item $\displaystyle \int 5e^u\,du$
\pause\item $\displaystyle \int t^2(1+t^3)\,dt$
\pause\item $\displaystyle \int \frac{dt}{\sqrt[3]{t}}$
\end{enumerate}
\end{frame}



\begin{frame}{Practice}
\begin{enumerate}
\item $\displaystyle \int \frac{6}{m^2}\,dm$
\pause\item $\displaystyle \int \frac{1-y^2}{3y}\,dy$
\pause\item $\displaystyle \int \frac{e^t-t}{2}\,dx$
\pause\item $\displaystyle \int 4t^3+\frac{2}{t^3}\,dt$
\end{enumerate}
\end{frame}


\begin{frame}{Finding $C$}
\Ex Find the antiderivative of $R(x)=500-0.4x$ subject to $R(0)=12$.

\pause
First find the family of antiderivatives
\begin{equation*}
  \int 500-0.4x\,dx=500x-0.2x^2+C
\end{equation*}
and second find the value of the constant:
\begin{equation*}
  12=500\cdot(0)-0.2\cdot (0^2)+C
\end{equation*}
gives $C=12$.

\medskip
\begin{enumerate}
\item Solve $dR/dt=50/t^3$ subject to $R(1)=50$.

\pause\item Solve $df/dx=x^{-1}-2x^{-2}+1$ subject to $f(1)=5$.
\end{enumerate}
\end{frame}





% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
