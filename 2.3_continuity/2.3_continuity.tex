\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title[Continuity] % (optional, use only with long paper titles)
{Section 2.3\ \ Continuity}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\subject{Continuity}
% This is only inserted into the PDF information catalog. Can be left
% out. 

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}
Continuity is an important principle in applications as well as in mathematics.
This is a graph of the temperature at various heights above the earth.
\begin{center}
  \includegraphics[height=0.5\textheight]{pix/atmosphere_1962.png}
\end{center}
Imagine that it had a discontinuity.
You'd say that can't be right.
\end{frame}


% \begin{frame}
% This illustrates continuity in Mathematics.
% \begin{center}
%   \includegraphics{asy/continuity000.pdf}
% \end{center}
% These high-wire walkers each want to get to the other's end.
% It doesn't matter if one walks fast and the other slow, 
% or even if one stops from time to time\Dash
% because of continuity, they will meet.
% It cannot happen that, say, the walker on the left somehow
% jumps from $x=2$ to $x=3$.
% \end{frame}



% \begin{frame}{Function with a discontinuity}
% Sometimes to understand what something is, it helps to see a counter example.
% \begin{center}
%   \vcenteredhbox{\includegraphics{asy/continuity001.pdf}}
%   \qquad
%   \begin{minipage}{0.3\linewidth}
%     \begin{equation*}
%       f(x)=
%       \begin{cases}
%         x^2  &\text{if $x\neq 1$}  \\
%         2    &\text{if $x=1$}
%       \end{cases}
%     \end{equation*}
%   \end{minipage}
% \end{center}
% Here, as a person on the $x$-axis walks toward~$a=1$ they are surprised
% when $f(1)$ is not equal to $1$, but instead $f(1)=2$.
% \end{frame}

\begin{frame}{Definition}
The \alert{function $f$ is continuous at $a$} if
\begin{equation*}
  \lim_{x\to a}f(x)\text{\ equals\ }f(a)
\end{equation*}
(where $f$ is defined near~$a$).
A function is continuous on an interval if it is continuous at each point 
in that interval.
\end{frame}


\begin{frame}{Example}
The function $g(x)=2-(x^2/2)$ is continuous at $a=0$.  
\begin{center}
  \vcenteredhbox{\includegraphics{asy/continuity002.pdf}}
\end{center}
From the graph it is clear that $\lim_{x\to 0}g(x)=2$ and
that $g(0)=2$.

In fact, this function is continuous on the entire real line.
\end{frame}


\begin{frame}{Some exercises}
Find the inputs (if any) where each function is discontinuous.
\begin{enumerate}
\item $f(x)=2/(x-1)$
\pause\item $x/(x^2-1)$
% \pause\item $\displaystyle\frac{x+1}{x^2-2x-3}$
\pause\item $\sqrt{x-3}$
\pause\item $x^3-2x$
% \pause\item $\sqrt{(x-1)(x-3)}$
\pause\item $\displaystyle f(x)=
\begin{cases}
  x-1  &\text{if $x<2$} \\
  x^2  &\text{if $x\geq 2$}
\end{cases}$
\pause\item $e^x$ (\textit{Hint.} See the next slide.)
\end{enumerate}
\end{frame}


\begin{frame}
This is a graph of $y=e^x$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/continuity007.pdf}}
\end{center}
\end{frame}




\begin{frame}{Kinds of discontinuities}
This function 
\begin{center}
  \vcenteredhbox{\includegraphics{asy/continuity001.pdf}}
  \qquad
  \begin{minipage}{0.3\linewidth}
    \begin{equation*}
      f(x)=
      \begin{cases}
        x^2  &\text{if $x\neq 1$}  \\
        2    &\text{if $x=1$}
      \end{cases}
    \end{equation*}
  \end{minipage}
\end{center}
has a \alert{removable discontinuity}.
We can make it continuous by redefining $f(1)$ to be~$1$.
\end{frame}


\begin{frame}{More kinds}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/continuity003.pdf}}
\end{center}
Is this function continuous at:
$x=0$?
\pause $x=1$?
\pause $x=2$?
\pause $x=3$?
\pause $x=4$?
\pause $x=5$?
\pause (Ans: Yes, no, yes, no, yes, yes.)

\only<6->{
At $x=1$ the function has a \alert{jump discontinuity} while at 
$x=3$ it has an \alert{infinite discontinuity}.}
\end{frame}


\begin{frame}{Continuity at an endpoint}
This is the function of $x$ whose value is the greatest integer less than or
equal to~$x$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/continuity004.pdf}}
\end{center}

A function $f$ is \alert{continuous from the right} at~$a$ if 
$\lim_{x\to a^+}f(x)=f(a)$.
It is \alert{continuous from the left} at~$a$ if 
$\lim_{x\to a^-}f(x)=f(a)$.
\end{frame}


\begin{frame}
At $a=2$ the function $f(x)=\sqrt{x-2}$
\begin{center}
  \vcenteredhbox{\includegraphics{asy/continuity005.pdf}}
\end{center}
is continuous from the right but not from the left.
\end{frame}


\begin{frame}{Facts about continuity}
\begin{enumerate}
\item
Both of these types of function are continuous at all inputs:
constant functions, 
  polynomial functions.
\item
Each of these types is continuous at every point where it is
defined: 
  rational functions,
  root functions (sometimes continuous from one side only at some points),
  % trigonometric function,
  % inverse trigonometric function,
  exponential functions,
  logarithmic functions.
\pause\item
If $f$ and~$g$ are continuous at~$a$ and $k$ is a constant 
then these functions are continuous at $a$: 
a multiple $k\cdot f$, 
a sum $f+g$, 
a difference $f-g$, 
the product $f\cdot g$,
the quotient $f/g$ (where $g(a)\neq 0$).
\pause\item
As to function composition, if $g$ is continuous at~$a$ and $f$ is continuous at
$f(a)$ then the composition function $f\circ g\, (x)=f(g(x))$ is continuous 
at $a$.
\end{enumerate}

For example, $(3x-5)^2$ is continuous on the entire real line because
it is the composition of $f(x)=x^5$ and $g(x)=3x-5$. 
Similarly, $h(x)=e^{5x}$ is also continuous on the entire real line.
And $\sqrt{1+x}$ is continuous where $x>-1$
(and continuous from the right at $x=-1$).
\end{frame}


\section{Sign charts}
\begin{frame}{Sign charts}
The function $f(x)=2x+1$ has a value of zero when 
$0=2x+1$, which gives $x=-1/2$.  

Trying a number on the left of $-1/2$ and also one on the right
shows that the function is 
negative to the left and positive to the right.
A \alert{sign chart} marks the function's zero's on the line, and then puts in 
$+$'s and $-$'s above the intervals.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/continuity008.pdf}}
\end{center}

\pause
\Ex
Next consider $f(x)=x^2-1$.
Factoring gives $y=(x+1)(x-1)$
and $0=(x+1)(x-1)$ gives $x=-1$ and $x=1$.
\pause
There are three regions to do, shown below.
Pick a point in each region, such as $-2$, $0$, and $2$, and plug into 
the function to get the signs.
\begin{center}
  \only<3>{\vcenteredhbox{\includegraphics{asy/continuity009.pdf}}}%
  \only<4->{\vcenteredhbox{\includegraphics{asy/continuity010.pdf}}}%
\end{center}
\end{frame}


\begin{frame}{To make a sign chart}
\begin{itemize}
\item Find all \alert{partition numbers}, $x$'s where $f(x)=0$ or 
$f(x)$ is undefined.
Put them on the number line.
That gives some intervals.
\item For each interval pick a test number and plug it into the function.
\item If the result is positive then write $+$'s, while if it is negative then
write $-$'s.
\end{itemize}

\pause
Make a sign chart for each.
\begin{enumerate}
\item $\displaystyle x^2-2x-8$
\pause \ Zeros at $x=-2,4$
\begin{center}
  \includegraphics{asy/continuity011.pdf}
\end{center}
% \item $\displaystyle x^2+7x+10$
% \pause \ Zeros at $x=-5, -2$
\item $\displaystyle \frac{2x+7}{5x-1}$
\pause \ Zero at $-7/2$, undefined at $1/5$
\begin{center}
  \includegraphics{asy/continuity012.pdf}
\end{center}
% \item $\displaystyle \frac{x^2+4}{x^2-9}$
% \pause \ No zeros, undefined at $x=+3,-3$
\end{enumerate}
\end{frame}



% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
