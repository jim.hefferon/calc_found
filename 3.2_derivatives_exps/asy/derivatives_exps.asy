// derivatives_exps.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "derivatives_exps%03d";

import graph;



// ================ y=e^x =======
real f0(real x) {return exp(x);}

picture pic;
int picnum = 0;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=2;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f0,xleft-0.1,1.61), highlight_color);
// sage: n(ln(5))
// 1.60943791243410
label(pic, "$f(x)=e^x$", (1,2), 4*W, filltype=Fill(white));


xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ y=ln x =======
real f1(real x) {return log(x);}

picture pic;
int picnum = 1;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-3; ytop=2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f1,0.05,xright+0.1), highlight_color);
label(pic, "$f(x)=\ln(x)$", (1,-1), SE, filltype=Fill(white));

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ y=e^x with tangent lines =======
real f2(real x) {return exp(x);}
real tangent_line2a(real x) {return exp(1)*(x-1)+exp(1);}
real tangent_line2b(real x) {return exp(-2)*(x+2)+exp(-2);}

picture pic;
int picnum = 2;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=2;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f2,xleft-0.1,1.61), black);
// sage: n(ln(5))
// 1.60943791243410
draw(pic, graph(tangent_line2a,1-0.5,1+0.5), highlight_color);
filldraw(pic,circle((1,exp(1)),0.05),highlight_color,highlight_color);
draw(pic, graph(tangent_line2b,-2-1,-2+1), highlight_color);
filldraw(pic,circle((-2,exp(-2)),0.05),highlight_color,highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ y=ln x with tangent lines =======
real f3(real x) {return log(x);}
real tangent_line3a(real x) {return (1/1)*(x-1)+log(1);}
// real tangent_line3b(real x) {return (1/-2)*(x+2)+log(-2);}

picture pic;
int picnum = 3;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=-3; ytop=2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f3,xleft+0.05,xright+0.1), black);
// sage: n(ln(5))
// 1.60943791243410
draw(pic, graph(tangent_line3a,1-0.5,1+0.5), highlight_color);
filldraw(pic,circle((1,log(1)),0.05),highlight_color,highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

