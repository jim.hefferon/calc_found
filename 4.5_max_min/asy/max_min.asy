// max_min.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "max_min%03d";

import graph;


// ==== local and absolute max ====
picture pic;
int picnum = 0;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path grph = (xleft-0.1,1){N}..(2,ytop){E}..(3.5,ytop-2){E}
..(4,ytop-1.5){E}..(xright+0.1,2){SE};


draw(pic, grph, highlight_color);

filldraw(pic, circle((2,ytop),0.05), highlight_color,  highlight_color);
label(pic,  "$A$", (2,ytop), S, filltype=Fill(white));
filldraw(pic, circle((3.5,ytop-2),0.05), highlight_color,  highlight_color);
label(pic,  "$B$", (3.5,ytop-2), S, filltype=Fill(white));
filldraw(pic, circle((4,ytop-1.5),0.05), highlight_color,  highlight_color);
label(pic,  "$C$", (4,ytop-1.5), N, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ==== parabola ====
real f1(real x) {return x^2;}

picture pic;
int picnum = 1;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=0; ytop=4;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f1,xleft-0.1,xright+0.1), highlight_color);

filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
label(pic,  "$f(x)=x^2$", (0,3), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ==== absolute value ====
real f2(real x) {return abs(x);}

picture pic;
int picnum = 2;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=0; ytop=4;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f2,xleft-0.1,xright+0.1), highlight_color);

filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
label(pic,  "$f(x)=|x|$", (0,3), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// // ==== non-optima at critical point ====

// // ....... max ............
// picture pic;
// int picnum = 3;
// size(pic,3cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=2;
// ybot=0; ytop=2;

// // Draw graph paper
// // for(real i=xleft; i <= xright; ++i) {
// //   if (abs(i)>.01) { 
// //     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
// //   }
// // }
// // for(real j=ybot; j <= ytop; ++j) {
// //   if (abs(j)>.01) { 
// //     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
// //   }
// // }

// path grf = (0.5,0.5){NE}::(1,1.5){E}::{SE}(1.5,0.5);

// draw(pic, grf, highlight_color);

// filldraw(pic, circle((1,1.5),0.05), highlight_color,  highlight_color);
// label(pic,  "max", (1,1.5), 2*N, filltype=Fill(white));

// real[] T3 = {1};

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.25, xmax=xright+.25,
//   p=currentpen,
//       ticks=RightTicks("%", T3, Size=2pt),
//   arrow=Arrows(TeXHead));
// labelx(pic,"$c$",1);

// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.25, ymax=ytop+0.25,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// // ....... undefined ............
// picture pic;
// int picnum = 4;
// size(pic,3cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=2;
// ybot=0; ytop=2;

// // Draw graph paper
// // for(real i=xleft; i <= xright; ++i) {
// //   if (abs(i)>.01) { 
// //     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
// //   }
// // }
// // for(real j=ybot; j <= ytop; ++j) {
// //   if (abs(j)>.01) { 
// //     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
// //   }
// // }

// path grf = (0.5,0.5){NE}::(1,1.5){NE}&(1,1.5)::{SE}(1.5,0.5);

// draw(pic, grf, highlight_color);

// filldraw(pic, circle((1,1.5),0.05), highlight_color,  highlight_color);
// label(pic,  "max", (1,1.5), 2*N, filltype=Fill(white));

// real[] T3 = {1};

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.25, xmax=xright+.25,
//   p=currentpen,
//       ticks=RightTicks("%", T3, Size=2pt),
//   arrow=Arrows(TeXHead));
// labelx(pic,"$c$",1);

// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.25, ymax=ytop+0.25,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// // ....... inflection ............
// picture pic;
// int picnum = 5;
// size(pic,3cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=2;
// ybot=0; ytop=2;

// // Draw graph paper
// // for(real i=xleft; i <= xright; ++i) {
// //   if (abs(i)>.01) { 
// //     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
// //   }
// // }
// // for(real j=ybot; j <= ytop; ++j) {
// //   if (abs(j)>.01) { 
// //     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
// //   }
// // }

// path grf = (0.5,0.5){NE}..(1,1){E}..{NE}(1.5,1.5);

// draw(pic, grf, highlight_color);

// filldraw(pic, circle((1,1),0.02), highlight_color,  highlight_color);
// draw(pic,(0.7,1)--(1.3,1),dotted);
// label(pic,  "inflection", (1,1), 2*NW, filltype=Fill(white));

// real[] T3 = {1};

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.25, xmax=xright+.25,
//   p=currentpen,
//       ticks=RightTicks("%", T3, Size=2pt),
//   arrow=Arrows(TeXHead));
// labelx(pic,"$c$",1);

// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.25, ymax=ytop+0.25,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// // ....... f'(c) undefined ............
// picture pic;
// int picnum = 6;
// size(pic,3cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=2;
// ybot=0; ytop=2;

// // Draw graph paper
// // for(real i=xleft; i <= xright; ++i) {
// //   if (abs(i)>.01) { 
// //     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
// //   }
// // }
// // for(real j=ybot; j <= ytop; ++j) {
// //   if (abs(j)>.01) { 
// //     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
// //   }
// // }

// path grf = (0.5,0.5){E}..(1,1){N}..{E}(1.5,1.5);

// draw(pic, grf, highlight_color);

// filldraw(pic, circle((1,1),0.02), highlight_color,  highlight_color);
// draw(pic,(1,0.7)--(1,1.3),dotted);
// // label(pic,  "$f'(c)$ undefined", (1,1), 2*W, filltype=Fill(white));

// real[] T3 = {1};

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.25, xmax=xright+.25,
//   p=currentpen,
//       ticks=RightTicks("%", T3, Size=2pt),
//   arrow=Arrows(TeXHead));
// labelx(pic,"$c$",1);

// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.25, ymax=ytop+0.25,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ==== x^3 ====
real f7(real x) {return x^3;}

picture pic;
int picnum = 7;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-8; ytop=8;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f7,xleft-0.02,xright+0.02), highlight_color);

filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=|x|$", (0,3), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== (x-1)x(x+1) ====
real f8(real x) {return (x-1)*x*(x+1);}

picture pic;
int picnum = 8;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-6; ytop=6;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f8,xleft-0.02,xright+0.02), highlight_color);

filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=|x|$", (0,3), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== 1/x ====
real f9(real x) {return 1/x;}

picture pic;
int picnum = 9;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=10;
ybot=0; ytop=10;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f9,xleft+0.1,xright+0.02), highlight_color);

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=|x|$", (0,3), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== 1/(x^2+1) ====
real f10(real x) {return 1/(x^2+1);}

picture pic;
int picnum = 10;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-5; xright=5;
ybot=0; ytop=2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f10,xleft-0.1,xright+0.1), highlight_color);

filldraw(pic, circle((0,1),0.05), highlight_color,  highlight_color);
label(pic,  "max", (0,1), N, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+1.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== optimize -x^2+3x-2 ====
real f11(real x) {return -x^2+3*x-2;}

picture pic;
int picnum = 11;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=-2; ytop=1;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f11,1,3), highlight_color);

filldraw(pic, circle((1,0),0.05), highlight_color,  highlight_color);
filldraw(pic, circle((3,-2),0.05), highlight_color,  highlight_color);
filldraw(pic, circle((3/2,1/4),0.05), highlight_color,  highlight_color);
// label(pic,  "max", (1,0), N, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== optimize -x^2+3x-2 ====
real f12(real x) {return x^2-3*x^(2/3);}

picture pic;
int picnum = 12;
size(pic,4cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=-2; ytop=1;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f12,0,2), highlight_color);

filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
filldraw(pic, circle((2,f12(2)),0.05), highlight_color,  highlight_color);
filldraw(pic, circle((1,-2),0.05), highlight_color,  highlight_color);
// label(pic,  "max", (1,0), N, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");






// === baseball ======

// picture pic;
// int picnum = 13;
// size(pic,0,5cm,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=10;
// ybot=0; ytop=5;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// label(pic,graphic("batter.eps","scale=0.1"), (0.5,0));
// label(pic,graphic("fielder.eps","scale=0.1"), (9.5,0));

// // dot(pic,(1,0.75),red);
// path grph = (1,0.75)..(4,4)..(5,4.4){E}..(6,4)..(9,0.75);
// draw(pic, grph, highlight_color);

// filldraw(pic, circle((5,4.4),0.05), highlight_color,  highlight_color);

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// === rain gutter ======
real PI = acos(-1);

// // ...... dimensions ..........
// picture pic;
// int picnum = 14;
// size(pic,5cm,0,keepAspect=true);

// real theta = PI/3;
// pair left_top = (-4*cos(theta), 4*sin(theta));
// pair left_mid = (0,0);
// pair right_mid = (4,0);
// pair right_top = right_mid+(4*cos(theta),4*sin(theta));
// path gutter = left_top--left_mid--right_mid--right_top;

// fill(pic, left_top--left_mid--right_mid--right_top--cycle, light_color+white);
// draw(pic, gutter, black);

// path left_gutter = left_top--left_mid;
// path mid_gutter = left_mid--right_mid;
// path right_gutter = right_mid--right_top;

// // find slopes, so can mark the dimensions
// pair dir_left = dir(left_gutter);
// pair dir_right = dir(right_gutter);

// real bar_length = 2pt;
// real offset = 0.5;

// path left_dimension = shift(offset*(-1*dir_left.y,dir_left.x))*left_gutter;
// draw(pic, "$4$", left_dimension, E, highlight_color, bar=Bars(bar_length));
// path mid_dimension = shift(0,-offset)*mid_gutter;
// draw(pic, "$4$", mid_dimension, highlight_color, bar=Bars(bar_length));
// // path right_dimension = shift(offset*(-1*dir_right.y,dir_right.x))*right_gutter;
// // draw(pic, "$4$", right_dimension, W, highlight_color, bar=Bars(bar_length));

// // thetas
// draw(pic, left_mid--(left_mid.x-1.25,left_mid.y), highlight_color+dashed);
// label(pic,"$\theta$", left_mid+(-0.75,0.5), highlight_color);
// path angle_line = left_mid--(left_mid.x-1.25,left_mid.y);
// draw(pic, angle_line, highlight_color+dashed);
// path left_mid_circle = circle(left_mid,0.5);
// // draw(pic, left_mid_circle, highlight_color);
// real angle_arc_left_gutter_time = intersect(left_mid_circle,left_gutter)[0];
// real angle_arc_angle_line_time = intersect(left_mid_circle,angle_line)[0];
// path angle_arc = subpath(left_mid_circle,angle_arc_left_gutter_time, angle_arc_angle_line_time);
// draw(pic, angle_arc, highlight_color, arrow=EndArrow(2.5));

// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// // ...... mark the triangle ..........
// picture pic;
// int picnum = 15;
// size(pic,5cm,0,keepAspect=true);

// real theta = PI/3;
// pair left_top = (-4*cos(theta), 4*sin(theta));
// pair left_mid = (0,0);
// pair right_mid = (4,0);
// pair right_top = right_mid+(4*cos(theta),4*sin(theta));
// path gutter = left_top--left_mid--right_mid--right_top;

// fill(pic, left_top--left_mid--right_mid--right_top--cycle, light_color+white);
// draw(pic, gutter, black);

// path left_gutter = left_top--left_mid;
// path mid_gutter = left_mid--right_mid;
// path right_gutter = right_mid--right_top;

// path top_gutter = left_top--right_top;
// draw(pic, top_gutter, black+dashed);

// // find slopes, so can mark the dimensions
// // pair dir_left = dir(left_gutter);
// // pair dir_right = dir(right_gutter);

// real bar_length = 2pt;
// real offset = 0.5;

// draw(pic, left_mid--(left_mid.x,left_top.y), highlight_color+dashed);

// // path left_dimension = shift(offset*(-1*dir_left.y,dir_left.x))*left_gutter;
// // draw(pic, "$4$", left_dimension, E, highlight_color, bar=Bars(bar_length));
// // path mid_dimension = shift(0,-offset)*mid_gutter;
// // draw(pic, "$4$", mid_dimension, highlight_color, bar=Bars(bar_length));
// // path right_dimension = shift(offset*(-1*dir_right.y,dir_right.x))*right_gutter;
// // draw(pic, "$4$", right_dimension, W, highlight_color, bar=Bars(bar_length));

// // thetas
// draw(pic, left_mid--(left_mid.x-1.25,left_mid.y), highlight_color+dashed);
// label(pic,"$\theta$", left_mid+(-0.75,0.5), highlight_color);
// path angle_line = left_mid--(left_mid.x-1.25,left_mid.y);
// draw(pic, angle_line, highlight_color+dashed);
// path left_mid_circle = circle(left_mid,0.5);
// // draw(pic, left_mid_circle, highlight_color);
// real angle_arc_left_gutter_time = intersect(left_mid_circle,left_gutter)[0];
// real angle_arc_angle_line_time = intersect(left_mid_circle,angle_line)[0];
// path angle_arc = subpath(left_mid_circle,angle_arc_left_gutter_time, angle_arc_angle_line_time);
// draw(pic, angle_arc, highlight_color, arrow=EndArrow(2.5));

// label(pic,"$\theta$", left_top+(offset,-0.3), highlight_color);

// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// // ...........................
// // graph of it
// real f16(real x) {return 16*sin(x)*(cos(x)+1);}

// picture pic;
// int picnum = 16;
// size(pic,0,5cm,keepAspect=true);
// scale(pic, Linear(2), Linear);


// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=2;
// ybot=0; ytop=21;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,Scale(pic,(i,ybot-0.25))--Scale(pic,(i,ytop+0.25)),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,Scale(pic,(xleft-0.25,j))--Scale(pic,(xright+0.25,j)),GRAPHPAPERPEN);
//   }
// }

// draw(pic, graph(pic,f16,0,PI/2), highlight_color);

// filldraw(pic, circle(Scale(pic,(0,0)),0.05), highlight_color,  highlight_color);
// filldraw(pic, circle(Scale(pic,(PI/3,f16(PI/3))),0.075), highlight_color,  highlight_color);
// filldraw(pic, circle(Scale(pic,(PI/2,f16(PI/2))),0.075), highlight_color,  highlight_color);

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ==== 1/x  on open interval (1..3) ====
real f17(real x) {return 1/x;}

picture pic;
int picnum = 17;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=4;
ybot=0; ytop=2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f17,1,3), highlight_color);

filldraw(pic, circle((1,1),0.05), white,  highlight_color);
filldraw(pic, circle((3,1/3),0.05), white,  highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
labelx(pic,"$1$",1);
labelx(pic,"$3$",3);
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");






// ==== 2x^3-15x^2+24x+7 ====
real f18(real x) {return 2*x^3-15*x^2+24*x+7;}

picture pic;
int picnum = 18;
scale(pic, Linear(5), Linear);
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=6;
ybot=-10; ytop=45;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(pic, f18,xleft-0.1,xright+0.02), highlight_color);

filldraw(pic, circle(Scale(pic,(6,f18(6))),0.5), highlight_color,  highlight_color);
filldraw(pic, circle(Scale(pic,(4,f18(4))),0.5), highlight_color,  highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
      ticks=LeftTicks(Step=10,step=0,OmitTick(0),Size=2pt),
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ==== x+4/x ====
real f19(real x) {return x+(4/x);}

picture pic;
int picnum = 19;
scale(pic, Linear(3), Linear);
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0.02; xright=20;
ybot=0; ytop=50;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(pic, f19,xleft+.06,xright+0.02), highlight_color);

filldraw(pic, circle(Scale(pic,(2,f19(2))),0.4), highlight_color,  highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-2.75, xmax=xright+.75,
  p=currentpen,
      ticks=RightTicks(Step=5,step=1,OmitTick(0),Size=2pt,size=1pt),
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-2.75, ymax=ytop+2.75,
  p=currentpen,
      ticks=LeftTicks(Step=10,step=5,OmitTick(0),Size=2pt,size=1pt),
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ==== 19x-x^2 ====
real f20(real x) {return 19*x-x^2;}

picture pic;
int picnum = 20;
scale(pic, Linear(10), Linear);
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=10;
ybot=0; ytop=100;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(pic, f20,xleft-.1,xright+0.1), highlight_color);

filldraw(pic, circle(Scale(pic,(9.5,f20(9.5))),0.5), highlight_color,  highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
      ticks=RightTicks(Step=1,OmitTick(0),Size=2pt),
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-4.75, ymax=ytop+4.75,
  p=currentpen,
      ticks=LeftTicks(Step=10,step=5,OmitTick(0),Size=2pt,size=1pt),
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// // ==== picture of linear approx and differentials ====
// real f1(real x) {return (1/3)*(5-(x-2)^2);}
// real tangent1(real x) {return (2/3)*(x-1)+(4/3);}

// picture pic;
// int picnum = 1;
// size(pic,0,4cm,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=2;
// ybot=0; ytop=2;

// // Draw graph paper
// // for(real i=xleft; i <= xright; ++i) {
// //   if (abs(i)>.01) { 
// //     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
// //   }
// // }
// // for(real j=ybot; j <= ytop; ++j) {
// //   if (abs(j)>.01) { 
// //     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
// //   }
// // }

// draw(pic, graph(f1,xleft+0.25,xright+0.1), black);
// draw(pic, graph(tangent1,0.5,2), highlight_color);
// filldraw(pic, circle((1,4/3),0.025), highlight_color,  highlight_color);
// label(pic,  "$(a,f(a))$", (1,4/3), 2*NW, filltype=Fill(white));

// real[] T1 = {1};

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.25, xmax=xright+.75,
//   p=currentpen,
//       ticks=RightTicks("%",T1),
//   arrow=Arrows(TeXHead));
// labelx(pic,"$a$", 1);
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.25, ymax=ytop,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
