// derivatives.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "derivative%03d";

import graph;


// ===== Secant lines go to tangent line ====
real f0(real x) {return 2-(1/4)*(x-3)^2;}

picture pic;
int picnum = 0;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f0,xleft-0.1,xright+0.1),
     black);

real h = 0.7;
pair pt0 = (1,f0(1));
pair pt1 = (1+h,f0(1+h));
filldraw(pic, circle(pt0,0.05), highlight_color, highlight_color);
label(pic,"$(a,f(a))$", pt0, NW, filltype=Fill(white));
filldraw(pic, circle(pt1,0.05), highlight_color, highlight_color);
label(pic,"$(a+h,f(a+h))$", pt1, NW, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ..... secant line .......
real secant1(real x) {return (pt1.y-pt0.y)/(pt1.x-pt0.x)*(x-pt0.x)+pt0.y;}

picture pic;
int picnum = 1;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=3;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f0,xleft-0.1,xright+0.1),
     black);

real h = 0.7;
pair pt0 = (1,f0(1));
pair pt1 = (1+h,f0(1+h));
filldraw(pic, circle(pt0,0.05), black, black);
label(pic,"$(a,f(a))$", pt0, NW, filltype=Fill(white));
filldraw(pic, circle(pt1,0.05), black, black);
label(pic,"$(a+h,f(a+h))$", pt1, NW, filltype=Fill(white));
draw(pic, graph(secant1,pt0.x-1,pt1.x+1),
     highlight_color);


xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ 5-(1/2)*(x-3)^2 =======
real f(real x) {return 5-(1/2)*(x-3)^2;}

picture pic;
int picnum = 2;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

// Label L_fcn = Label("$f(x)$", align=1.65*W, position=MidPoint, black,
// 		    filltype=Fill(white));
draw(pic, graph(f,-0.1,5.1), highlight_color);
filldraw(pic, circle((1,3),0.05), white, black);
label(pic,  "$(1,3)$", (1,3), SE);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ............................
// sage: def f(x):
// ....:     return 5-(1/2)*(x-3)^2
// ....: 
// sage: f(3)
// 5
// sage: f(1)
// 3
// sage: f(2)
// 9/2
pair anchor_pt = (1,3);
pair first_secant_pt = (2,4.5);
real first_secant(real x) {return ((4.5-3)/(2-1))*(x-1)+3;}


picture pic;
int picnum = 3;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}


draw(pic, graph(f,-0.1,5.1), black);
draw(pic, graph(first_secant,0.5,2.5), highlight_color);

filldraw(pic, circle(anchor_pt,0.05), white, black);
label(pic,  "$(1,3)$", anchor_pt, SE);
filldraw(pic, circle(first_secant_pt,0.05), white, black);
label(pic,  "$(2,4.5)$", first_secant_pt, SE);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ............................
// sage: def f(x):
// ....:     return 5-(1/2)*(x-3)^2
// ....: 
// sage: f(1.1)
// 3.19500000000000
pair anchor_pt = (1,3);
pair second_secant_pt = (1.1,3.195);
real second_secant(real x) {return ((3.195-3)/(1.1-1))*(x-1)+3;}


picture pic;
int picnum = 4;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}


draw(pic, graph(f,-0.1,5.1), black);
draw(pic, graph(second_secant,0.25,1.75), highlight_color);

filldraw(pic, circle(anchor_pt,0.05), white, black);
label(pic,  "$(1,3)$", anchor_pt, SE);
filldraw(pic, circle(second_secant_pt,0.05), white, black);
label(pic,  "$(1.1,3.195)$", second_secant_pt, (0.85,0.1));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ............................
// sage: def f(x):
// ....:     return 5-(1/2)*(x-3)^2
// ....: 
// sage: f(1.1)
// 3.19500000000000
pair anchor_pt = (1,3);
real tangent_line(real x) {return 2*(x-1)+3;}


picture pic;
int picnum = 5;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f,-0.1,5.1), black);
draw(pic, graph(tangent_line,0.25,1.75), highlight_color);

// label(pic,graphic("climbing.png","width=0.80cm,angle=10"),
//       anchor_pt-(0.1,-0.15));

filldraw(pic, circle(anchor_pt,0.05), white, black);
label(pic,  "$(1,3)$", anchor_pt, SE);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ===== Secant lines go to tangent line ====
real f6(real x) {return 5-x^2;}
real tangent6(real x) {return -2*(x-1)+4;}

picture pic;
int picnum = 6;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f6,xleft-0.1,xright+0.25),
     black);
draw(pic, graph(tangent6,0.5,1.75),
     highlight_color);

filldraw(pic, circle((1,4),0.05), highlight_color, highlight_color);
label(pic,"$(1,4)$", (1,4), NE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

