\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title % 
{Section 3.1\ \ The constant $e$ and the natural logarithm}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}{Where it comes from: review of simple and compound interest}
Invest a principal of~$P_0$ for $t$ years at an annual rate of~$r$
percent per year then at the end the \alert{simple interest} value is this.
\begin{equation*}
  A=P_0[1+rt]
\end{equation*}

\pause
If the interest is assessed $n$ times a year then at the end the 
\alert{compound interest} value is this.
\begin{equation*}
  A=P_0\left[1+\frac{r}{n}\right]^{n\cdot t}
\end{equation*}

\Ex Deposit $\$1000$ for $t=3$ years at an annual rate of 
$4.25\%$ (that's $r=0.0425$)
compounded quarterly, so $m=4$ times per year:
\begin{equation*}
  1000\left[1+\frac{0.0425}{4}\right]^{4\cdot3}=1135.22
\end{equation*}
(rounded to the nearest cent).
% sage: 1000*(1+0.0425/4)^(4*3)
% 1135.22108009586
\end{frame}


\begin{frame}{What if you compound more often}
The simplest principal is $P_0=1$,
the simplest number of years is $t=1$, 
and the simplest rate is $r=1.00$.
What happens for larger and larger $n$'s?
\begin{center}
  \begin{tabular}{r|l}
    \multicolumn{1}{c}{\textit{Num compoundings $m$}}
      &\multicolumn{1}{c}{\rule[-5pt]{0em}{12pt}\textit{Value $1\cdot\left[1+\frac{1.00}{n}\right]^{n\cdot 1}$}}           \\
    \hline
    $1$         &$2.00000$  \\
    $4$         &$2.44141$  \\
    $12$        &$2.61304$  \\
    $365$       &$2.71457$  \\
    $1,000$     &$2.71692$  \\
    $1,000,000$ &$2.71828$  \\
  \end{tabular}
\end{center}
\pause
As $n$ grows the amount of money grows, but it is bounded.
It has a limit.
This limit is denoted \alert{$e$}.
\begin{equation*}
  e=\lim_{n\to\infty}\left[1+\frac{1}{n}\right]^n
   \approx 2.718
\end{equation*}
\end{frame}
% sage: def A(x):
% ....:     return 1*(1+(1/x))^(x*1)
% ....: 
% sage: for m in [1, 4, 12, 365, 1000, 1000000]:
% ....:     print("m=",m," A=",n(A(m)))
% ....: 
% m= 1  A= 2.00000000000000
% m= 4  A= 2.44140625000000
% m= 12  A= 2.61303529022468
% m= 365  A= 2.71456748202187
% m= 1000  A= 2.71692393223589
% m= 1000000  A= 2.71828046931938


\begin{frame}{The lesson: feedback can discrete or continuous}

With interest, at each compounding you get more money, and because you
have more money, the next time you get even more.
This is feedback.

Feedback can be discrete or continuous.
\begin{itemize}
\item Discrete feedback: $\displaystyle A=P_0\left[1+\frac{r}{n}\right]^{nt}$
\item Continuous feedback: $\displaystyle A=P_0e^{rt}$
\end{itemize}
\end{frame}


\begin{frame}{Graph of $e^x$}
\begin{center}
  \includegraphics{asy/e000.pdf}    
\end{center}
\end{frame}



\section{Extra: review of logarithmic functions}

\begin{frame}{Definition}
The inverse of the exponential function $f(x)=b^x$
is the \alert{base~$b$ logarithm $\log_b(x)$}.
\begin{equation*}
  b^x=y \quad\text{if and only if}\quad \log_b(y)=x
\end{equation*}
On the left is the exponential function for
the base $e\approx 2.718$.
On the right is its inverse, the 
\alert{natural logarithm $\ln(x)$}. 
\begin{center}
   \vcenteredhbox{\includegraphics{../3.2_derivatives_exps/asy/derivatives_exps000.pdf}}
   \qquad
   \vcenteredhbox{\includegraphics{../3.2_derivatives_exps/asy/derivatives_exps001.pdf}}
\end{center}
\end{frame}




% \begin{frame}{The natural logarithm $\ln y$}
% To solve an equation with a variable in the exponent, we need the 
% function \alert{$\ln(y)$}, defined by $\ln(e^x)=x$.

% \Ex Solve $5=e^t$ by applying the $\ln$ function to both sides.
% \begin{align*}
%   \ln(5) &=\ln(e^t)  \\
%   1.609  &= t
% \end{align*}
% The value on the left came from a calculator.  
% Three decimal places is enough for us.

% \pause
% Solve each.  
% Use a calculator as needed.
% \begin{enumerate}
% \item $\displaystyle 10=e^{2t}$
% \pause
% \\ We have
%   $\ln(10)=2t$.
% Divide to get $t=2.303/2=1.151$.
% \item $\displaystyle 41=5e^{-t}$
% \pause
% \\ First divide to get the exponential expression by itself
%   $41/5=8.2=e^{-t}$.
% Take the natural log of both sides
%   $\ln(8.2)=-t=2.104$.
% The answer is $t=-2.104$.
% % \item $\displaystyle 0.51=3e^{4t}$
% % \pause
% % \\ First divide
% %   $0.51/3=0.17=e^{4t}$.
% % Take the log
% %   $\ln(0.17)=-1.772=4t$.
% % Solve for $t$:  $t=-1.772/4=-0.443$.
% \end{enumerate}
% \end{frame}
% sage: n(ln(10))
% 2.30258509299405
% sage: n(ln(10)/2)
% 1.15129254649702
% sage: n(41/5)
% 8.20000000000000
% sage: n(ln(8.2))
% 2.10413415427021
% sage: n(0.51/3)
% 0.170000000000000
% sage: n(ln(0.17))
% -1.77195684193188
% sage: n(ln(0.17))/4
% -0.442989210482969

\begin{frame}{Graph of $\ln(x)$}
\begin{center}
  \includegraphics{asy/e001.pdf}    
\end{center}
\end{frame}




\begin{frame}{Most important formulas for logarithms}
Some formulas below are expressed in terms of the natural 
log $\ln$, although they work for all $\log_b$.
\begin{itemize}
\item The expression $\log_b(x)$ asks: $b^{\text{what?}}=x$.
  In the special case of the base~$e$, the 
  expression $\ln(x)$ asks the question $e^{\text{what?}}=x$.
\item The \alert{change of base}  formula
\begin{equation*}
  \log_b(x)=\frac{\ln x}{\ln b}
\end{equation*}
\item The logarithm of a product is the sum of the logarithms \\
  $\displaystyle \ln(x\cdot y)=\ln x+\ln y $
\item Because of the prior formula, 
  $\displaystyle \ln\bigl(\frac{x}{y}\bigr)=\ln x-\ln y $
  and $\displaystyle \ln\bigl(\frac{1}{x}\bigr)=-\ln x $
  and $\displaystyle \ln(x^n)=n\cdot \ln x $.
\end{itemize}
\end{frame}



\begin{frame}{Practice}
\begin{enumerate}
\item $\log_3(81)$
\pause \\ The question $\displaystyle 3^{\text{what?}}=81$ is answered by $4$.
\item $\ln(e^{-7x})$
\pause \\ This asks the question $e^{\text{what?}}=e^{-7x}$ so the answer
is the exponent $-7x$.
In short, the $\ln$ function cancels the exponential, giving $-7x$.
\item $\ln(x\cdot e^5)$
\pause \\ $\displaystyle \ln(x)+\ln(e^5)=\ln(x)+5$.
% \item Solve for the unknown: $100=7e^{5t}$.
% \pause \\ First divide both sides $\displaystyle 100/7=e^{5t}\!$.
%   That's $14.286=e^{5t}\!$.
%   Now take the $\ln$ of both sides $\ln(14.286)=5t$, which gives
%   $t=\ln(14.286)/5=0.532$. 
\end{enumerate}
\end{frame}
% sage: n(100/7)
% 14.2857142857143
% sage: ln(14.286)/5
% 0.531856007346556

\begin{frame}{Using logarithms to solve for exponents}
\begin{enumerate}
\item Solve for the unknown: $100=7e^{5t}$
\pause\\
   Divide $100/7=e^{5t}$ to get $14.286=e^{5t}$
   (we will write three decimal places and call it equal).
   Take the logarithm of both sides $\displaystyle \ln(14.286)=\ln(e^{5t})=5t$,
   to get $t=\ln(14.286)/5=0.532$.
\item Solve: $3000=2000e^{0.04t}$
\pause\\
   Divide $3000/2000=e^{0.04t}$ to get $1.5=e^{0.04t}$.
   Take the logarithm, $\displaystyle \ln(1.5)=0.04t$,
   giving $t=\ln(1.5)/0.04=10.137$.
\item Deposit $\$12,000$ at an annual rate of $3.8\%$ compounded continuously.
  How long until you have $\$18,000$? 
\pause\\
   The continuously compounded equation is $A=P_0e^{rt}$ so we have
   $18000=12000e^{0.038t}\!$.
\pause\\
   Divide $18000/12000=e^{0.038t}$ to get $1.5=e^{0.038t}\!$.
   Take the logarithm $\displaystyle \ln(1.5)=0.038t$,
   giving $t=\ln(1.5)/0.038=10.670$.
\end{enumerate}
\end{frame}


% \begin{frame}{Most important formulas for logarithms}  
% The formulas below are expressed in terms of the natural 
% log $\ln$, although they work for all $\log_b$.
% \begin{itemize}
% \item The expression $\log_b(x)$ asks: $b^{\text{what?}}=x$.
% \item The \alert{change of base}  formula
% \begin{equation*}
%   \log_b(x)=\frac{\ln x}{\ln b}
% \end{equation*}
% \item The logarithm of a product is the sum of the logarithms \\
%   $\displaystyle \ln(x\cdot y)=\ln x+\ln y $
% \item Because of the prior formula, 
%   $\displaystyle \ln\bigl(\frac{x}{y}\bigr)=\ln x-\ln y $
%   and $\displaystyle \ln\bigl(\frac{1}{x}\bigr)=-\ln x $
%   and $\displaystyle \ln(x^n)=n\cdot \ln x $.
% \end{itemize}
% \end{frame}

% \begin{frame}{Practice}
% \begin{enumerate}
% \item $\log_3(81)$
% \pause \\ The question $\displaystyle 3^{\text{what?}}=81$ is answered by $4$.
% \item $\ln(e^{-7x})$
% \pause \\ The $\ln$ function cancels the exponential, so the answer is $-7x$.
% \item $\ln(x\cdot e^5)$
% \pause \\ $\displaystyle \ln(x)+\ln(e^5)=\ln(x)+5$.
% % \item Solve for the unknown: $100=7e^{5t}$.
% % \pause \\ First divide both sides $\displaystyle 100/7=e^{5t}\!$.
% %   That's $14.286=e^{5t}\!$.
% %   Now take the $\ln$ of both sides $\ln(14.286)=5t$, which gives
% %   $t=\ln(14.286)/5=0.532$. 
% \end{enumerate}
% \end{frame}
% % sage: n(100/7)
% % 14.2857142857143
% % sage: ln(14.286)/5
% % 0.531856007346556


% \begin{frame}{Using logarithms to solve for exponents}
% \begin{enumerate}
% \item Solve for the unknown: $100=7e^{5t}$
% \pause\\
%    Divide $100/7=e^{5t}$ to get $14.286=e^{5t}$
%    (we will write three decimal places and call it equal).
%    Apply the definition of the logarithm $\displaystyle \ln(14.286)=5t$,
%    to get $t=\ln(14.286)/5=0.532$.
% \item Solve: $3000=2000e^{0.04t}$
% \pause\\
%    Divide $3000/2000=e^{0.04t}$ to get $1.5=e^{0.04t}$.
%    Apply the definition of the logarithm $\displaystyle \ln(1.5)=0.04t$,
%    giving $t=\ln(1.5)/0.04=10.137$.
% \item Deposit $\$12,000$ at an annual rate of $3.8\%$ compounded continuously.
%   How long until you have $\$18,000$? 
% \pause\\
%    The continuously compounded equation is $A=Pe^{rt}$ so we have
%    $18000=12000e^{0.038t}$.
% \pause\\
%    Divide $18000/12000=e^{0.0038t}$ to get $1.5=e^{0.038t}$.
%    Take the logarithm $\displaystyle \ln(1.5)=0.038t$,
%    giving $t=\ln(1.5)/0.038=10.670$.
% \end{enumerate}
% \end{frame}




% \begin{frame}
% The \alert{change of base}  formula
% \begin{equation*}
%   \log_b(x)=\frac{\ln x}{\ln b}
% \end{equation*}
% means that we can do all our work in the natural log. 
% So the formulas below are expressed in $\ln$, although they work
% for all $\log_b$.
% \begin{itemize}
% \item The logarithm of a product is the sum of the logarithms \\
%   $\displaystyle \ln(x\cdot y)=\ln x+\ln y $
% \item $\displaystyle \ln\bigl(\frac{x}{y}\bigr)=\ln x-\ln y $
% \item $\displaystyle \ln\bigl(\frac{1}{x}\bigr)=-\ln x $
% \item $\displaystyle \ln(x^n)=n\cdot \ln x $
% \end{itemize}

% Practice
% \begin{enumerate}
% \item $\log_3(27)$
% \item $\ln(1/\sqrt{e})$
% \item Solve for the unknown: $100=7e^{5t}$
% \end{enumerate}  
% \end{frame}


% \begin{frame}
% \Ex
% Save $\$2000$ in an account at an annual rate of 
% $3.25\%$ compounded continuously.
% How long until you have $\$4000$?
% \begin{align*}
%   4000       &= 2000\cdot e^{0.0325\cdot t} \\
%   4000/2000 &= e^{0.0325\cdot t} \\
%   2 &= e^{0.0325\cdot t} \\
%   \ln(2) &= \ln(e^{0.0325\cdot t}) \\
%   0.693  &= 0.0325\cdot t
% \end{align*}
% Now divide $0.693/0.0325$ to get $t=21.323$ years.
% \end{frame}


\begin{frame}\vspace*{-1ex}
% \Ex An investor buys a stock for $\$20,000$ and three years later sells 
% for $\$28,000$.
% Assuming continuous compounding, what is the interest rate?
% \pause
% \begin{align*}
%   28000 &= 20000\cdot e^{r\cdot 3}  \\
%   28000/20000 &= e^{r\cdot 3}  \\
%   1.400 &= e^{r\cdot 3}   \\
%   \ln(1.400) &=  3r \\
%   0.336  &=  3r
% \end{align*}
% The answer is $0.336/3=0.112$.  
% That's a little more than $11\%$.

\Ex A population of bacteria grows at $9\%$ per hour.
How long until the population doubles?
\pause
\begin{align*}
  2P   &= P\cdot e^{0.09 t}  \\
  2P/P &= e^{0.09 t}  \\
  2 &= e^{0.09 t}  \\
  \ln(2) &= 0.09\cdot t  \\
  0.693 &= 0.09 t
\end{align*}
So the \alert{doubling time} is $t=0.693/0.09\approx 7.7$ hours.

\pause
\Ex We have $P_0$ grams of radium, a radioactive material.
Scientists know that for this material $r\approx -0.000433$.
What is the \alert{half life}, the time until we only have $P_0/2$ grams left?
\pause
\begin{align*}
  P_0/2 &= P_0\cdot e^{-0.000433\cdot t}  \\
  1/2 &= e^{-0.000433\cdot t}  \\
  \ln(1/2) &= -0.000433\cdot t  \\
  -0.693   &= -0.000433 t
\end{align*}
The division gives about $t=-0.693/-0.000433\approx 1600.461$ years.
\end{frame}




% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
