\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 4.4\ \ Curve sketching}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................





\begin{frame}{Combining first and second derivative information}
  \begin{center}\small
  \begin{tabular}{r|cc}
    \multicolumn{1}{c}{\ }
    &\multicolumn{1}{c}{\textit{Concave up}}
      &\multicolumn{1}{c}{\textit{Concave down}}  \\ \cline{2-3}
    \rule{0pt}{0.8cm}
    \textit{Increasing}
      &\vcenteredhbox{\includegraphics{asy/curve_sketching000.pdf}}  
      &\vcenteredhbox{\includegraphics{asy/curve_sketching001.pdf}}  \\
    \rule{0pt}{0.9cm}
    \textit{Decreasing}
      &\vcenteredhbox{\includegraphics{asy/curve_sketching002.pdf}}  
      &\vcenteredhbox{\includegraphics{asy/curve_sketching003.pdf}}  
  \end{tabular}
\end{center}

\pause
\textbf{Curve sketching guidelines}
  
\begin{enumerate}
\item Use the techniques for understanding the curve without calculus.
That includes finding $f(0)$ and the roots, where $f(x)=0$, if convenient.

\item Use the first derivative to find the intervals of increase and
decrease.
Use the second derivative to find the intervals of concavity.
Find the points of local minima and maxima.

\item Combine the information to sketch the graph.
\end{enumerate}
\end{frame}



\begin{frame}{$f(x)=(1/3)x^3-(1/2)x^2-2x+3$}
This is a cubic so we know its large scale behavior,
$\lim_{x\to\infty}f(x)=\infty$ and $\lim_{x\to-\infty}f(x)=-\infty$.
\begin{center}
  \includegraphics{asy/curve_sketching006.pdf}%
\end{center}
It crosses the $y$~axis at $(0,3)$.
  Finding $x$~axis crossings by solving $f(x)=0$ is not as easy.
\end{frame}

\begin{frame}
The derivatives are here.
\begin{align*}
  f'(x)  &= x^2-x-2=(x+1)(x-2)  \\
  f''(x) &= 2x-1
\end{align*}
The critical numbers are $c_1=-1$ and $c_2=2$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-1}$  &$f'(-2)=4$  &$f$ is increasing  \\
    $\open{-1}{2}$        &$f'(0)=-2$  &$f$ is decreasing  \\
    $\open{2}{\infty}$    &$f'(3)=4$   &$f$ is increasing  
  \end{tabular}
\end{center}
There is one solution to $f''(x)=0$ so these are the intervals of concavity.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{1/2}$  &$f''(0)=-1$  &$f$ is concave down  \\
    $\open{1/2}{\infty}$   &$f''(1)=1$   &$f$ is concave up  
  \end{tabular}
\end{center}

At $x=-1$ there is a local max, at $x=2$ there is a local min.
At $x=1/2$ there is an inflection point.
Here are the associated outputs.
\begin{equation*}
  f(-1)=25/6
  \quad
  f(1/2)=23/12
  \quad
  f(2)=-1/3
\end{equation*}
\end{frame}

\begin{frame}
\begin{center}\small
  \includegraphics{asy/curve_sketching007.pdf}  \\[1ex]
  $f(x)=(1/3)x^3-(1/2)x^2-2x+3$    
\end{center}
\end{frame}


\begin{frame}{$(2x-4)/(x+2)$}
It is a rational function, so we note that the numerator is zero at $x=2$
and that the denominator is zero at $x=-2$.
Further, $f(0)=-2$
We also note the horizontal asymptote $\lim_{x\to\pm\infty}f(x)$ of $y=2$. 
\begin{center}
  \includegraphics{asy/curve_sketching010.pdf}
\end{center}
\end{frame}

\begin{frame}
These are the derivatives.
\begin{align*}
  f'(x)
       &=\frac{(x+2)\cdot 2-(2x-4)\cdot 1}{(x+2)^2}
       =\frac{8}{(x+2)^2}=8(x+2)^{-2}                  \\
  f''(x)
      &=8\cdot (-2\cdot (x+2)^{-3}\cdot 1)
       =-16(x+2)^{-3}
\end{align*}

The only critical number is $-2$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-2}$  &$f'(-3)=8$  &$f$ is increasing  \\
    $\open{-2}{\infty}$    &$f'(0)=2$   &$f$ is increasing  
  \end{tabular}
\end{center}
The only solution to $f''(x)=0$ is also $-2$
so these are the intervals of concavity.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-2}$  &$f''(-3)=16$  &$f$ is concave up  \\
    $\open{-2}{\infty}$   &$f''(0)=-2$   &$f$ is concave down  
  \end{tabular}
\end{center}
\end{frame}
% sage: def f_prime(x):
% ....:     return 8*(x+2)^(-2)
% ....: 
% sage: f_prime(-1)
% 8
% sage: f_prime(0)
% 2
% sage: def f_double(x):
% ....:     return -16*(x+2)^(-3)
% ....: 
% sage: f_double(-3)
% 16
% sage: f_double(0)
% -2
% sage: f_prime(-3)
% 8

\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching011.pdf} \\[2ex]
  $\displaystyle f(x)=\frac{2x-4}{x+2}$    
\end{center}
\end{frame}




% =========== 3+7e^{-0.2x} ============
\begin{frame}{Exponential function $f(x)=3+7e^{-0.2x}$}
We see that when $x=0$ then $f(x)=10$.
Because it is an exponential function it never crosses the $x$~axis.
Notice that $\lim_{x\to\infty}3+7e^{-0.2x}=3$ and
$\lim_{x\to-\infty}3+7e^{-0.2x}=\infty$. 
\begin{center}
  \includegraphics{asy/curve_sketching012.pdf}%
\end{center}
\end{frame}


\begin{frame}
These are the derivatives.
\begin{equation*}
  f'(x)=-1.4e^{-0.2x}
  \qquad
  f''(x)=0.28e^{-0.2x}
\end{equation*}
Exponential functions are never zero so never is $f'(x)=0$.
There are no critical numbers.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{\infty}$  &$f'(0)=-1.4$     &$f$ decreasing  \\
  \end{tabular}
\end{center}
The second derivative also has no roots.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{\infty}$  &$f''(0)=0.28$      &$f$ concave up  \\
  \end{tabular}
\end{center}
There are no local maximums or minimums.
\end{frame}

\begin{frame}
\begin{center}\small
  \includegraphics{asy/curve_sketching013.pdf}  \\[1ex]
  $f(x)=3+7e^{-0.2x}$  
\end{center}
\end{frame}




\begin{frame}{Practice}
\Ex Summarize the pertinent information and sketch the graph for
$f(x)=x^2-4x +3$  

\pause
It is a quadratic, and $f(0)=3$.
It factors $f(x)=(x-1)(x-3)$ so roots are $x=1$ and $x=3$.

\pause 
Here are the derivatives.
\begin{equation*}
  f'(x)=2x-4=2(x-2)
  \qquad
  f''(x)=2
\end{equation*}
There is one critical number, $x=2$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{2}$  &$f'(1)=-2$  &$f$ decreasing  \\
    $\open{2}{\infty}$   &$f'(3)=2$   &$f$ increasing  \\
  \end{tabular}
\end{center}

\pause
The second derivative has no roots.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{\infty}$  &$f''(0)=2$      &$f$ concave up  \\
  \end{tabular}
\end{center}
The local minimum is $(2,-1)$.
\end{frame}
% sage: def f_prime(x):
% ....:     return 2*x-4
% ....: 
% sage: f_prime(1)
% -2
% sage: f_prime(3)
% 2
\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching014.pdf}    
\end{center}
\end{frame}


\begin{frame}
\Ex Summarize the pertinent information and sketch the graph for
$f(x)=-2x^2+5x - 3$.  

\pause
It is a downwards quadratic with $f(0)=-3$.
It factors $f(x)=(-2x+3)(x-1)$ so the roots are $x=1$ and $x=3/2$.

\pause
These are the derivatives.
\begin{equation*}
  f'(x)=-4x+5
  \qquad
  f''(x)=-4
\end{equation*}
There is one critical number, $x=5/4$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{5/4}$  &$f'(1)=1$    &$f$ increasing  \\
    $\open{5/4}{\infty}$   &$f'(2)=-3$   &$f$ decreasing  \\
  \end{tabular}
\end{center}

\pause
The second derivative has no roots.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{\infty}$  &$f''(0)=-4$      &$f$ concave down  \\
  \end{tabular}
\end{center}
\end{frame}
% sage: def f_prime(x):
% ....:     return -4*x+5
% ....: 
% sage: f_prime(1)
% 1
% sage: f_prime(2)
% -3

\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching015.pdf}    
\end{center}
\end{frame}




\begin{frame}
\Ex Sketch $2x^3-3x^2-12x+12$. 

\pause
The leading term is $2x^3$, so that gives the long-term behavior.
It crosses the $y$~axis at $(0,12)$.
It doesn't easily factor, so we skip looking for roots.
\begin{equation*}
  f'(x)=6x^2-6x-12
       =6(x+1)(x-2)
  \qquad
  f''(x)=12x-6=6(2x-1)  
\end{equation*}
The critical numbers are $-1$ and $2$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-1}$  &$f'(-2)=24$   &$f$ increasing  \\
    $\open{-1}{2}$        &$f'(0)=-12$   &$f$ decreasing  \\
    $\open{2}{\infty}$    &$f'(3)=24$    &$f$ increasing  \\
  \end{tabular}
\end{center}
So there is a local maximum at $(-1,19)$ 
and a local minimum at $(2,-8)$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{1/2}$  &$f''(0)=-6$   &$f$ concave down  \\
    $\open{1/2}{\infty}$   &$f''(1)=6$    &$f$ concave up  \\
  \end{tabular}
\end{center}
\end{frame}
% sage: def f(x):
% ....:     return(2*x^3-3*x^2-12*x+1)
% ....: 
% sage: f(-1)
% 19
% sage: f(2)
% -8
% sage: def f_prime(x):
% ....:     return (6*(x+1)*(x-2))
% ....: 
% sage: f_prime(-2)
% 24
% sage: f_prime(0)
% -12
% sage: f_prime(3)
% 24
% sage: def f_double(x):
% ....:     return (6*(2*x-1))
% ....: 
% sage: f_double(0)
% -6
% sage: f_double(1)
% 6
\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching020.pdf}
\end{center}
\end{frame}



\begin{frame}
Summarize the pertinent information and sketch the graph.

\Ex $f(x)=(2+x)/(3-x)$

\pause
It has the horizontal asymptote $\lim_{x\to\pm\infty}f(x)=-1$, 
the vertical asymptote $x=3$, and a root at $x=-2$.
It crosses the $y$~axis at $2/3$.
\pause
\begin{align*}
  f'(x) &=\frac{(3-x)\cdot 1-(2+x)\cdot (-1)}{(3-x)^2}
         =\frac{5}{(3-x)^2}
         =5(3-x)^{-2}
  \\
  f''(x) &=5\cdot (-2)(3-x)^{-3}\cdot(-1)=\frac{2}{(3-x)^3}
\end{align*}
There is one critical number, $x=3$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{3}$  &$f'(2)=5$    &$f$ increasing  \\
    $\open{3}{\infty}$   &$f'(4)=5$    &$f$ increasing  \\
  \end{tabular}
\end{center}
The second derivative has only the root $x=3$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{3}$  &$f''(2)=2$      &$f$ concave up  \\
    $\open{3}{\infty}$   &$f''(4)=-2$      &$f$ concave down  \\
  \end{tabular}
\end{center}
\end{frame}
% sage: def f_prime(x):
% ....:     return 5/((3-x)^2)
% ....: 
% sage: f_prime(2)
% 5
% sage: f_prime(4)
% 5
% sage: def f_double(x):
% ....:     return 2/((3-x)^3)
% ....: 
% sage: f_double(2)
% 2
% sage: f_double(4)
% -2

\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching016.pdf}  \\[1.5ex]
   $\displaystyle f(x)=\frac{2+x}{3-x}$
\end{center}
\end{frame}





\begin{frame}{Practice}
Sketch $\displaystyle x^4-6x^2\!$.

\pause
It factors as 
$x^4-6x^2=x^2(x^2-6)$ so the $x$~intercepts are $x=0,\pm\sqrt{6}$.

\pause
The derivative is 
$f'(x)=4x^3-12x=4x(x^2-3)$,
so there are local mins at $(-\sqrt{3},-9)$ and~$(\sqrt{3},-9)$
and a local max at $(0,0)$.

\pause
The second derivative is 
$f''(x)=12x^2-12=12(x^2-1)$ so it is concave up, then down, then up
and the inflection points are $(-1,5)$ and $(1,-5)$. 
\end{frame}
\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching021.pdf}
\end{center}
\end{frame}

% \item $\displaystyle (x^2-1)^3$
% \pause \\
% $f(x)=[(x+1)(x-1)]^3$ so the $x$~intercepts are $x=-1,1$.
% $f'(x)=6x(x^2-1)^2=6x(x+1)^2(x-1)^2$, and $f$ is decreasing, 
% then decreasing again, then increasing, and then increasing again, 
% and therefore has a local minimum at $(0,-1)$.
% $f''(x)=6(x^3-1)(5x^2-1)$ and is first concave up, then down, then up,
% then down and then up again, giving inflection points at
% $(-1,0)$, $(-1/\sqrt{5},-64/125)$, $(1/\sqrt{5},-64/125)$, and $(1,0)$. 
% \item $\displaystyle x\cdot \sqrt{x^2+1}$
% \pause\\
% The only $x$~intercept is $x=0$.
% By the product rule $f'(x)=(2x^2+1)/\sqrt{x^2+1}$
% and because $2x^2+1$ has no real roots there are no local optima.
% As to the second derivative, 
% \begin{equation*}
%   f''(x)=\frac{2x^3+3x}{(x^2+1)\sqrt{x^2+1}}
% \end{equation*}
% and we get that $f''(x)=0$ for $x=0, -3/2$,
% and the function is concave down, then down, then up, 
% and so there is an inflection at $(0,0)$
\begin{frame}
Sketch $\displaystyle \frac{x}{(x-1)^2}$.

\pause
There is one $x$~intercept, at $x=0$.

\pause
The derivative is 
$f'(x)=(-x-1)/(x-1)^3$ so the critical points are $x=-1,0,1$.
We get $f$ increasing, decreasing, and increasing again
so there is a local minimum at $(-1,-1/4)$.

\pause
The second derivative is 
$f''(x)=(2x+4)/(x-1)^4$ which has only one zero, namely $x=-2$,
but there are three intervals to check because of the vertical asymptote
at $x=1$.
We get that $f$ is concave down, up, and then down again,
so it has an inflection point at $(-2,-2/9)$. 
\end{frame}

\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching022.pdf}
\end{center}
\end{frame}






\begin{frame}
\Ex Summarize the pertinent information and sketch $f(x)=4e^{0.5x}-1$.

\pause
It crosses the $y$~axis at $3$.
Any exponential function~$b^x$ has the horizontal asymptote at zero,
so $f$ has the horizontal asymptote at $y=-1$.
Looking for roots by solving $f(x)=0$ gives $\ln(1/4)=0.5x$, so $x\approx-2.77$.

\pause
Here is the derivative information.
\begin{equation*}
  f'(x) =2e^{0.5x}
  \qquad
  f''(x) =e^{0.5x}
\end{equation*}
Trying to solve $f'(x)=0$ shows that there is no critical number.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{\infty}$  &$f'(0)=2$    &$f$ increasing  \\
  \end{tabular}
\end{center}
The second derivative also has no roots.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{\infty}$  &$f''(0)=1$      &$f$ concave up  \\
  \end{tabular}
\end{center}
\end{frame}

\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching017.pdf}  \\[1.5ex]
   $\displaystyle f(x)=4e^{0.5x}-1$
\end{center}
\end{frame}



\begin{frame}
\Ex Summarize the pertinent information and sketch $f(x)=\ln(2x+4)$.

\pause
It crosses the $y$~axis at $\ln(4)\approx 1.39$.
Any logarithmic function has vertical asymptote, because $\ln(x)$ is
not defined when $x\leq 0$.
Rewriting this one as $\ln(2(x+2))$ shows that the vertical asymptote is
at $x=-2$.
Looking for roots by solving $f(x)=0$ gives $\ln(2x+4)=0$, so 
$2x+4=e^0=1$ and 
$x=-3/2$.

\pause
Here is the derivative information.
\begin{equation*}
  f'(x) =\frac{1}{2x+4}\cdot 2=\frac{1}{x+2}
  \qquad
  f''(x) =-1\cdot (x+2)^{-2}\cdot 1=\frac{-1}{(x+2)^2}
\end{equation*}
There is no critical number.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{\infty}$  &$f'(0)=1/2$    &$f$ increasing  \\
  \end{tabular}
\end{center}
The second derivative also has no roots.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{\infty}$  &$f''(0)=-1/2$      &$f$ concave down  \\
  \end{tabular}
\end{center}
\end{frame}

\begin{frame}
\begin{center}
  \includegraphics{asy/curve_sketching018.pdf}  \\[1.5ex]
   $\displaystyle f(x)=\ln(2x+4)$
\end{center}
\end{frame}



\begin{frame}{Practice}
Sketch each curve.
\begin{enumerate}
\item $\displaystyle \frac{2+x}{3-x}$
\pause
\item $\displaystyle -2+4e^3x$
\pause
\item $\displaystyle 10xe^{-0.1x}$
\pause
\item $\displaystyle \frac{x^2}{1+x^2}$
\end{enumerate}
\end{frame}



% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
