// continuity.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "continuity%03d";

import graph;


// ===== Tightrope ====
real f0(real x) {return 2+(1/10)*(x-2.5)^2;}


picture pic;
int picnum = 0;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=4;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f0,xleft-0.1,xright+0.1), // L=L_fcn,
     highlight_color);

label(pic,graphic("tightrope1.png","height=0.50cm,angle=-20"),(0.5,2.7),
      filltype=NoFill);
label(pic,graphic("tightrope.png","height=0.50cm,angle=30"),(4.1,2.54),
      filltype=NoFill);
// filldraw(pic, circle(focus_other,0.05), white, black);
// label(pic,graphic("earth.png","width=0.20cm"), earth_point,
//       filltype=Fill(white));


xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ===== Discontinuity ====
real f1(real x) {return (x^2);}

picture pic;
int picnum = 1;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=4;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f1,xleft-0.1,xright+0.1), // L=L_fcn,
     highlight_color);

filldraw(pic, circle((1,1),0.05), white, highlight_color);
filldraw(pic, circle((1,2),0.05), highlight_color, highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ===== Discontinuity ====
real f2(real x) {return((2-(1/2)*x^2));}

picture pic;
int picnum = 2;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=3;
ybot=-2; ytop=2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f2,xleft-0.1,xright-0.1), // L=L_fcn,
     highlight_color);

filldraw(pic, circle((0,2),0.05), highlight_color, highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ================ Random graph =======
picture pic;
int picnum = 3;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-2; ytop=4;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic,(xleft-0.2,1)--(1,1),highlight_color);
filldraw(pic, circle((1,1),0.05), white, highlight_color);
// label(pic,  "$(1,1)$", (1,1), NW);
filldraw(pic, circle((1,2),0.05), highlight_color, highlight_color);
draw(pic,(1,2)..{right}(2,3)::{down}(2.9,-2),highlight_color);
draw(pic,(3.1,-2){up}::(5,4),highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .... greatest integer ...
real f6(real x) {return tan(x);}

picture pic;
int picnum = 4;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3.25;
ybot=0; ytop=4;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

// label(pic, Label("$f(x)=\sqrt{x+7}$", 
//  		    highlight_color,
// 		 filltype=Fill(white)), (2,4));
filldraw(pic, circle((0,0),0.05), highlight_color, highlight_color);
draw(pic,(0,0)--(0.95,0),highlight_color);
  filldraw(pic, circle((1,0),0.05), white, highlight_color);
filldraw(pic, circle((1,1),0.05), highlight_color, highlight_color);
draw(pic,(1,1)--(1.95,1),highlight_color);
  filldraw(pic, circle((2,1),0.05), white, highlight_color);
filldraw(pic, circle((2,2),0.05), highlight_color, highlight_color);
draw(pic,(2,2)--(2.95,2),highlight_color);
  filldraw(pic, circle((3,2),0.05), white, highlight_color);
filldraw(pic, circle((3,3),0.05), highlight_color, highlight_color);
draw(pic,(3,3)--(xright+0.25,3),highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ sqrt(x-2) =======
real f5(real x) {return sqrt(x-2);}

picture pic;
int picnum = 5
  ;
size(pic,4.5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f5,2,xright+0.2), // L=L_fcn,
     highlight_color);

filldraw(pic, circle((2,0),0.05), highlight_color, highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ (1/5)e^x =======
real f6(real x) {return (1/5)*exp(x);}

picture pic;
int picnum = 6
  ;
size(pic,4.5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f5,2,xright+0.2), // L=L_fcn,
     highlight_color);

filldraw(pic, circle((2,0),0.05), highlight_color, highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ===== Discontinuity ====
real f7(real x) {return(exp(x));}

picture pic;
int picnum = 7;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=0; ytop=8;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f7,xleft-0.1,xright+0.1), // L=L_fcn,
     highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ===== Sign charts ====
real SIGN_Y = +0.4;  // where to locate words "pos", "neg"


picture pic;
int picnum = 8;
size(pic,5cm,0,keepAspect=true);

real xleft, xright; // limits of number line
xleft=-3; xright=3;

label(pic,"\makebox[3em][l]{- - - }",(-3/2,SIGN_Y));
label(pic,"\makebox[3em][l]{+ + + }",(1/2,SIGN_Y));

real[] T={-1/2};
xaxis(pic, L="",  // label
      // axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=Ticks(T),
  arrow=Arrows(TeXHead));
  
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

// ................ x^2-1 .........
picture pic;
int picnum = 9;
size(pic,5cm,0,keepAspect=true);

real xleft, xright; // limits of number line
xleft=-3; xright=3;

// label(pic,"\makebox[3em][l]{- - - }",(-3/2,SIGN_Y));
// label(pic,"\makebox[3em][l]{+ + + }",(1/2,SIGN_Y));

real[] T9={-1, +1};
xaxis(pic, L="",  // label
      // axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=Ticks(T9),
  arrow=Arrows(TeXHead));
  
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

// .............................
picture pic;
int picnum = 10;
size(pic,5cm,0,keepAspect=true);

real xleft, xright; // limits of number line
xleft=-3; xright=3;

label(pic,"\makebox[3em][l]{+ + + }",(-2,SIGN_Y));
label(pic,"\makebox[3em][c]{- - - }",(0,SIGN_Y));
label(pic,"\makebox[3em][l]{+ + + }",(2,SIGN_Y));

real[] T10={-1, +1};
xaxis(pic, L="",  // label
      // axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=Ticks(T10),
  arrow=Arrows(TeXHead));
  
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ........... x^2-2x-8 ..................
picture pic;
int picnum = 11;
size(pic,5cm,0,keepAspect=true);

real xleft, xright; // limits of number line
xleft=-3; xright=6;

label(pic,"\makebox[3em][l]{+ + + }",(-4,SIGN_Y));
label(pic,"\makebox[3em][l]{- - - }",(1,SIGN_Y));
label(pic,"\makebox[3em][l]{+ + + }",(6,SIGN_Y));

real[] T11={-2, +4};
xaxis(pic, L="",  // label
      // axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=Ticks(T11),
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ........... (2x+7)/(5x-1) ..................
picture pic;
int picnum = 12;
size(pic,5cm,0,keepAspect=true);

real xleft, xright; // limits of number line
xleft=-5; xright=5;

label(pic,"\makebox[3em][l]{+ + + }",(-5,SIGN_Y));
label(pic,"\makebox[3em][l]{- - - }",(-1,SIGN_Y));
label(pic,"\makebox[3em][l]{+ + + }",(2,SIGN_Y));

real[] T12={-7/2, +1/5};
xaxis(pic, L="",  // label
      // axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
      ticks=RightTicks("%",T12),
  arrow=Arrows(TeXHead));
labelx(pic,"-7/2",-7/2);
labelx(pic,"1/5",1/5);


shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
