// first_derivative.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "first_derivative%03d";
real PI = acos(-1);

import graph;



// ==== inc/dec and concavity ====

// ....... inc and up ............
picture pic;
int picnum = 0;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf = (0.5,0.5){E}::(1,0.75)::(1.5,1.5);

draw(pic, grf, highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... inc and down ............
picture pic;
int picnum = 1;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf = (0.5,0.5)::(1,1.25)::(1.5,1.5);

draw(pic, grf, highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... dec and down ............
picture pic;
int picnum = 2;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf = (0.5,1.5)::(1,0.75)::(1.5,0.5);

draw(pic, grf, highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ....... dec and up ............
picture pic;
int picnum = 3;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf = (0.5,1.5)::(1,1.25)::(1.5,0.5);

draw(pic, grf, highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ======= concave up ======
real f4(real x) {return 2*(x-1)^2+1;}

picture pic;
int picnum = 4;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(f4, 0.5, 1.5), highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

real f5(real x) {return -2*(x-1)^2+1.5;}

// ......... concave down ...........
picture pic;
int picnum = 5;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(f5, 0.5, 1.5), highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ===== Sign charts ====
real SIGN_Y = +0.4;  // where to locate words "pos", "neg"


picture pic;
int picnum = 6;
size(pic,5cm,0,keepAspect=true);

real xleft, xright; // limits of number line
xleft=-3; xright=9;

label(pic,"\makebox[0em][r]{- - -}",(5-1/2,SIGN_Y));
label(pic,"\makebox[0em][l]{+ + +}",(5+1/2,SIGN_Y));

real[] T={5};
xaxis(pic, L="",  // label
      // axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=Ticks(T),
  arrow=Arrows(TeXHead));
    
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ..........................
picture pic;
int picnum = 7;
size(pic,5cm,0,keepAspect=true);

real xleft, xright; // limits of number line
xleft=-6; xright=6;

label(pic,"\makebox[0em][r]{+ + +}",(-3-1/2,SIGN_Y));
label(pic,"\makebox[0em][c]{- - -}",(0,SIGN_Y));
label(pic,"\makebox[0em][l]{+ + +}",(3+1/2,SIGN_Y));

real[] T={-3, 3};
xaxis(pic, L="",  // label
      // axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=Ticks(T),
  arrow=Arrows(TeXHead));
  
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ========= f(x)=x^3-27x-20 =========
real f8(real x) {return x^3-27*x-20;}

picture pic;
int picnum = 8;
size(pic,0,5cm,keepAspect=true);
scale(pic, Linear(7), Linear);

real xleft, xright, ybot, ytop; // limits of number line
xleft=-4; xright=4;
ybot=-80; ytop=35;

draw(pic, graph(pic, f8, xleft-0.1, xright+0.1), highlight_color);
dotfactor = 4;
dot(pic, Scale(pic,(3,f8(3))), highlight_color);
dot(pic, Scale(pic,(-3,f8(-3))), highlight_color);

xaxis(pic, L="",  // label
      // axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
      ticks=RightTicks("%",Step=1,step=0,OmitTick(0),size=2pt),
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-5, ymax=ytop+2.75,
      p=currentpen,
      ticks=LeftTicks(Step=10,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");






// ========= f(x)=1/(x-5) =========
real f9(real x) {return 1/(x-5);}

picture pic;
int picnum = 9;
size(pic,0,4cm,keepAspect=true);
scale(pic, Linear(2), Linear);

real xleft, xright, ybot, ytop; // limits of number line
xleft=-1; xright=6;
ybot=-10; ytop=10;

draw(pic, graph(pic, f9, xleft-0.1, 5-0.1), highlight_color);
draw(pic, graph(pic, f9, 5+0.1, xright+0.1), highlight_color);

draw(pic, Scale(pic, (5,ybot))--Scale(pic, (5,ytop)), dashed);

xaxis(pic, L="",  // label
      // axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
      ticks=RightTicks("%",Step=1,step=0,OmitTick(0),Size=2pt),
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks(Step=10,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ========= f(x)=x^{2/3} =========
real f10(real x) {return (x^2)^(1/3);}

picture pic;
int picnum = 10;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of number line
xleft=-3; xright=3;
ybot=0; ytop=2;

draw(pic, graph(pic, f10, xleft-0.1, xright+0.1), highlight_color);

xaxis(pic, L="",  // label
      // axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ..........................
picture pic;
int picnum = 11;
size(pic,5cm,0,keepAspect=true);

real xleft, xright; // limits of number line
xleft=-2; xright=2;

label(pic,"\makebox[0em][r]{- - -}",(0-1/2,SIGN_Y));
label(pic,"\makebox[0em][l]{+ + +}",(0+1/2,SIGN_Y));

real[] T={0};
xaxis(pic, L="",  // label
      // axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=Ticks(T),
  arrow=Arrows(TeXHead));
  
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ========= 1/(x-5)  sign chart ==========
picture pic;
int picnum = 12;
size(pic,5cm,0,keepAspect=true);

real xleft, xright; // limits of number line
xleft=-1; xright=8;

label(pic,"\makebox[0em][r]{- - -}",(4-1/2,SIGN_Y));
label(pic,"\makebox[0em][c]{- - -}",(6+1/2,SIGN_Y));

real[] T={5};
xaxis(pic, L="",  
      // axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=Ticks(T),
  arrow=Arrows(TeXHead));
  
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ============ First Derivative Test =============
real f13(real x) {return -2*(x-1)^2+1.5;}

picture pic;
int picnum = 13;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(f13, 0.5, 0.95), highlight_color);
draw(pic, graph(f13, 1.05, 1.5), highlight_color);
// filldraw(pic, circle((1,f10(1)), 0.025), highlight_color, highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ..............................
real f14(real x) {return 2*(x-1)^2+0.5;}

picture pic;
int picnum = 14;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(f14, 0.5, 0.95), highlight_color);
draw(pic, graph(f14, 1.05, 1.5), highlight_color);
// filldraw(pic, circle((1,f10(1)), 0.025), highlight_color, highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");






