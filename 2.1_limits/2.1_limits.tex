\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title[Limits] % (optional, use only with long paper titles)
{Section 2.1\ Limits}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\subject{Limits}
% This is only inserted into the PDF information catalog. Can be left
% out. 

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}{Last time: the rate of change as a limit}
\begin{center}
  \only<1>{\includegraphics{asy/limits000.pdf}}%    
  \only<2>{\includegraphics{asy/limits001.pdf}}%    
  \only<3>{\includegraphics{asy/limits002.pdf}}%    
  \only<4->{\includegraphics{asy/limits003.pdf}}%    
\end{center}
\begin{center}
  \begin{tabular}{lc|c}
    \multicolumn{2}{c}{\textit{Inputs}}
      &\multicolumn{1}{c}{\textit{Slope of line}} \\ \hline
      $2$ &$1$       &\uncover<2->{$(4.5-3)/(2-1)=1.5$} \\
      $1.1$ &$1$     &\uncover<3->{$(3.195-3)/(1.1-1)=1.95$} \\
      $1.01$ &$1$    &\uncover<4->{$(3.0019995-3)(1.01-1)=1.999499$} \\
    $1.001$ &$1$   &\uncover<4->{$(3.000199995-3)(1.001-1)=1.999950$} \\
    $1$ &$1$       &\uncover<4->{?$2$, somehow?}
  \end{tabular}
\end{center}
\end{frame}


\begin{frame}
\frametitle{Intuitive definition of a limit}
\alert{The limit of $f$ is $L$ as $x$ approaches $c$} if whenever
input $x$'s are near~$c$ then the output $f(x)$'s are near~$L$.
We write this.
\begin{equation*}
  \alert{\lim_{x\to c}f(x)=L}
  \quad\text{or}\quad
  \text{\alert{if $x\to c$ then $f(x)\to L$}}
\end{equation*}

In terms of the prior slide's line slopes, $c=1$ and $L=2$.
\begin{center}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{\textit{Input $x$}}
      &\multicolumn{1}{c}{\textit{Output $\operatorname{slope}(x)$}} \\ \hline
      $2$       &$1.5$ \\
      $1.1$     &$1.95$ \\
      $1.01$    &$1.999499$ \\
    $1.001$     &$1.999950$ 
  \end{tabular}
\end{center}
% (Where $g(x)=5-(1/2)(x-3)^2$, %=-x^2/2+3x+1/2$.
% the function is $f(x)=(g(x)-3)/(x-1)$.)  

For the prior slide,
as $x$'s get closer to $c=1$, the slopes get closer to $L=2$.
So we write this.
\begin{equation*}
  \lim_{x\to 1}\operatorname{slope}(x)=2
\end{equation*}
\end{frame}


\begin{frame}{Numerical example: $f(x)=x^2$}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits004.pdf}}
  \hspace*{2em}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{$x$} &\multicolumn{1}{c}{$f(x)$}  \\
    \hline
    $2.100$ &\uncover<2>{$4.4100$}  \\
    $2.010$ &\uncover<2>{$4.0401$}  \\
    $2.001$ &\uncover<2>{$4.0040$}  \\
  \end{tabular}
\end{center}
\only<2->{We've only done numbers above $2$.
Numbers just below $2$ would show a similar trend.
We estimate that as $x$ approaches~$2$ then $f(x)$ approaches~$4$.
\begin{equation*}
  \lim_{x\to 2}f(x)=4
  \qquad
  \lim_{x\to 2}x^2=4
\end{equation*}}
Of course, $f(2)=4$ so this is actually an easy one.
\end{frame}


\begin{frame}{Practice without a picture}
  Numerically estimate $\lim_{t\to 2}g(t)$, using numbers just below~$c=2$.
  \begin{equation*}
    g(t)=\frac{4(t^2-4)}{t-2}
  \end{equation*}
\begin{center}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{$t$} &\multicolumn{1}{c}{$g(t)$}  \\
    \hline
    $1.5$ &\uncover<2->{$14$}  \\
    $1.9$ &\uncover<2->{$15.6$}  \\
    $1.99$ &\uncover<2->{$15.96$}  \\
    $1.999$ &\uncover<2->{$15.996$}  \\
  \end{tabular}
\end{center}
\only<2->{We estimate that $\lim_{t\to 2}g(t)=16$.}

\medskip
\only<3>{
Notice that with
\begin{equation*}
  g(t)=\frac{4(t^2-4)}{t-2}
\end{equation*}
the function is not defined at $t=2$.
So this example is more subtle than just plugging~$c$ into the function.
In the next class we will see rules for when you can plug in and when
you cannot.}
\end{frame}


\begin{frame}{Limits from both sides}
  So far in this introduction we have only looked
  at inputs just above~$c$.
  Fully correct would be to also look just below it.
  
Let $f(x)=x^2-3x$ and look near $c=1$.
\begin{center}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{$x$} &\multicolumn{1}{c}{$f(x)$}  \\
    \hline
    $1.100$ &\only<2>{$-2.090$}  \\
    $1.010$ &\only<2>{$-2.010$}  \\
    $1.001$ &\only<2>{$-2.001$}  \\
  \end{tabular}
  \hspace*{2em}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{$x$} &\multicolumn{1}{c}{$f(x)$}  \\
    \hline
    $0.900$ &\only<2>{$-1.890$}  \\
    $0.990$ &\only<2>{$-1.990$}  \\
    $0.999$ &\only<2>{$-1.999$}  \\
  \end{tabular}
\end{center}
\uncover<2->{We say this.
\begin{center}
  $\displaystyle \lim_{x\to 1}x^2-3x=-2$
\end{center}}
\end{frame}
% sage: def f(x):
% ....:     return x^2-3*x
% ....: 
% sage: for i in [0.1, 0.01, 0.001]:
% ....:     print("1+i",1+i," f(1+i)=",f(1+i))
% ....: 
% 1+i 1.10000000000000  f(1+i)= -2.09000000000000
% 1+i 1.01000000000000  f(1+i)= -2.00990000000000
% 1+i 1.00100000000000  f(1+i)= -2.00099900000000
% sage: for i in [-0.1, -0.01, -0.001]:
% ....:     print("1+i",1+i," f(1+i)=",f(1+i))
% ....: 
% 1+i 0.900000000000000  f(1+i)= -1.89000000000000
% 1+i 0.990000000000000  f(1+i)= -1.98990000000000
% 1+i 0.999000000000000  f(1+i)= -1.99899900000000


\begin{frame}{One more from both sides}
Let $f(x)=x/(3(x-2))$ and find $\lim_{x\to 1}f(x)$.
\begin{center}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{$x$} &\multicolumn{1}{c}{$f(x)$}  \\
    \hline
    $1.100$ &\uncover<2>{$-0.407$}  \\
    $1.010$ &\uncover<2>{$-0.340$}  \\
    $1.001$ &\uncover<2>{$-0.334$}  \\
  \end{tabular}
  \hspace*{2em}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{$x$} &\multicolumn{1}{c}{$f(x)$}  \\
    \hline
    $0.900$ &\uncover<2>{$-0.273$}  \\
    $0.990$ &\uncover<2>{$-0.327$}  \\
    $0.999$ &\uncover<2>{$-0.333$}  \\
  \end{tabular}
\end{center}
\uncover<2->{We estimate this.
\begin{center}
  $\displaystyle \lim_{x\to 1}\frac{x}{3(x-2)}=\frac{-1}{3}$
\end{center}}
\end{frame}
% sage: for i in [0.1, 0.01, 0.001]:
% ....:     print("1+i=",1+i," f12(1+i)=",f12(1+i))
% ....: 
% 1+i= 1.10000000000000  f12(1+i)= -0.407407407407407
% 1+i= 1.01000000000000  f12(1+i)= -0.340067340067340
% 1+i= 1.00100000000000  f12(1+i)= -0.334000667334001
% sage: for i in [-0.1, -0.01, -0.001]:
% ....:     print("1+i=",1+i," f12(1+i)=",f12(1+i))
% ....: 
% 1+i= 0.900000000000000  f12(1+i)= -0.272727272727273
% 1+i= 0.990000000000000  f12(1+i)= -0.326732673267327
% 1+i= 0.999000000000000  f12(1+i)= -0.332667332667333



\begin{frame}{Graphical example}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits005.pdf}}
\end{center}
Estimate each.
\begin{enumerate}
\item limit as $x$ approaches $c=1$
\pause
\item limit as $x$ approaches $c=3$
\pause
\item limit as $x$ approaches $c=4$ (just guess)
\end{enumerate}
\pause
We write $\lim_{x\to 1}f(x)=3$,
and
$\lim_{x\to 3}f(x)=5$,
and
$\lim_{x\to 4}f(x)\approx 4.5$. 
\end{frame}


\begin{frame}{Another graphical example: a function that is defined in pieces}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits006.pdf}}
  \qquad
  \begin{minipage}{0.3\linewidth}
    \begin{equation*}
      f(x)=
      \begin{cases}
        x^3  &\text{if $x<1$}  \\
        3-x  &\text{if $1\leq x$}
      \end{cases}
    \end{equation*}
  \end{minipage}
\end{center}
First just practice working with it.  Find each.
\begin{enumerate}
\item $f(2)$
\pause
\item $f(1)$
\pause
\item $f(0)$
\end{enumerate}
\end{frame}


\begin{frame}
This is the function from the prior slide.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits006.pdf}}
\end{center}
Estimate each.
\begin{enumerate}
\item limit as $x$ approaches $c=3$
\pause
\item limit as $x$ approaches $c=2$
\pause
\item limit as $x$ approaches $c=1$
  (this one is trickier)
\end{enumerate}
\end{frame}



\begin{frame}{Another example}
  This is $f(x)=\sqrt{x-2}$.
  The second and third parts are tricky.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits007.pdf}}
\end{center}
Estimate each.
\begin{enumerate}
\item the limit as $x$ approaches $c=3$.
\pause
\item limit as $x$ approaches $c=0$
\pause
\item limit as $x$ approaches $c=2$
\end{enumerate}
\end{frame}



\begin{frame}
  \frametitle{One-sided limits}
This function $f(x)=\sqrt{x-2}$ has no value for inputs less than~$c=2$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits007.pdf}}
\end{center}
\alert{At the input~$c$ the left-hand limit of $f$ is $L$}
if for inputs~$x$ close to but below~$c$, the output values $f(x)$
get close to~$L$.

The \alert{right-hand limit of $f$ is $L$} if for inputs~$x$
close to but greater than~$c$, the output values $f(x)$ get close to~$L$.
\end{frame}

\begin{frame}
We write these for left-\hbox{} and right-hand limits.
  \begin{equation*}
    \alert{\lim_{x\to c^-}f(x)=L}
    \qquad
  \alert{\lim_{x\to c^+}f(x)=L}
\end{equation*}
For the prior example we would say this.
  \begin{equation*}
     \text{$\lim_{x\to 2^-}f(x)$ does not exist}
    \qquad
  \lim_{x\to 2^+}f(x)=0
\end{equation*}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits007.pdf}}
\end{center}
\end{frame}

\begin{frame}
Even for a function that is defined on both sides, we can take one-sided limits.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits007.pdf}}
\end{center}
Find each.
\begin{enumerate}
\item $\displaystyle \lim_{x\to 3^-} f(x)$
\pause\item $\displaystyle \lim_{x\to 3^+} f(x)$
\pause\item $\displaystyle \lim_{x\to 3} f(x)$
\end{enumerate}

A two-sided limit exists and equals~$L$
if and only if both one-sided limits exist and both equal~$L$.
\end{frame}


\begin{frame}{Limits with graphs instead of formulas}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/limits009.pdf}}
\end{center}
This is another function that has separate pieces.
Estimate each: 
(1)~$\lim_{x\to 0}f(x)$,
\pause(2)~$\lim_{x\to 1^-}f(x)$,
\pause(3)~$\lim_{x\to 1^+}f(x)$,
\pause(4)~$\lim_{x\to 1}f(x)$,
\pause(5)~$\lim_{x\to 2^-}f(x)$,
\pause(6)~$\lim_{x\to 2^+}f(x)$,
\pause(7)~$\lim_{x\to 2}f(x)$,
\pause(8)~$\lim_{x\to 3^-}f(x)$,
\pause(9)~$\lim_{x\to 3^+}f(x)$,
\pause(10)~$\lim_{x\to 4}f(x)$.
\end{frame}


\section{Algebra of limits}
\begin{frame}
\begin{enumerate}
\item The limit of a constant function is that constant, $\lim_{x\to c} k=k$.
\item If $f(x)=x$ then $\lim_{x\to c} f(x)=c$.
\item The limit of a constant times a function is the constant times the 
limit.
  \begin{equation*}
    \lim_{x\to c}\,k\cdot f(x)\,
    =k\cdot \lim_{x\to c}f(x)
  \end{equation*}
\item The limit of a sum is the sum of the limits.
  \begin{equation*}
    \lim_{x\to c}\,f(x)+g(x)\,
    =\lim_{x\to c}f(x)+\lim_{x\to c} g(x)
  \end{equation*}
\item The limit of a difference is the difference of the limits.
  \begin{equation*}
    \lim_{x\to c}\,f(x)-g(x)\,
    =\lim_{x\to c}f(x)-\lim_{x\to c} g(x)
  \end{equation*}
\item The limit of the product of functions is the product of the 
limits.
  \begin{equation*}
    \lim_{x\to c}\,f(x)\cdot g(x)\,
    =\lim_{x\to c}f(x)\,\cdot\,\lim_{x\to c} g(x)
  \end{equation*}
\item The limit of a quotient of functions is the quotient of the 
limits, provided that the denominator is not zero.
  \begin{equation*}
    \lim_{x\to c}\frac{f(x)}{g(x)}
    =\frac{\lim_{x\to c}f(x)}{\lim_{x\to c} g(x)}
    \qquad
    \text{if $\lim_{x\to c} g(x)\neq 0$}
  \end{equation*}
\end{enumerate}  
\end{frame}

\begin{frame}
\begin{enumerate}
\item[8] The limit of a power is the power of the limit.
  \begin{equation*}
    \lim_{x\to c}f(x)^k
    =\left[\,\lim_{x\to c}f(x)\right]^k
  \end{equation*}
\item[9] The limit of a root is the root of the limit.
  \begin{equation*}
    \lim_{x\to c}\sqrt[k]{f(x)}
    =\sqrt[k]{\lim_{x\to c}f(x)}
  \end{equation*}
  (In this case we disallow even $k$'s for $f(x)\leq 0$, to avoid the
  square root of a negative number.)
\end{enumerate}
\end{frame}



\begin{frame}{Limits of polynomial functions}
The laws make limits of polynomials easy.
\begin{align*}
  \lim_{x\to 5}x^2-3x+4
  &=\lim_{x\to 5}x\cdot\lim_{x\to 5}x -3\cdot \lim_{x\to 5}x+4  \\
  &=5\cdot 5-3\cdot 5+4=14
\end{align*}

  If $f$ is a polynomial function then to find the limit as $x$ approaches~$c$,
  plug $c$ into the polynomial.
  \begin{equation*}
    \lim_{x\to c}f(x)\quad\text{equals}\quad f(c)
  \end{equation*}

  Examples:
  \begin{enumerate}
  \item Where $f(x)=2x^2-5$ find $\lim_{x\to 4}f(x)$.
    \only<2->{\begin{equation*}
      \lim_{x\to 4}f(x)=f(4)=2\cdot 4^2-5=27
    \end{equation*}}
  \only<3->{\item Where $f(x)=6-x^3$ find $\lim_{x\to 1}f(x)$.}
    \only<4->{\begin{equation*}
           \lim_{x\to 1}f(x)=f(1)=6-1^3=5
      \end{equation*}}
  \only<5->{\item Where $f(x)=-3+2x$ find $\lim_{x\to 5}f(x)$.}
    \only<6->{\begin{equation*}
       \lim_{x\to 5}f(x)=f(5)=-3+10=7
      \end{equation*}}
  \end{enumerate}  
\end{frame}




\begin{frame}{Limits of rational functions where the denominator is not zero}
  Let $f$ be a rational function, a fraction of two functions.
  To find the limit as $x$ approaches~$c$,
  as long as the denominator is not zero, just plug $c$ into $f\!$.
  (If the denominator is zero then we must do more thinking.)
  \begin{equation*}
    \lim_{x\to c}\frac{n(x)}{d(x)}=\frac{n(c)}{d(c)} \quad\text{if $d(c)\neq 0$}
  \end{equation*}

  Examples:
  \begin{enumerate}
  \item Find $\lim_{x\to 3}f(x)$. 
    \begin{equation*}
      f(x)=\frac{x^2-1}{x+1}
    \end{equation*}
    \only<2->{$\lim_{x\to 3}f(x)=f(3)=(9-1)/(3+1)=2$}
  \only<3->{\item Find the limit as $x$ approaches $c=0$.
    \begin{equation*}
      g(x)=\frac{3}{9x^2-6}
    \end{equation*}}
    \only<4->{$\lim_{x\to 0}f(x)=f(0)=3/-6=-1/2$}
  \end{enumerate}
\end{frame}




\begin{frame}{What about rational functions when the denominator equals zero?}

Suppose that we want $\lim_{x\to c}f(x)$ where $f(x)=n(x)/d(x)$.
If $d(c)$ is not zero then we just plug in to get $n(c)/d(c)$.

If $d(c)=0$ then either thing could happen:~in some problems 
the limit exists, and in some it does not.
We have to do it on a case-by-case basis.
\end{frame}

\begin{frame}{Case One: it can happen that the limit does not exist}
This is $f(x)=1/(x-2)$.
We want $\lim_{x\to 2}f(x)$.
At $c=2$ the denominator is zero.
\begin{center}
  \includegraphics{asy/limits008.pdf}%    
\end{center}
No single limit exists.
To the left, as $x\to c^-$ the values $f(x)$ are more and more negative.
To the right, they are more and more positive.
\end{frame}

\begin{frame}{Case two: it can happen that the limit does exist}
Here also, the denominator is zero at $c$.
\begin{equation*}
  \lim_{h\to 0}\frac{h(h+2)}{h}
\end{equation*}
\pause
But with some algebra the difficulty goes away.
\begin{equation*}
  \lim_{h\to 0}\frac{h(h+2)}{h}
  =\lim_{h\to 0}\frac{h+2}{1}
  =\frac{2}{1}
  =2
\end{equation*}
At $c=0$, in $h(h+2)/h$ the denominator equals zero.
But in $(h+2)/1$ the denominator does not equal zero.
So the limit laws apply.
\end{frame}


\begin{frame}{With rational functions the numerator also matters}
Suppose that $f(x)=n(x)/d(x)$, and also that $d(c)=0$.
\begin{itemize}
\item If $n(c)$ is not zero then (provided we have cancelled all common terms) 
the limit of $f(x)$ is either $+\infty$ or $-\infty$.
\item  If $n(c)=0$ then we say $\lim_{x\to c}n(x)/d(x)$
is an \alert{indeterminate form}. 
We will say more about these later.
\end{itemize}

Decide if each is an indeterminate form.
\begin{enumerate}
\item $\lim_{x\to 2}(3-x)/(x^2-4)$
\pause\item $\lim_{x\to -1}(x+1)/(x^2-1)$
\pause\item $\lim_{x\to 0}(5x)/(x+1)$
\end{enumerate}
\end{frame}





\begin{frame}{Examples}
\begin{enumerate}
\pause\item
\textnumber{2.1.36}
$\lim_{x\to 4} \frac{x-2}{x}$  

\pause\item
$\lim_{x\to 2}x^2-5x+1$

\pause\item
\textnumber{2.1.42}
Given that $\lim_{x\to 1}f(x)=-5$
and $\lim_{x\to 1}g(x)=4$, find 
$\lim_{x\to 1} (g(x)-3\cdot f(x))$  

\pause\item
\textnumber{2.1.44}
Given that $\lim_{x\to 1}f(x)=-5$
and $\lim_{x\to 1}g(x)=4$, find 
$
  \displaystyle\lim_{x\to 1}\frac{3-f(x)}{1-4g(x)}
$

\pause\item
Find $\lim_{x\to 3}f(x)$ where the function is defined by cases.
\begin{equation*}
  f(x)=
  \begin{cases}
    x^2+1 &\text{if $x\leq 2$}  \\
    x^2-1 &\text{if $x>2$}  \\
  \end{cases}
\end{equation*}
\end{enumerate}
\end{frame}



\section{Difference quotients}

\begin{frame}{Difference quotient limits}
Often we are given $f(x)$ and $a$, and asked to find this
\alert{difference quotient limit}.
\begin{equation*}
  \lim_{h\to 0}\frac{f(a+h)-f(a)}{h}
\end{equation*}
For instance, if $f(x)=x+5$ and $a=4$ then we have this.
\begin{equation*}
  \lim_{h\to 0}\frac{(a+h+5)-(a+5)}{h}
  =\lim_{h\to 0}\frac{(4+h+5)-(4+5)}{h}
\end{equation*}
Notice how on the top we started by taking $f(x)=x+5$ and on the left 
plugging in $a+h$ for $x$, giving $a+h+5$.
\pause
\begin{align*}
  =\lim_{h\to 0}\frac{(4+h+5)-(4+5)}{h}
  &=\lim_{h\to 0}\frac{(9+h)-(9)}{h}  \\
  &=\lim_{h\to 0}\frac{h}{h}=\lim_{h\to 0}1=1
\end{align*}
\end{frame}




\begin{frame}{Another example}
If $f(x)=x^2-1$ and $a=3$ then this is the difference quotient limit.
\begin{align*}
  \lim_{h\to 0}\frac{f(a+h)-f(a)}{h}
  &=\lim_{h\to 0}\frac{f(a+h)-(3^2-1)}{h}    \\
  &=\lim_{h\to 0}\frac{(3+h)^2-1-8}{h}    \\
  &=\lim_{h\to 0}\frac{(3^2+6h+h^2)-1-8}{h}    
\end{align*}
Agian, notice how we plugged in $3+h$ for $x$.
We did \emph{not} get $3^2+h-1$, we instead got $(3+h)^2-1$.
\begin{align*}
  &=\lim_{h\to 0}\frac{6h+h^2}{h}    \\
  &=\lim_{h\to 0}\frac{h(6+h)}{h}    \\
  &=\lim_{h\to 0} 6+h=6    
\end{align*}
The final line does not have $0$ in the denominator, so we can 
apply the limit laws.
\end{frame}

\begin{frame}{Exercises}
For each, find the limit of the difference quotients.
\begin{enumerate}
\item $f(x)=x^2-1$ and $a=3$
\pause
\item $f(x)=5x-1$ and $a=2$
\end{enumerate}
\end{frame}




% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
