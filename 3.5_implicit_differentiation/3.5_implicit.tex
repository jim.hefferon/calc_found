\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 3.5\ \ Implicit differentiation}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}
% Change the default thickness of the underbars.
% https://tex.stackexchange.com/a/559081/339
% \usepackage{xpatch}
% \MHInternalSyntaxOn
% \xpatchcmd\upbracketfill
%   {\sbox\z@{$\braceld$}\edef\l_MT_bracketheight_fdim{\the\ht\z@}}
%   {\edef\l_MT_bracketheight_fdim{.6pt}}
%   {}{\fail}

% \xpatchcmd\downbracketfill
%   {\sbox\z@{$\braceld$}\edef\l_MT_bracketheight_fdim{\the\ht\z@}}
%   {\edef\l_MT_bracketheight_fdim{.6pt}}
%   {}{\fail}
% \MHInternalSyntaxOff

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................




\begin{frame}{Expressing a relationship implicitly}\vspace*{-1ex}
Consider the unit circle $x^2+y^2=1$.
\begin{center}
  \includegraphics{asy/implicit000.pdf}    
\end{center}
Here $y$ is not a function of~$x$\Dash for some input $x$'s there are 
two $y$'s and for some input $x$'s there aren't any $y$'s at all.

So  rather than 
an expression of one variable as a function of the other,
we have a relationship between variables.
\end{frame}

\begin{frame}
But if we focus on, say, a small arc of the circle around the $45^\circ$
point, then there $y$ is a function of~$x$.
\begin{center}
  \includegraphics{asy/implicit002.pdf}    
\end{center}
We want to know this function's rate of change, the slope of the
line tangent to the curve at the point.
\end{frame}

\begin{frame}
From $x^2+y^2=1$ we want $dy/dx$.
We first think of $y$ as a function of~$x$.
For teaching purposes, I explicitly write ``$y(x)$.''
The book doesn't write this and other people won't write it, 
it is just for learning. 
\begin{equation*}
  x^2+[y(x)]^2=1
\end{equation*}
\pause
Next take the derivative of both sides.
\begin{align*}
  \frac{d\,(x^2+[y(x)]^2)}{dx}  &=\frac{d\,1}{dx}  \\
  2x  + \underbracket{2y(x)}\cdot \underbracket{\frac{dy}{dx}}  &=0
\end{align*}
\pause
Finish by solving for $dy/dx$ (I go back to just writing ``$y$''). 
\begin{align*}
  2x  &= -2y(x)\cdot \frac{dy}{dx}  \\
  -\frac{x}{y}  &= \frac{dy}{dx}  
\end{align*}
For example, at the $45^\circ$ point we have
$x=y=\sqrt{2}/2$, giving $-1$.
\end{frame}


\begin{frame}
Here is that tangent line.
\begin{center}
  \includegraphics{asy/implicit001.pdf}    
\end{center}

Note that with $dy/dx=-x/y$ the rate of change is undefined at $y=0$.
That's where $y$ is not a function
of~$x$ because no matter how small an arc you consider, 
for some $x$'s there are two $y$'s.
\end{frame}



\begin{frame}{Implicit differentiation}
When $y$ is a function of $x$ implicitly, to find $dy/dx$ follow these steps.
\begin{itemize}
\item Take $y$ as a function of~$x$, perhaps by writing $y(x)$.
\item Take the derivative of both sides of the equation.
  This may involve the Chain Rule or the Product Rule.
\item Solve for $dy/dx$:~move all terms with $dy/dx$ to one side and 
  all terms without it to the other, and then divide.
\end{itemize}

\bigskip
\Rm
We have seen a number of differentiation rules.
No one of them is hard, it is just that there are a number of them,
so a person may struggle with the amount.
The way around this is just practice.
\end{frame}



\begin{frame}{Practice}
Evaluate $y'$ at the given point.
\begin{equation*}
  5x^3-y-1=0 
  \qquad (1,4)
\end{equation*}
\pause
First, think of $y$ as a function of~$x$.
\begin{equation*}
  5x^3-y(x)-1=0
\end{equation*}
Next take the derivative of both sides.
\pause
\begin{equation*}
  15x^2-\frac{dy}{dx} = 0
\end{equation*}
Finally, solve for $dy/dx$.
\begin{equation*}
  15x^2=\frac{dy}{dx}
\end{equation*}
At $(1,4)$ we have $y'(x)=15$.
\end{frame}

\begin{frame}{Practice \ldots}
\Ex
Find the slope of the line tangent to the graph of $3x+xy+1=0$ at the point
where $x=-1$.
Note that the `$xy$' term is a product of two functions of~$x$, so 
use the Product Rule.

\pause
Think of $y$ as a function of $x$.
\begin{equation*}
  3x+x\cdot y(x)+1=0
\end{equation*}
Differentiate both sides (the Product Rule part is highlighted),
then solve for $dy/dx$.
\begin{align*}
  3+{\color{red}x\cdot \frac{dy}{dx}+y(x)\cdot 1}  &=0   \\
  x\cdot \frac{dy}{dx}  &=-3-y(x)   \\
  \frac{dy}{dx}  &=\frac{-3-y(x)}{x}   
\end{align*}
To find the slope, plug $x=-1$ into the original equation
$3(-1)+(-1)y+1=0$, giving $-2=y$.
Use $(x,y)=(-1,-2)$ to get $dy/dx=(-3-(-2)/-1=-1/-1=1$.
\end{frame}

\begin{frame}{More practice}
Find $dy/dx$ at the given point.
\begin{equation*}
  3xy-2x-2=0
  \qquad
  (2,1)
\end{equation*}
\pause
First think of $y$ as a function of~$x$.
\begin{equation*}
  3x\cdot y(x)-2x-2=0
\end{equation*}
Differentiate with respect to~$x$ (again the Product Rule part is in red).
\pause
\begin{equation*}
  {\color{red}3x\cdot\frac{dy}{dx}+y(x)\cdot (3)}-2=0
\end{equation*}
Solve for $dy/dx$.
\begin{align*}
  3x\cdot\frac{dy}{dx} &=-3y(x)+2  \\
  \frac{dy}{dx}        &=\frac{-3y+2}{3x}
\end{align*}
At the point $(2,1)$ we have $dy/dx=-1/-6=1/6$.
\end{frame}



\begin{frame}{Even more practice}
Where $y^4+xy=x^3-x+2$, find $dy/dx$.
The point $(1,1)$ is on the curve; find the line tangent to that point.

\pause
First think of $y$ as a function of $x$.
\begin{equation*}
  [y(x)]^4+x\cdot y(x)=x^3-x+2
\end{equation*}
\pause
Next take the derivative of both sides.
Use the Chain Rule on the first term and the Product Rule on the 
second.
\begin{align*}
  \underbracket{4y(x)^3}\cdot\underbracket{\frac{dy}{dx}}
     +x\frac{dy}{dx}+y(x)\cdot 1 &=3x^2-1 \\
  \frac{dy}{dx}\cdot (4y(x)^3+x) &=3x^2-1-y(x) \\
  \frac{dy}{dx} &= \frac{3x^2-1-y}{4y^3+x}
\end{align*}
At $(1,1)$ the slope of the tangent line is $1/5$.
\begin{equation*}
  y-1=\frac{1}{5}(x-1)
\end{equation*}
\end{frame}


\begin{frame}{Exercises}

Use implicit differentiation to find $y'$ at the given point.
\begin{enumerate}
\item $\displaystyle 5x^3-y-1=0$ at $(x,y)=(1,4)$
\pause
\item $\displaystyle y^2+x^3+4=0$ at $(-2,2)$
\end{enumerate}

\pause
Find $y'$ and the line tangent to the graph at the given point:
$\displaystyle (y-3)^4-x=y$ at $(x,y)=(-3,4)$.

\end{frame}







% \begin{frame}
% \Ex Find $y'$ if $e^x\cos y-e^{-y}\sin x=\pi$.

% \pause  
% We start by reminding ourselves that we want $y$ as a function of~$x$
% so that when we differentiate, we remember to apply the Chain Rule.
% \begin{equation*}
%   e^x\cos(y(x))-e^{-y(x)}\sin x=\pi
% \end{equation*}
% Take $d/dx$ of both sides.
% Again there are a number of differentiation rules.
% \begin{equation*}
%   e^x\cdot(-\sin(y(x)))\cdot\frac{dy}{dx}+e^x\cos(y(x))
%    -\left[e^{-y(x)}\cos x+\sin x\cdot e^{-y(x)}(-\frac{dy}{dx}) \right]
%   = 0
% \end{equation*}
% Solve for $dy/dx$.
% \begin{align*}   
%   \frac{dy}{dx}\cdot\left(-e^x\sin y+e^{-y}\sin x\right) &=
%       -e^x\cos y+e^{-y}\cos x     \\
%   \frac{dy}{dx}  &=\frac{-e^x\cos y+e^{-y}\cos x}{-e^x\sin y+e^{-y}\sin x}
% \end{align*}
% \end{frame}




% \begin{frame}{Even more practice}
% \begin{enumerate}
% \item Find $dy/dx$ where $2x^2+xy-y^2=2$. 
% \pause\\
% First, rewrite as $2x^2+x\cdot y(x)-y(x)^2=2$.
% Differentiate.
% \begin{equation*}
%   4x+x\frac{dy}{dx}-\underbracket{2y(x)}\cdot\underbracket{\frac{dy}{dx}}=0
% \end{equation*}
% Solve for $dy/dx$.
% \begin{align*}
%    x\frac{dy}{dx} -2y\frac{dy}{dx} &=-4x  \\
%    \frac{dy}{dx}\cdot(x-2y)        &=-4x  \\
%    \frac{dy}{dx}                   &=\frac{-4x}{x-2y}
% \end{align*}
% \item Find $dy/dx$ where $xe^y=x-y$. 
% \item Give an equation for the tangent line
%   when $x^{2/3}+y^{2/3}=4$ at $(-3\sqrt{3},1)$.
% \end{enumerate}
% \end{frame}



% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
