\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 4.2\ \ Second derivative}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................





\section{What the second derivative says about a function}


\begin{frame}{Intervals of concavity}
A graph is \alert{concave upward} over an interval if it opens upward
on that interval.
It is \alert{concave downward} if it opens downward.

\begin{center}\small
  \begin{tabular}{@{}c@{}}
  \includegraphics{asy/second_derivative004.pdf} \\
  \textit{Concave up}
  \end{tabular}
  \quad    
  \begin{tabular}{@{}c@{}}
  \includegraphics{asy/second_derivative005.pdf} \\
  \textit{Concave down}
  \end{tabular}
\end{center}

In the concave up case, from right to left the function is first going
down, then is flat, and then is going up.
Restated, the function's slope is 
negative, then zero, and then positive. 

Likewise, in the concave down case the slope is first positive, then zero,
then negative.
\end{frame}


\begin{frame}{Second derivative}
\Df The \alert{second derivative} of a function $f(x)$
is the derivative of $f'(x)$.
We denote it in either of these ways.
\begin{equation*}
  f''(x)
  \qquad
  \frac{d^2\,f}{dx^2}
\end{equation*}

\Ex
Find the second derivative of $f(x)=5x^2-3x+2$.

\pause
We have $f'(x)=10x-3$.
So $f''(x)=10$.

\pause\medskip
Find the second derivative.
\begin{enumerate}
\item $g(x)=-x^3-2x^2-3x+9$
\pause \\ We have $g'(x)=-3x^2-4x-3$
  and so $g''(x)=-6x-4$.

\item $h(t)=e^{2t}+9$
\pause \\ Because the Chain Rule gives $h'(t)=e^{2t}\cdot 2=2e^{2t}$
  we get $h''(t)=4e^{2t}$.

\item $f(s)=s\cdot \ln(s)$
\pause \\ The Product Rule gives $f'(s)=s\cdot(1/s)+\ln(s)\cdot 1=1+\ln(s)$
  and so $f''(s)=1/s$.
\end{enumerate}
\end{frame}


\begin{frame}{Second derivative and concavity}
\Tm Let $f(x)$ be continuous on an interval.
\begin{itemize}
\item If $f''(x)>0$ for all $x$ in that interval then 
  the graph of $f$ is concave up over that interval.
\item If $f''(x)<0$ in that interval then 
  the graph of $f$ is concave down there.
\end{itemize}
If at some $(c,f(c))$ the graph of a function changes concavity, either from 
up to down or from down to up, then this is an \alert{inflection point}. 
\begin{center}
  \includegraphics[height=0.4\textheight]{pix/coaster.jpg}
\end{center}
\end{frame}





\begin{frame}
\Ex Suppose that the second derivative is $f''(x)=(x+2)^2(x-1)$.
Find the intervals where the function is concave up or down.

\pause
The second derivative is zero at $x=-2$ and $x=1$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-2}$ &$f''(-3)=-4$  &$f$ is concave down  \\
    $\open{-2}{1}$       &$f''(0)=-4$   &$f$ is concave down  \\
    $\open{1}{\infty}$   &$f''(2)=16$   &$f$ is concave up  
  \end{tabular}
\end{center}
\end{frame}
% sage: def f_double(x):
% ....:     return ((x+2)^2)*(x-1)
% ....: 
% sage: f_double(-3)
% -4
% sage: f_double(0)
% -4
% sage: f_double(2)
% 16


\begin{frame}{Concavity intervals}
Suppose that a function has the given second derivative. 
Find the intervals where that function is concave up or down.

\begin{enumerate}
\item $f''(x)=3x$
\pause \\ The second derivative is $0$ only at $x=0$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{0}$ &$f''(-1)=-3$ &$f$ is concave down  \\
    $\open{0}{\infty}$  &$f''(1)=3$   &$f$ is concave up  
  \end{tabular}
\end{center}

\item $f''(x)=(x+1)x(x-2)^2$
\pause \\ The second derivative is $0$ at $x=-1$, at $x=0$, and at $x=2$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-1}$ &$f''(-2)=32$       &$f$ is concave up  \\
    $\open{-1}{0}$       &$f''(-1/2)=-25/16$ &$f$ is concave down  \\
    $\open{0}{2}$        &$f''(1)=2$         &$f$ is concave up  \\
    $\open{2}{\infty}$   &$f''(3)=12$        &$f$ is concave up  
  \end{tabular}
\end{center}
\end{enumerate}
\end{frame}
% sage: def f_double(x):
% ....:     return (x+1)*x*((x-2)^2)
% ....: 
% sage: f_double(-2)
% 32
% sage: f_double(-1/2)
% -25/16
% sage: f_double(1)
% 2
% sage: f_double(3)
% 12


% \begin{frame}{Second Derivative test}
% \Tm Let $f$ be continouous near some input~$c$ where $f'(c)=0$.
% If $f''(c)>0$ then $c$ is a local minimum.
% If $f''(c)<0$ then $c$ is a local maximum.
% If $f''(c)$ is zero then the test is inconclusive.
% \begin{center}
%   \vcenteredhbox{\includegraphics{asy/second_derivative006.pdf}}
%   \qquad
%   \vcenteredhbox{\includegraphics{asy/second_derivative007.pdf}}
% \end{center}


% \pause
% \Df If at some $(c,f(c))$ the graph of a function changes concavity, either from 
% up to down or from down to up, then this is an \alert{inflection point}. 
% \end{frame}




% \section{Graph sketching}

% \begin{frame}{Combining first and second derivative information}
%   \begin{center}\small
%   \begin{tabular}{r|cc}
%     \multicolumn{1}{c}{\ }
%     &\multicolumn{1}{c}{\textit{Concave up}}
%       &\multicolumn{1}{c}{\textit{Concave down}}  \\ \cline{2-3}
%     \rule{0pt}{0.8cm}
%     \textit{Increasing}
%       &\vcenteredhbox{\includegraphics{asy/second_derivative000.pdf}}  
%       &\vcenteredhbox{\includegraphics{asy/second_derivative001.pdf}}  \\
%     \rule{0pt}{0.9cm}
%     \textit{Decreasing}
%       &\vcenteredhbox{\includegraphics{asy/second_derivative002.pdf}}  
%       &\vcenteredhbox{\includegraphics{asy/second_derivative003.pdf}}  
%   \end{tabular}
% \end{center}

% % \pause
% % \textbf{Curve sketching steps}
  
% % \begin{enumerate}
% % \item Use the techniques for understanding the curve without calculus.
% % That includes finding $f(0)$ and the roots, where $f(x)=0$ if convenient.

% % \item Use the first derivative to find the intervals of increase and
% % decrease.
% % Use the second derivative to find the intervals of concavity.
% % Find the points of local minima and maxima.

% % \item Combine the information to sketch the graph.
% % \end{enumerate}
% \end{frame}



\begin{frame}
Find the intervals of concavity and the inflection points.
\begin{enumerate}
\item $3x^4-18x^2$
\pause \\ 
The derivatives are $f'(x)=12x^3-36x$ and
$36x^2-36$.
Setting it to zero gives $0=36(x^2-1)=36(x-1)(x+1)$.
So the intervals are $\open{-\infty}{-1}$, and $\open{-1}{1}$, and
$\open{1}{\infty}$.

\item $-x^3+3x^2+5x-4$
\pause\\
The derivatives are $f'(x)=-3x^2+6x+5$ and
$f''(x)=-6x+6$.
Set the second derivative to zero to get $0=-6(x-1)$, and
there are two intervals $\open{-\infty}{1}$ and $\open{1}{\infty}$.

\item $x^4-2x^3-5x+3$
\pause\\
We have $f'(x)=4x^3-6x^2-5$
and $f''(x)=12x^2-12x=12(x^2-1)=12(x-1)(x+1)$.
Set it to zero to get the intervals
$\open{-\infty}{-1}$, and $\open{-1}{1}$, and
$\open{1}{\infty}$.

% \item $\ln(x^2+4x+5)$
% \pause \\
% The first derivative is 
% \begin{equation*}
%   f'(x)=\frac{2x-4}{x^2-4x+5}
% \end{equation*}
% and the second derivative is this.
% \begin{equation*}
%   f''(x)=\frac{(x^2-4x+5)(2)-(2x-4)(2x-4)}{(x^2-4x+5)^2}
%         =\frac{(2x^2-8x+10)-(4x^2-8x-8x+16)}{(x^2-4x+5)^2}
% \end{equation*}
\end{enumerate}
\end{frame}



\begin{frame} %{Information about $f(x)=(1/3)x^3-(1/2)x^2-2x+3$}
\Ex Find the first and second derivative information for
$f(x)=(1/3)x^3-(1/2)x^2-2x+3$.
\pause
% This is a cubic so we know its large scale behavior,
% $\lim_{x\to\infty}f(x)=\infty$ and $\lim_{x\to-\infty}f(x)=-\infty$.
% \begin{center}
%   \includegraphics{asy/second_derivative008.pdf}%
% \end{center}
% It crosses the $y$~axis at $(0,3)$.
%   Finding $x$~axis crossings by solving $f(x)=0$ is not as easy.
% \end{frame}
% \begin{frame}
\begin{align*}
  f'(x)  &= x^2-x-2=(x+1)(x-2)  \\
  f''(x) &= 2x-1
\end{align*}
The critical numbers are $-1$ and $2$.
\pause
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-1}$  &$f'(-2)=4$  &$f$ is increasing  \\
    $\open{-1}{2}$        &$f'(0)=-2$  &$f$ is decreasing  \\
    $\open{2}{\infty}$    &$f'(3)=4$   &$f$ is increasing  
  \end{tabular}
\end{center}
\pause
There is one solution to $f''(x)=0$, namely $1/2$,
so these are the intervals of concavity.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{1/2}$  &$f''(0)=-1$  &$f$ is concave down  \\
    $\open{1/2}{\infty}$   &$f''(1)=1$   &$f$ is concave up  
  \end{tabular}
\end{center}

% At $x=-1$ there is a local max, at $x=2$ there is a local min.
% At $x=1/2$ there is an inflection point.
% Here are the associated outputs.
% \begin{equation*}
%   f(-1)=25/6
%   \quad
%   f(1/2)=23/12
%   \quad
%   f(2)=-1/3
% \end{equation*}
\end{frame}

% \begin{frame}
% \begin{center}\small
%   \includegraphics{asy/second_derivative009.pdf}  \\[1ex]
%   $f(x)=(1/3)x^3-(1/2)x^2-2x+3$    
% \end{center}
% \end{frame}




\begin{frame}{Practice}
Summarize the derivative information for
$f(x)=x^2-4x +3$.  
\pause
\begin{equation*}
  f'(x)=2x-4
  \qquad
  f''(x)=2
\end{equation*}
There is one critical point, $x=2$.
\pause
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{2}$     &$f'(1)=-2$     &$f$ decreasing  \\
    $\open{2}{\infty}$      &$f'(3)=2$      &$f$ increasing  \\
  \end{tabular}
\end{center}
\pause
The second derivative is never zero.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{\infty}$  &$f''(0)=2$      &$f$ concave up  \\
  \end{tabular}
\end{center}
\end{frame}



% \begin{frame}
% \Ex Find the derivative information for 
% $f(x)=2x^2-5x+3$.  
% \pause
% \begin{equation*}
%   f'(x)=4x-5
%   \qquad
%   f''(x)=4
% \end{equation*}
% There is one critical point, $x=5/4$.
% \pause
% \begin{center}\small
%   \begin{tabular}[t]{c|cc}
%     \multicolumn{1}{c}{\textit{Interval}}
%       &\multicolumn{1}{c}{\textit{Test input}}
%       &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
%     $\open{-\infty}{5/4}$     &$f'(1)=-1$     &$f$ decreasing  \\
%     $\open{5/4}{\infty}$      &$f'(2)=3$      &$f$ increasing  \\
%   \end{tabular}
% \end{center}
% \pause
% The second derivative is constant.
% \begin{center}\small
%   \begin{tabular}[t]{c|cc}
%     \multicolumn{1}{c}{\textit{Interval}}
%       &\multicolumn{1}{c}{\textit{Test input}}
%       &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
%     $\open{-\infty}{\infty}$  &$f''(0)=4$      &$f$ concave up  \\
%   \end{tabular}
% \end{center}
% \end{frame}
% sage: def f_prime(x):
% ....:     return 4*x-5
% ....: 
% sage: f_prime(1)
% -1
% sage: f_prime(2)
% 3





\begin{frame}
\Ex Find the derivative information for 
$f(x)=2x^3+3x^2+1$.  
\pause
\begin{equation*}
  f'(x)=6x^2+6x=6x(x+1)
  \qquad
  f''(x)=12x+6
\end{equation*}
There are two critical points, $x=0$ and $x=-1$.
\pause
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-1}$    &$f'(-2)=12$      &$f$ increasing  \\
    $\open{-1}{0}$          &$f'(-1/2)=-3/2$  &$f$ decreasing  \\
    $\open{0}{\infty}$      &$f'(1)=12$       &$f$ increasing  \\
  \end{tabular}
\end{center}
\pause
The second derivative has one root, $x=-1/2$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-1/2}$  &$f''(-1)=-6$     &$f$ concave down  \\
    $\open{-1/2}{\infty}$   &$f''(0)=6$       &$f$ concave up  \\
  \end{tabular}
\end{center}
\end{frame}





\begin{frame}
\Ex Find the derivative information for 
$f(x)=xe^{-x}$.  
\pause
Remember that never is a power $e^t$ negative.
For the first derivative we need the product rule.
\begin{equation*}
  f'(x)=x(-e^{-x})+e^{-x}(1)=e^{-x}\cdot(1-x)
\end{equation*}
The only critical point is $x=1$.
\pause
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{1}$     &$f'(0)=e^{-x}$    &$f$ increasing  \\
    $\open{1}{\infty}$      &$f'(2)=-e^{-x}$   &$f$ decreasing  \
  \end{tabular}
\end{center}
\pause
The second derivative also requires the product rule.
\begin{equation*}
  % f''(x)=
  (1-x)(-e^{-x})+e^{-x}(-1)
        =-e^{-x}+xe^{-x}-e^{-x}=-2e^{-x}+xe^{-x}=e^{-x}(x-2)   
\end{equation*}
The only zero is $x=2$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{2}$  &$f''(0)=-2e^{-x}$   &$f$ concave down  \\
    $\open{2}{\infty}$   &$f''(3)=2e^{-x}$    &$f$ concave up  
  \end{tabular}
\end{center}
\end{frame}




% =========== 10x^3/(x^2-1)
\begin{frame}  % {Information about $f(x)=10x^3/(x^2-1)$}
\Ex Find the derivative information for 
$f(x)=10x^3/(x^2-1)$.

\pause
% We can get its large scale behavior
% by looking at the leading terms of the numerator and denominator,
% namely that it acts like $y=10x$.
% It crosses the $y$~axis at $(0,0)$.
% It is undefined at $\pm 1$.
% \begin{center}
%   \includegraphics{asy/second_derivative010.pdf}%
% \end{center}
% \end{frame}


% \begin{frame}
% These are the derivatives.
\begin{equation*}
  f'(x)=\frac{10x^2(x^2-3)}{(x^2-1)^2}
  \qquad
  f''(x)=\frac{20x(x^2+3)}{(x^2-1)^3}
\end{equation*}
\pause
Besides $x=\pm 1$, the critical numbers are $x=0$ and $x=\pm\sqrt{3}$.
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-\sqrt{3}}$ &$f'(-2)=40/9$     &$f$ increasing  \\
    $\open{-\sqrt{3}}{-1}$      &$f'(-3/2)=-54/4$  &$f$ decreasing  \\
    $\open{-1}{0}$              &$f'(-1/2)=-110/9$ &$f$ decreasing  \\
    $\open{0}{1}$               &$f'(1/2)=-110/9$  &$f$ decreasing  \\
    $\open{1}{\sqrt{3}}$        &$f'(3/2)=-54/4$   &$f$ decreasing  \\
    $\open{\sqrt{3}}{\infty}$   &$f'(2)=40/9$      &$f$ increasing  
  \end{tabular}
\end{center}
\pause
\begin{center}\small
  \begin{tabular}[t]{c|cc}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Test input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-1}$  &$f''(-2)=280/27$      &$f$ concave down  \\
    $\open{-1}{0}$        &$f''(-1/2)=2080/27$   &$f$ concave up  \\
    $\open{0}{1}$         &$f''(1/2)=-2080/27$   &$f$ concave down  \\
    $\open{1}{\infty}$    &$f''(2)=280/27$       &$f$ concave up  
  \end{tabular}
\end{center}
\end{frame}
% sage: def f_prime(x):
% ....:     return 10*x^2*(x^2-3)/((x^2-1)^2)
% ....: 
% sage: f_prime(-2)
% 40/9
% sage: f_prime(-3/2)
% -54/5
% sage: f_prime(-1/2)
% -110/9
% sage: f_prime(1/2)
% -110/9
% sage: f_prime(3/2)
% -54/5
% sage: f_prime(2)
% 40/9
% sage: def f_double(x):
% ....:     return 20*x*(x^2+3)/((x^2-1)^3)
% ....: 
% sage: f_double(-2)
% -280/27
% sage: f_double(-1/2)
% 2080/27
% sage: f_double(1/2)
% -2080/27
% sage: f_double(2)
% 280/27

% \begin{frame}
% \begin{center}\small
%   \includegraphics{asy/second_derivative011.pdf}  \\[1ex]
%   $f(x)=10x^3/(x^2-1)$  
% \end{center}
% \end{frame}





% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
