// chain_rule.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for graphic command
settings.render=0;

unitsize(1cm);

// Set LaTeX defaults
// texpreamble("\usepackage{ccfonts}");

string OUTPUT_FN = "chain_rule%03d";

import graph;

real e = exp(1);


// ================ product rule example =======
real f0(real x) {return (1/10)*exp(x^2);}
real tangent0(real x) {return (2*e/10)*(x-1)+(e/10);}

picture pic;
int picnum = 0;

size(pic,0,5cm,keepAspect=true);


real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=8;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f0,xleft-0.1,xright+0.1), black);
draw(pic, graph(tangent0,0.25,1.75), highlight_color);

// filldraw(pic, circle((1,6),0.05), highlight_color, highlight_color);
// label(pic,  "$(a,f(a))$", (2,4), SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



