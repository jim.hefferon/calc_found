\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 5.5:\ \ Fundamental Theorem of Calculus}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}{Fundamental Theorem of Calculus}
\Tm Let $f(t)$ be continuous on the interval $\closed{a}{b}$.
Then the definite integral equals this,
\begin{equation*}
  \int_{x=a}^{b}f(x)\,dx
  =F(b)-F(a)
\end{equation*}
where $F(x)$   
is any antiderivative of $f(x)$, that is, where $F'(x)=f(x)$.

\medskip
We use a vertical bar notation or square brackets notation for the difference.
\begin{equation*}
  \Bigl. F(x)\Bigr|_{x=a}^b=
  \biggl[ F(x)\biggr]_{x=a}^b\!= 
  F(b)-F(a)
\end{equation*}
\end{frame}

\begin{frame}\vspace*{-1ex}
\Ex Evaluate the definite integral.
\begin{equation*}
  \int_0^4 2x\,dx
\end{equation*}
\pause
Because an antiderivative of $f(x)=2x$ is $F(x)=x^2$, the FTC gives this.
\begin{equation*}
  \int_0^4 2x\,dx=\biggl[ x^2\biggr]_0^4
  =\bigl(4^2\biggr)-\bigl(0^2\biggr)
  =16
\end{equation*}

\pause
It doesn't matter which antiderivative you use.
Another antiderivative is $G(x)=x^2+5$.
\begin{equation*}
  \int_0^4 2x\,dx=\biggl[ x^2+5\biggr]_0^4
  =\bigl(4^2+5\bigr)-\bigl(0^2+5\bigr)
  =16
\end{equation*}

\Ex Evaluate the definite integral.
\begin{equation*}
  \int_0^3 5\,dx
\end{equation*}
\pause
An antiderivative of $f(x)=5$ is $F(x)=5x$ so the FTC gives this.
\begin{equation*}
  \int_0^3 5\,dx=\biggl[ 5x\biggr]_0^3
  =\bigl(5\cdot 3\bigr)-\bigl(5\cdot 0\bigr)
  =15
\end{equation*}
\end{frame}

\begin{frame}
Evaluate each definite integral.
\begin{enumerate}
\item $\displaystyle \int_3^6 x^2\,dx$
\pause

\item $\displaystyle \int_1^3 \frac{1}{x^2}\,dx$
\pause

\item $\displaystyle \int_2^3 \frac{1}{x}\,dx$
\pause

\item $\displaystyle \int_0^2 3e^x\,dx$
\pause

\item $\displaystyle \int_1^5 x^2+3x\,dx$
\pause

\item $\displaystyle \int_{-1}^4 5x^3-1\,dx$

\end{enumerate}
\end{frame}





\begin{frame}
\Ex Find the area under $f(x)=x^2$ between $x=0$ and~$x=1$.

\pause
\begin{center}
  \includegraphics{asy/ftc019.pdf}
\end{center}
\begin{equation*}
  \int_{x=0}^{1}x^2\,dx
  =\biggl[\frac{1}{3}\cdot x^3\biggr]_{x=0}^{1}
  =\frac{1}{3}(1)^3-\frac{1}{3}(0)^3
  =\frac{1}{3}
\end{equation*}
\end{frame}


\begin{frame}
\Ex Find the area under $f(x)=1-x^2$ between $x=0$ and~$x=2$.
Use the signed area.

\pause
\begin{equation*}
  \int_{x=0}^{2}1-x^2\,dx
  =\biggl[x-\frac{x^3}{3}\biggr]_{x=0}^{2}
  =\bigl(2-\frac{2^3}{3}\bigr)-\bigl(0-\frac{0^3}{3}\bigr)
  =\frac{-2}{3}
\end{equation*}
\begin{center}
  \includegraphics{asy/ftc035.pdf}
\end{center}
\end{frame}


\begin{frame}
\Ex Find the geometric area under $f(x)=1-x^2$ between $x=0$ and~$x=2$.
That is, count all areas as positive.

\pause
\begin{center}
  \includegraphics{asy/ftc035.pdf}
\end{center}
\begin{align*}
  \int_{x=0}^{1}1-x^2\,dx
  +(-1)\cdot\int_{x=1}^{2}1-x^2\,dx   
  &=\biggl[x-\frac{x^3}{3}\biggr]_{x=0}^{1}
    -\biggl[x-\frac{x^3}{3}\biggr]_{x=1}^{2}      \\
  &=\bigl[\bigl(1-\frac{1^3}{3}\bigr)-\bigl(0-\frac{0^3}{3}\bigr)\bigr]  \\
  &\qquad
  -\bigl[\bigl(2-\frac{2^3}{3}\bigr)-\bigl(1-\frac{1^3}{3}\bigr)\bigr]=2 
\end{align*}
\end{frame}


\begin{frame}{Definite integration formulas}
The Fundamental Theorem says that the antidifferentiation formulas
apply to definite integrals.
\begin{center}\small
  \begin{tabular}{@{}l@{\hspace{3em}}l@{}}
    $\displaystyle \int_a^b k\,dx=\biggl[ kx\biggr]_a^b$ 
     &$\displaystyle \int_a^b x^n\,dx=\biggl[ \frac{1}{n+1}x^{n+1}\biggr]_a^b \quad n\neq 1$ \\
    % $\displaystyle \int_a^b \cos x\,dx=\biggl[ \sin x\biggr]_a^b$ 
    %  &$\displaystyle \int_a^b \sin x\,dx=\biggl[ -\cos x\biggr]_a^b$ \\
    $\displaystyle \int_a^b e^x\,dx=\biggl[ e^x\biggr]_a^b$ 
     &$\displaystyle \int_a^b \frac{1}{x}\,dx=\biggl[ \ln |x|\biggr]_a^b$ 
  \end{tabular}
\end{center}
Definite integrals play nice with constant multiplication, as
well as with addition and subtraction.
\begin{itemize}
\item 
${\displaystyle
  \int_a^b k\cdot f(x)\,dx=k\cdot\int_a^b f(x)\,dx
}$

\item  
${\displaystyle
  \int_a^b f(x)+ g(x)\,dx=\int_a^b f(x)\,dx +\int_a^b g(x)\,dx
}$ 
\item  
${\displaystyle
  \int_a^b f(x)- g(x)\,dx=\int_a^b f(x)\,dx -\int_a^b g(x)\,dx
}$ 
\end{itemize}
\end{frame}




\section{Definite integrals and the substitution method}

\begin{frame}
\Ex
This integral needs substitution.
\begin{equation*}
  \int_{x=0}^4 \frac{x}{x^2+10}\,dx
\end{equation*}
The first way is to use substitution to get the antiderivative as a function
of~$x$, and then apply the FTC.
Here take $u=x^2+10$ so $du/dx=2x$ and $(1/2)\,du=x\,dx$.
This is the indefinite integral.
\begin{equation*}
  \int \frac{x}{x^2+10}\,dx
  =\int \frac{1}{u}\cdot \frac{1}{2}\,du
  =\frac{1}{2}\int \frac{1}{u}\,du
  =\frac{1}{2}\ln|u|+C
  =\frac{1}{2}\ln|x^2+10|+C
\end{equation*}
The Fundamental Theorem gives this.
\begin{equation*}
  \int_{x=0}^4 \frac{x}{x^2+10}\,dx
  =\biggl[ \frac{1}{2}\ln|x^2+10|\biggr]_{x=0}^4\!\!
  =\bigl( \frac{1}{2}\ln(26)\bigr)-\bigl( \frac{1}{2}\ln(10)\bigr)
  =\frac{1}{2}\ln(\frac{26}{10})
\end{equation*}
\end{frame}


\begin{frame}
The second way is the one we will usually use.
We rewrite the entire integral, including the limits of integration.
\begin{align*}
  \int_{x=0}^4 \frac{x}{x^2+10}\,dx
  &=\int_{u=10}^{26} \frac{1}{u}\cdot \frac{1}{2}\,du  \\
  &=\frac{1}{2}\int_{u=10}^{26} \frac{1}{u}\,du        \\
  &=\frac{1}{2}\biggl[\ln|u|\biggr]_{u=10}^{26}        \\
  &=\frac{1}{2}\ln(\frac{26}{10})
\end{align*}
\end{frame}



\begin{frame}{Practice}
For each definite integral, use the second method of substution, then
apply the Fundamental Theorem.
\begin{enumerate}
\item $\displaystyle \int_{x=0}^1 xe^{x^2}\,dx$
\pause
\item $\displaystyle \int_{x=0}^1 x\sqrt{3x^2+2}\,dx$
\pause
\item $\displaystyle \int_{1}^2 \frac{x+1}{2x^2+4x+4}\,dx$
\pause
\item $\displaystyle \int_{6}^{7} \frac{\ln(t-5)}{t-5} \,dt$
\end{enumerate}
\end{frame}



\section{Average value of a function}

\begin{frame}
Let $f$ be continuous on the interval $\closed{a}{b}$.
\begin{equation*}
  \text{Average value}
  =\frac{1}{b-a}\cdot\int_a^b f(x)\,dx
\end{equation*}
\begin{enumerate}
\item Find the average of $f(x)=x-3x^2$ over $\closed{-1}{2}$.
\item Find the average value of $g(x)=2x+7$ over $\closed{0}{5}$.
\item Find the average of $h(t)=\sqrt{t+1}$ over $\closed{3}{8}$.
\end{enumerate}
\end{frame}
\end{document}







% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 
