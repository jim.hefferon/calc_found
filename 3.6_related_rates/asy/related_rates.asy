// related_rates.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for graphic command
settings.render=0;

unitsize(1pt);

// Set LaTeX defaults
texpreamble("\usepackage{ccfonts}");

string OUTPUT_FN = "related_rates%03d";

import graph;

// some parameters
real axis_arrow_size = 0.35mm;
real axis_tick_size = 0.75mm;

pen graphpaperpen=(0.4*rgb("00BFFF")+0.6*white)
  +linewidth(0.8pt)
  +squarecap;

// ===== Stick figures
picture pic;
int picnum = 0;
size(pic,5cm);

picture runner;
label(runner,reflect((0,-10),(0,10))*shift(0,0.45cm)*graphic("running-person.eps","scale=0.5"), (0,0));


picture walker;
label(pic,reflect((0,-10),(0,10))*shift(0,0.45cm)*graphic("man-walking.eps","scale=0.6"), (0,0));


for(int i=0; i <= 23; ++i) {
  draw(pic,(i-3,0)--(i-3.6,-1),graphpaperpen);
}

add(pic,runner,(15,0));
add(pic,walker,(0,0));

// Add the labels
object Box=draw(pic,"\tiny Person x",box,(0,-1),0.5mm,white,Fill);
object Box=draw(pic,"\tiny Person y",box,(15,-1),0.5mm,white,Fill);

path base=(-3.6,0)--(20.6,0);
draw(pic,base,graphpaperpen); 

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ===== Boat puller
picture pic;
int picnum = 1;
size(pic,0,3cm);

// puller
label(pic,graphic("pulling.eps","scale=0.13"), (2.6,2.6));

// boat
label(pic,graphic("boat.eps","scale=0.13"), (6.5,1.2));

path water = (3,1)--(6,1);
draw(pic,water,blue);

// dock
path dock = (0,0)--(3,0)--(3,2)--(0,2)--cycle;
filldraw(pic,dock,gray(0.85), black);

// rope
pair rope_end_puller = (3,2.57);
pair rope_end_boat = (5.58,1.35);
// dot(pic,rope_end_puller,red);
// dot(pic,rope_end_boat,red);
path rope = rope_end_puller--rope_end_boat;
draw(pic,rope,highlight_color);

path triangle = rope_end_puller--rope_end_boat--(rope_end_puller.x,rope_end_boat.y)--cycle;
draw(pic,triangle,highlight_color);

filldraw(pic, circle(rope_end_puller,0.05),highlight_color,highlight_color);
  label(pic,"P",rope_end_puller,NE,filltype=Fill(white));
filldraw(pic, circle(rope_end_boat,0.05),highlight_color,highlight_color);
  label(pic,"B",rope_end_boat,2*N,filltype=Fill(white));
filldraw(pic, circle((rope_end_puller.x,rope_end_boat.y),0.05),highlight_color,highlight_color);
  label(pic,"D",(rope_end_puller.x,rope_end_boat.y),W,filltype=Fill(white));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ elephant ears =======
real f2(real x) {return (1/2)*x;}

picture pic;
int picnum = 2;

size(pic,0,5cm,keepAspect=true);


real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=6;
ybot=0; ytop=3;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f2,xleft-0.1,xright+0.1), highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ ellipse =======

picture pic;
int picnum = 3;

size(pic,0,2.5cm,keepAspect=true);


real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-2; ytop=2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path ellipse = scale(3,2)*circle((0,0),1);
draw(pic, ellipse, highlight_color);
filldraw(pic, circle((3,0),0.05), highlight_color, highlight_color);
label(pic, "$(3,0)$", (3,0), 2*W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");






// for(int i=0; i <= 23; ++i) {
//   draw(pic,(i-3,0)--(i-3.6,-1),graphpaperpen);
// }

// add(pic,runner,(15,0));
// add(pic,walker,(0,0));

// // Add the labels
// object Box=draw(pic,"\tiny Person x",box,(0,-1),0.5mm,white,Fill);
// object Box=draw(pic,"\tiny Person y",box,(15,-1),0.5mm,white,Fill);

// path base=(-3.6,0)--(20.6,0);
// draw(pic,base,graphpaperpen); 

// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");
