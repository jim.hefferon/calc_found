\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 4.1\ \ First derivative}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................





\section{What the first derivative says about a function}


\begin{frame}{Intervals of increase and decrease}
\Tm Let $f$ be differentiable on $\open{a}{b}$.
If $f'(x)$ is positive in that interval then $f$ is 
increasing there.
If $f'(x)$ is negative then $f$ is 
decreasing.

\Df An input $x$ such that $f'(x)=0$ is a \alert{stationary number}. 
An input~$x$ such that either $f'(x)=0$ or $f'(x)$ does not exist
is a \alert{critical number}. 
\end{frame}


\begin{frame}{Sign charts}
\Ex For $f(x)=x^2-10x-4$ find the intervals of increase and decrease.

\pause
The derivative is $f'(x)=2x-10=2(x-5)$.

We have these intervals.
\begin{center}
  \begin{tabular}{l|ll}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Trial input}}
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{5}$    &$f'(4)=-2$ &$f$ decreasing  \\
    $\open{5}{\infty}$     &$f'(6)=2$  &$f$ increasing 
  \end{tabular}
\end{center}
\pause
Recall sign charts: the critical numbers of the function~$f$ are
the partition numbers of the sign chart of $f'\!$.
\begin{center}
  \includegraphics{asy/first_derivative006.pdf}
\end{center}
This contains the same information as the table, but in a graphical form.
\end{frame}


\begin{frame}{Practice}
For the function $f(x)=x^3-27x-20$,  
give the intervals where the function is increasing and decreasing
and make the sign chart. 

\pause 
The derivative is $f'(x)=3x^2-27=3(x^2-9)=3(x+3)(x-3)$.
The critical numbers of $f$ are $x=-3$ and $x=3$.
Here are the intervals.
\begin{center}
  \begin{tabular}{l|ll}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Trial values}} 
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-3}$ &$f'(-4)=21$  &$f$ increasing\\
    $\open{-3}{3}$       &$f'(0)=-27$  &$f$ decreasing\\
    $\open{3}{\infty}$   &$f'(4)=21$   &$f$ increasing\\
  \end{tabular}
\end{center}
Here is the sign chart.
\begin{center}\small
  \includegraphics{asy/first_derivative007.pdf} 
\end{center}
\end{frame}

\begin{frame}{Practice}
For the function $f(x)=1/(x-5)$, 
give the intervals where the function is increasing and decreasing
and make the sign chart. 
\textit{Warning:} you want critical points, so points where the first
derivative is undefined or zero.


\pause 
Here is the derivative.
\begin{equation*}
  f'(x)=\underbracket{-1(x-5)^{-2}}\cdot \underbracket{1}=\frac{-1}{(x-5)^2}
\end{equation*}
There is just one critical number, $x=5$.
Here are the intervals.
\begin{center}
  \begin{tabular}{l|ll}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Trial values}} 
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{5}$ &$f'(4)=-1$  &$f$ decreasing\\
    $\open{5}{\infty}$  &$f'(6)=-1$ &$f$ decreasing\\
  \end{tabular}
\end{center}
Here is the sign chart.
\begin{center}\small
  \includegraphics{asy/first_derivative012.pdf} 
\end{center}
\end{frame}


\begin{frame}{Practice}
For the function $f(x)=x^{2/3}$, 
give the intervals where the function is increasing and decreasing
and make the sign chart. 

\pause 
The derivative is $f'(x)=(2/3)x^{-1/3}=2/(3\sqrt[3]{x})$.
The only critical number of $f$ is $x=0$, where the derivative is undefined.
Here are the intervals.
\begin{center}
  \begin{tabular}{l|ll}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Trial values}} 
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{0}$  &$f'(-1)=-2/3$ &$f$ decreasing\\
    $\open{0}{\infty}$   &$f'(1)=2/3$    &$f$ increasing\\
  \end{tabular}
\end{center}
Here is the sign chart.
\begin{center}\small
  \includegraphics{asy/first_derivative011.pdf} 
\end{center}
\end{frame}


\begin{frame}{Recall: local optima and global optima}
A number~$c$ is a \alert{local maximum} if 
$f(c)\geq f(x)$ for $x$'s near to $c$.
Similarly,~$c$ is a \alert{local minimum} if 
$f(c)\leq f(x)$ for $x$'s near to $c$.

A number~$c$ is a \alert{global maximum} or \alert{absolute maximum}
if $f(c)\geq f(x)$ for all inputs~$x$, and it 
is a \alert{global minimum} or \alert{absolute minimum}
if $f(c)\leq f(x)$ for all inputs~$x$. 
\end{frame}


\begin{frame}{First Derivative test for local optima}
\Tm Suppose that $f$ be continuous, and let $c$ be a critical number.
\begin{itemize}
\item
If the derivative $f'$ changes from positive to negative at $c$,
so that $f$ changes from increasing to decreasing,
then the function $f$ has a local maximum there.  
\item
Similarly, if $f'$ changes from negative to positive at $c$,
so that it changes from decreasing to increasing,
then $f$ has a local minimum there.  
\item
Otherwise,
if the derivative does not change sign, then the function does not have
a local optima at~$c$.
\end{itemize}

\begin{center}
  \includegraphics{asy/first_derivative013.pdf}%
  \qquad
  \includegraphics{asy/first_derivative014.pdf}%
\end{center}
\end{frame}



\begin{frame}
\alert{Strategy:} given a function, 
first find the critical numbers.
Determine the sign of the derivative between those numbers,
and then use the test to find the local optima. 

\pause\medskip
\Ex If possible, apply the First Derivative test to $f(x)=x^3-27x-20$.

We already know that the derivative is $f'(x)=3x^2-27=3(x^2-9)=3(x+3)(x-3)$,
that the critical numbers of $f$ are $x=-3$ and $x=3$,
and that these are the intervals of increase and decrease.
\begin{center}
  \begin{tabular}{l|ll}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Trial values}} 
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{-3}$ &$f'(-4)=21$  &$f$ increasing\\
    $\open{-3}{3}$       &$f'(0)=-27$  &$f$ decreasing\\
    $\open{3}{\infty}$   &$f'(4)=21$   &$f$ increasing\\
  \end{tabular}
\end{center}

\pause
Conclusion: At $x=-3$ the function has a local maximum, and
at $x=3$ it has a local minimum.
\end{frame}

\begin{frame}
\begin{center}\small
  \begin{tabular}{c}
  \includegraphics{asy/first_derivative008.pdf} \\[1.5ex]
  $f(x)=x^3-27x-20$
  \end{tabular}
\end{center}
At the critical numbers $c_1=-3$ and $c_2=3$, the function is
stationary, that is, the derivative is zero.
\end{frame}



\begin{frame}
\Ex If possible, apply the First Derivative test to  $f(x)=x^{2/3}$. 

\pause
The derivative is $f'(x)=(2/3)x^{-1/3}=2/3\sqrt[3]{x}$.
The only critical number of $f$ is $x=0$, where the derivative is undefined.
Here are the intervals.
\begin{center}
  \begin{tabular}{l|ll}
    \multicolumn{1}{c}{\textit{Interval}}
      &\multicolumn{1}{c}{\textit{Trial values}} 
      &\multicolumn{1}{c}{\textit{Conclusion}}  \\ \hline
    $\open{-\infty}{0}$  &$f'(-1)=-2/3$ &$f$ decreasing\\
    $\open{0}{\infty}$   &$f'(1)=2/3$    &$f$ increasing\\
  \end{tabular}
\end{center}

\pause 
The function has a local minimum at $x=0$.
\end{frame}



\begin{frame}
\begin{center}\small
  \includegraphics{asy/first_derivative010.pdf} \\[1ex]
  $f(x)=x^{2/3}$
\end{center}

This example differs from the prior one in that at the critical number $c=0$
the derivative does not exist.
\end{frame}




\begin{frame}{Practice}
\Ex If possible, apply the First Derivative test to $f(x)=1/(x-5)$. 

\pause 
The test does not apply because the function is not continuous at
all inputs.
\begin{center}\small
  \includegraphics{asy/first_derivative009.pdf}
\end{center}
Note that it has no local optima.
\end{frame}





\begin{frame}{Practice}
Analyze these.
\begin{enumerate}
\item $x^3-3x^2-9x-1$
\pause \\ 
The derivative is $3(x+1)(x-3)$.
From left to right, the function is increasing, decreasing, and then increasing
again.
So at $-1$ there is a local maximum and at $3$ there is a local minimum. 
\item $(1/3)x^3-x^2+x-4$
\pause \\
The derivative is $(x-1)^2\!$.
The only critical number is $c=1$ but there is no sign change there, so it
is not a local optimum.
\item $x+(1/x)$
\pause \\
The derivative is $(x+1)(x-1)/x^2\!$.
There are three critical points.
In the four intervals the function is increasing, decreasing, decreasing again,
and increasing.
So $-1$ is a local maximum, and $1$ is a local minimum. 
\end{enumerate}
\end{frame}


\begin{frame}{More practice}
%\begin{enumerate}
%\item 
$\displaystyle 5x^2-10x-3$
\pause\\
The derivative is $10x-10=10(x-1)$ so there is a one critical number, $x=1$.
The table shows that the
function is decreasing and then increasing, so the First Derivative
Test says that it has a minimum at $x=1$.
% \item $\displaystyle -3x^2+12x-5$
% \pause\\
% The derivative is $-6x+12=-6(x+2)$.
% There is one critical point, at $x=-2$.
% The table shows the function increasing, then decreasing, so the 
% First Derivative Test says it has a maximum at $x=-2$.
%\end{enumerate}
  
\end{frame}


% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
