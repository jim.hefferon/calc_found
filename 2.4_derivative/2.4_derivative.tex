\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title[Derivative] % (optional, use only with long paper titles)
{Section 2.4\ \ Derivative}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\subject{Continuity}
% This is only inserted into the PDF information catalog. Can be left
% out. 

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}{Average rate of change}
This shows two points on a curve, $(a,f(a))$ and $(a+h,f(a+h))$.
\begin{center}
  \only<1>{\includegraphics{asy/derivative000.pdf}}%
  \only<2->{\includegraphics{asy/derivative001.pdf}}
\end{center}
\only<2->{The \alert{average rate of change of $f$ from $x=a$ to $x=a+h$} is 
\begin{equation*}
  \frac{f(a+h)-f(a)}{(a+h)-a}
  =\frac{f(a+h)-f(a)}{h}
\end{equation*}
(when $h\neq 0$).
It is the slope of the \alert{secant line}.
}
\end{frame}

\begin{frame}
\textsc{Example}
The revenue from the sales of $x$-many baby car seats is modeled by
$R(x)=60x-0.025x^2$.
Find the average rate of change if $a=1000$ and $h=50$.
\pause
\begin{equation*}
  \frac{R(1050)-R(1000)}{50}
  =\frac{35437.50-35000.00}{50}
  =8.75
\end{equation*}
% sage: x = var('x')
% sage: R(x) = 60*x-0.025*x^2
% sage: R(1050)
% 35437.5000000000
% sage: R(1000)
% 35000.0000000000
% sage: (R(1050)-R(1000))/50
% 8.75000000000000


\Ex Find the average rate of change:
% \begin{enumerate}
% \item $f(x)=5-x$, $a=2$, $h=1$
% \pause \\ First, $f(a+h)=f(3)=-4$ and $f(a)=1$.
%   Then $(f(a+h)-f(a))/h$ is $(-4-1)/1$, which is $-5$.
% sage: f(x)=5-x^2
% sage: a,h=var('a,h')
% sage: a=2
% sage: h=1
% sage: f(a+h)
% -4
% sage: f(a)
% 1
% sage: (f(a+h)-f(a))/h
% -5
% \item 
$f(x)=2x+8$, $a=3$, $h=0.5$.
\pause \\ We have $f(a+h)=15$ and $f(a)=14$.
  Then $(f(a+h)-f(a))/h$ is $(15-14)/0.5$, which is $2$.
% sage: f(x) = 2*x+8
% sage: a=3
% sage: h=0.5
% sage: f(a+h)
% 15.0000000000000
% sage: f(a)
% 14
% sage: (f(a+h)-f(a))/h
% 2.00000000000000
% \end{enumerate}
\end{frame}

\begin{frame}{Average velocity}
Drop a ball from $100$~meters above the Earth's surface and its
height after $x$~seconds will be $y=100- 4.9 x^2$.
\begin{center}\small
  \vcenteredhbox{\includegraphics[height=0.75in]{pix/pisa.png}}
  \hspace*{3em}
  \begin{tabular}{c|c}  
   \multicolumn{1}{c}{\begin{tabular}{@{}c@{}} 
                             \textit{Time} \\ 
                             $x$ 
                      \end{tabular}}  
   &\multicolumn{1}{c}{\begin{tabular}{@{}c@{}}
                          \textit{Height}  \\ 
                          $h(x)$
                       \end{tabular}}  
    \\ \hline
    $1$    &$95.1$   \\
    $2$    &$80.4$   \\
    $3$    &$55.9$   \\
    $4$    &$21.6$   
  \end{tabular}
\end{center}
Find the average rate of change of height, using $a=0$ and $h=1$.
\pause
\begin{equation*}
  \frac{h(1)-h(0)}{1-0}=\frac{95.1-100}{1}=-4.9
\end{equation*}
This is the \alert{average velocity}, in meters per second, 
over the first second.

Find the average velocity over the next second, using $a=1$ and $h=1$.
\pause
\begin{equation*}
  \frac{h(2)-h(1)}{2-1}=\frac{80.4-95.1}{1}=-14.7
\end{equation*}  
\end{frame}




\begin{frame}{Instantaneous rate of change}
\begin{center}
  \only<1>{\includegraphics{asy/derivative002.pdf}}%    
  \only<2>{\includegraphics{asy/derivative003.pdf}}%    
  \only<3>{\includegraphics{asy/derivative004.pdf}}%    
  \only<4->{\includegraphics{asy/derivative005.pdf}}%    
\end{center}
\begin{center}
  \begin{tabular}{lc|c}
    \multicolumn{2}{c}{\textit{Inputs}}
      &\multicolumn{1}{c}{\textit{Rate of change}} \\ \hline
    \uncover<2->{$2$ &$1$       &$\frac{4.5-3}{2-1}=1.5$} \\
    \uncover<3->{$1.1$ &$1$     &$\frac{3.195-3}{1.1-1}=1.95$} \\
    \uncover<4->{$1.01$ &$1$    &$\frac{3.0019995-3}{1.01-1}=1.999499$} \\
    \uncover<4->{$1.001$ &$1$   &$\frac{3.000199995-3}{1.001-1}=1.999950$} 
  \end{tabular}
\end{center}
\end{frame}

\begin{frame}
\alert{The instantaneous rate of change of $f(x)$ at $x=a$} is
\begin{equation*}
  \lim_{h\to 0}\text{(average rate of change)}
  =\lim_{h\to 0}\frac{f(a+h)-f(a)}{h}
\end{equation*}
if that limit exists.
We often just say \alert{rate of change}.
We also call this the \alert{derivative} of the function at~$a$ and denote it
as here.
\begin{equation*}
  f'(a)  
  \quad\text{or}\quad
  \left.\frac{df}{dx}\right|_{x=a}
\end{equation*}

\medskip
The procedure for finding the rate of change of $f$ at $a$:
\begin{enumerate}
\item Find $f(a+h)$ and $f(a)$.
\item Use those to write the \alert{difference quotient}.
  \begin{equation*}
    \frac{f(a+h)-f(a)}{h} 
  \end{equation*}
\item Do the algebra to take the limit as $h\to 0$.
\end{enumerate}
\end{frame}


\begin{frame}{Practice with $f(a+h)$}

Find each $f(a+h)$.
\begin{enumerate}
% \item $f(x)=2x+1$, $a=3$
% \pause \\ $2\cdot(3+h)+1=6+h+1=7+h$
\item $f(x)=x^2$, $a=1$
\pause \\ $(1+h)^2=1^2+2\cdot 1\cdot h+h^2=1+2h+h^2$
\item $f(x)=x^2+x$, $a=3$
\pause \\ $(3+h)^2+(3+h)=(9+6h+h^2)+3+h=12+7h+h^2$
\end{enumerate}
\end{frame}


% \begin{frame}{Example}
% Use $f(x)=5-3x$.
% Follow the three steps to find the rate of change at~$a=1$.
% % \begin{enumerate}
% % \item $a=1$
% \pause \\(1) We have $f(a+h)=f(1+h)=5-3\cdot (1+h)=5-(3+3h)=2-3h$.
%   We also have $f(a)=f(1)=5-3\cdot 1=2$. \\
%  (2) The fraction is this.
%  \begin{equation*}
%    \frac{f(a+h)-f(a)}{h}
%    =\frac{(2-3h)-2}{h}
%  \end{equation*}  
%   (3) Here is the limit.
%  \begin{equation*}
%    \lim_{h\to 0}\frac{f(a+h)-f(a)}{h}
%    =\lim_{h\to 0}\frac{(2-3h)-2}{h}
%    =\lim_{h\to 0}\frac{-3h}{h}
%    =\lim_{h\to 0}-3
%    =-3
%  \end{equation*}  
% \item $a=-1$
% \pause \\ (1) $f(a+h)=5-3\cdot(-1+h)=8-3h$ and $f(-1)=8$.
%   (2)~$(f(a+h)-f(a))/h$ is $(8-3h-8)/h$
%   (3) Here is the limit.
%   \begin{equation*}
%     \lim_{h\to 0}\frac{f(a+h)-f(a)}{h}
%     =\lim_{h\to 0}\frac{8-3h-8}{h}
%     =\lim_{h\to 0}\frac{-3h}{h}
%     =\lim_{h\to 0}-3=-3
%   \end{equation*}
% \end{enumerate}
% \end{frame}

\begin{frame}{Example}
Use $f(x)=4+x^2$.
Follow the three steps to find the rate of change at each~$a$.
\begin{enumerate}
\item $a=2$
\pause \\(1) We have $f(a+h)=f(2+h)=4+(2+h)^2=4+(4+4h+h^2)=8+4h+h^2$.
  We also have $f(a)=f(2)=4+2^2=8$. \\
 (2) The fraction is
   $\frac{(8+4h+h^2)-8}{h}$. 
  (3) Here is the limit.
 \begin{equation*}
   \lim_{h\to 0}\frac{(8+4h+h^2)-8}{h}
   =\lim_{h\to 0}\frac{4h+h^2}{h}
   =\lim_{h\to 0}\frac{h(4+h)}{h}
   =\lim_{h\to 0}4+h
   =4
 \end{equation*}  
\item $a=-1$
\pause \\(1) We have $f(a+h)=f(-1+h)=4+(-1+h)^2=4+(1-2h+h^2)=5-2h+h^2$.
  We also have $f(a)=f(-1)=4+(-1)^2=5$. \\
 (2) The fraction is
   $\frac{(5-2h+h^2)-5}{h}$. 
  (3) Here is the limit.
 \begin{equation*}
   \lim_{h\to 0}\frac{(5-2h+h^2)-5}{h}
   =\lim_{h\to 0}\frac{-2h+h^2}{h}
   =\lim_{h\to 0}\frac{h(-2+h)}{h}
   =-2
 \end{equation*}  
\end{enumerate}
\end{frame}




\begin{frame}{Slope of tangent line}
Find the slope of the line tangent to the curve 
$f(x)=5-x^2$ at the point where $x=1$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/derivative006.pdf}}
  \qquad
  \begin{minipage}{0.4\linewidth}
    \begin{equation*}
      \text{slope}=\lim_{h\to 0}\frac{f(1+h)-f(1)}{h}
    \end{equation*}
  \end{minipage}
\end{center}
\pause
We have $f(1+h)=5-(1+h)^2=5-(1+2h+h^2)=4-2h-h^2$.
And, $f(1)=4$.  
\end{frame}

\begin{frame}\vspace*{-1.5ex}
  \begin{align*}
    \lim_{h\to 0}\frac{f(1+h)-f(1)}{h}
    &=
    \lim_{h\to 0}\frac{(4-2h-h^2)-(4)}{h}  \\
    &=
    \lim_{h\to 0}\frac{(-2h-h^2}{h} \\
    &=
    \lim_{h\to 0}\frac{h\cdot(-2-h}{h}
    =
    \lim_{h\to 0}-2-h=-2
  \end{align*}
That is the slope of the red line.
It is the rate of change of the function at the instant when $x=1$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/derivative006.pdf}}
\end{center}
\end{frame}



\begin{frame}{Derivative}
Find the rate of change of the function $f(x)=x^2$ at any point~$x$.
This problem differs from prior ones in that we are not working at a fixed
single number~$a$, we are working with a variable~$x$.
\pause
First, $f(x+h)=(x+h)^2=x^2+2xh+h^2$.
Also, $f(x)=x^2$.
\begin{align*}
  \lim_{h\to 0}\frac{f(x+h)-f(x)}{h}
  &=\lim_{h\to 0}\frac{(x+h)^2-x^2}{h}     \\
  &=\lim_{h\to 0}\frac{(x^2+2xh+h^2)-x^2}{h}     \\
  &=\lim_{h\to 0}\frac{2xh+h^2}{h}     \\
  &=\lim_{h\to 0}\frac{h(2x+h)}{h}     \\
  &=\lim_{h\to 0}2x+h=2x
\end{align*}
\end{frame}


% \begin{frame}
% \alert{The function derived from $f(x)$},
% also called \alert{the derivative of $f$} is
% \begin{equation*}
%   \lim_{h\to 0}\frac{f(x+h)-f(x)}{h}
% \end{equation*}
% if the limit exists.
% If so, we write
% \begin{equation*}
%   f'(x)
%   \quad\text{or}\quad
%   \frac{df}{dx}
% \end{equation*}
% and say that the function $f$ is \alert{differentiable}.
% \end{frame}


\begin{frame}{Example}
Take $f(x)=x+x^2$. 
Find the derivative.
\pause
First, $f(x+h)=(x+h)+(x+h)^2$, which equals
$(x+h)+(x^2+2xh+h^2)$.
\begin{align*}
  \lim_{h\to 0}\frac{f(x+h)-f(x)}{h}
  &=\lim_{h\to 0}\frac{(x+h+x^2+2xh+h^2)-(x+x^2)}{h}     \\
  &=\lim_{h\to 0}\frac{h+2xh+h^2}{h}     \\
  &=\lim_{h\to 0}\frac{h(1+2x+h)}{h}     \\
  &=\lim_{h\to 0}1+2x+h             \\
  &=1+2x
\end{align*}
We write $f'(x)=1+2x$.
% For instance, the derivative at the input $-3$ is $f'(-3)=1+2(-3)=-5$.
\end{frame}


% \begin{frame}{More derivatives}
% Find the derivative of each.
% Use that to compute the rate of change at inputs $a=1$ and $a=2$.
% \begin{enumerate}
% \item $g(x)=4-6x$
% \pause \\ $g'(x)=-6$, $g'(1)=-6$, $g'(2)=-6$
% \item $c(x)=9$
% \pause \\ $c'(x)=0$, $c'(1)=0$, $c'(2)=0$
% \item $f(x)=2x^2+8$
% \pause \\ $f'(x)=4x$, $f'(1)=4$, $f'(2)=8$ 
% \end{enumerate} 
%\end{frame}

% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
