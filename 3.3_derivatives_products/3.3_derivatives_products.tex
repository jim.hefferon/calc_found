\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 3.3\ \ Derivatives of products and quotients}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................
\begin{frame}{What we know and what we don't}
We know how to take the derivative of powers such as $x^5$, and of $e^x$
and $\ln x$.
We know how to take the derivative of a sum or difference.

But we do not yet know how to take 
the derivative of the product of two
functions
\begin{equation*}
  \frac{d\,f(x)\cdot g(x)}{dx}
\end{equation*}
or of the quotient of two
functions
\begin{equation*}
  \frac{d\,f(x)/g(x)}{dx}
\end{equation*}
or of the composition of two
functions.
\begin{equation*}
  \frac{d\,f(\, g(x) \,)}{dx}
\end{equation*}
\end{frame}

\begin{frame}{Product Rule}
Where $f$ and $g$ are functions, this is the derivative of their product.
\begin{equation*}
  \frac{d\,f(x)\cdot g(x)}{dx}
  =f(x)\cdot\frac{dg(x)}{dx}
    +g(x)\cdot\frac{df(x)}{dx}
\end{equation*}
Restated, where $h(x)=f(x)\cdot g(x)$ then this holds.
\begin{equation*}
  h'(x)=f(x)\cdot g'(x)+g(x)\cdot f'(x)
\end{equation*}
\pause 
\Ex Let $h(x)=x^2\cdot (x^5+x)$.
We could in principle multiply it out and then take the derivative of
the resulting polynomial, but instead we apply the Product Rule.
\begin{equation*}
  \frac{d\,h(x)}{dx}
  =x^2\cdot (5x^4+1)+(x^5+x)\cdot 2x
\end{equation*}

\pause
\Ex Let $h(x)=xe^x$.
This we could not multiply out.
\begin{equation*}
  \frac{d\,h(x)}{dx}
  =x\cdot (e^x)+e^x\cdot (1)
  =e^x\cdot(x+1)
\end{equation*}
\end{frame}

\begin{frame}{Practice}
Find the derivative, and then simplify.
\begin{enumerate}
\item $\displaystyle (5x+2)\cdot(-x-1)$
\pause
\begin{equation*}
  (5x+2)\cdot (-1)+(-x-1)\cdot 5
  =-5x-2-5x-5=-10x-7
\end{equation*}
\item $\displaystyle 5x^2\cdot (x^3+2)$
\pause
\begin{equation*}
  5x^2\cdot 3x^2+(x^3+2)\cdot 10x
  =15x^4+10x^4+20x
  =25x^4+20x
\end{equation*}
\item $\displaystyle x\ln x$
\pause
\begin{equation*}
  x\cdot \frac{1}{x}+\ln(x)\cdot 1
  =1+\ln(x)
\end{equation*}
\item $\displaystyle 3x \cdot e^{x}$
\pause
\begin{equation*}
  3x\cdot e^x+e^x\cdot 3
  =3e^x(x+1)
\end{equation*}
\end{enumerate}
\end{frame}



\begin{frame}
\Ex Find the line tangent to the graph of $(x^2+1)\cdot 3^x$ at $x=1$.
\pause
The derivative is this.
\begin{equation*}
  (x^2+1)\cdot \ln(3)3^x+3^x\cdot(2x)
  =3^x\cdot(\ln(3)(x^2+1)+2x)
\end{equation*}
At $x=1$ we get this slope for the tangent line.
\begin{equation*}
  3\cdot(2\ln(3)+2)=6\ln(3)+6\approx 42.592
\end{equation*}
And the equation of the tangent line is this.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/derivatives_products000.pdf}}
  \quad
  \begin{minipage}{0.4\linewidth}
    \begin{equation*}
       y-6=(6\ln(3)+6)\cdot(x-1)
    \end{equation*}
  \end{minipage}
\end{center}
\end{frame}




\begin{frame}{Quotient Rule}
Where $f$ and $g$ are functions, this is the derivative of the quotient.
\begin{equation*}
  \frac{d\,{\displaystyle\left(\frac{f(x)}{g(x)}\right)}}{dx}
  =\frac{{\displaystyle g(x)\cdot\frac{df(x)}{dx}} 
         - {\displaystyle f(x)\cdot\frac{d\,g(x)}{dx}}}{g(x)^2}
\end{equation*}

\Ex Find the derivative of $\displaystyle \frac{3x}{2x+1}$.
\pause
\begin{equation*}
  % \frac{(2x+1)\cdot \frac{d\,3x}{dx}-3x\cdot \frac{d\,(2x+1)}{dx}}{(2x+1)^2}=
  \frac{(2x+1)\cdot 3-3x\cdot 2}{(2x+1)^2}
  =\frac{6x+3-6x}{(2x+1)^2}
  =\frac{3}{(2x+1)^2}
\end{equation*}

\Ex Find the derivative of $\displaystyle \frac{3x-4}{2x-3}$.
\pause
\begin{equation*}
  \frac{(2x-3)\cdot 3-(3x-4)\cdot 2}{(2x-3)^2}
  =\frac{6x-9-(6x-8)}{(2x-3)^2}
  =\frac{-1}{(2x-3)^2}
\end{equation*}
\end{frame}




\begin{frame}{Practice}\vspace*{-1ex}
Find the derivative.
\begin{enumerate}
\item $\displaystyle (3x+2)/(4x-5)$
\pause
\begin{equation*}
  \frac{(4x-5)\cdot 3-(3x+2)\cdot 4}{(4x-5)^2}
  =\frac{12x-15-(12x+8)}{(4x-5)^2}
  =\frac{-23}{(4x-5)^2}
\end{equation*}
\item $e^x/(x+1)$
\pause
\begin{equation*}
  % \frac{{\displaystyle (x+1)\cdot \frac{d\,e^x}{dx} - e^x\cdot \frac{d\,(x+1)}{dx}}}{(x+1)^2}=
  \frac{(x+1)\cdot e^x - e^x\cdot 1}{(x+1)^2}
  =\frac{xe^x}{(x+1)^2}
\end{equation*}
\item $\displaystyle 5x^2/(x^3+2)$
\pause
\begin{equation*}
  \frac{(x^3+2)\cdot 10x-(5x^2)\cdot 3x^2}{(x^3+2)^2}
  =\frac{10x^4+20x-15x^4}{(x^3+2)^2}
  =\frac{-5x^4+20x}{(x^3+2)^2}
\end{equation*}
% \item
% Find $f'(2)$ where $f(x)=1/(x+4)$.
% \pause
% \begin{equation*}
%   % \frac{\displaystyle (x+4)\cdot\frac{d\,1}{dx} - 1\cdot\frac{d\,(x+4)}{dx}}{(x+4)^2}=
%   \frac{(x+4)\cdot 0 - 1\cdot 1}{(x+4)^2}
%   =\frac{-1}{(x+4)^2}
% \end{equation*}
% So $f'(2)=-1/36$.
\end{enumerate}
\end{frame}


% \begin{frame}
% % \begin{enumerate}
% % \item Find the derivative of $f(x)=(x^2+3x+4)/(x^2-1)$. 
% % \begin{equation*}
% %   \uncover<2->{
% %  f'(x)=\frac{(x^2-1)(2x+3)- (x^2+3x+4)(2x)}{(x^2-1)^2}    
% %   =\frac{-3x^2-10x-3}{(x^2-1)^2}}    
% % \end{equation*}

% Find the equation of the line tangent to $f(x)=(x^2+1)/(x^2-4)$
% at $(3,1)$.
% \pause
% First find the derivative.
% \begin{equation*}
%   \frac{(x^2-4)\cdot 2x-(x^2+1)\cdot 2x}{(x^2-4)^2}
%   =\frac{2x^3-8x-2x^3-2x}{(x^2-4)^2}
%   =\frac{-10x}{(x^2-4)^2}
% \end{equation*}
% At $(3,2)$ the slope of the tangent line is $-30/25=-6/5$.
% In the equation of a line we get
% $
%   y-2=\frac{-6}{5}(x-3)
% $.
% \begin{center}
%   \includegraphics{asy/derivatives_products001.pdf}
% \end{center}
% \end{frame}



% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
