\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 5.4:\ \ Definite integral}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{textcomp}
\usepackage{listings}
\lstset{basicstyle=\ttfamily\scriptsize,
        upquote=true,
        language=Python,
        keepspaces=true}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................


\section{Approximating the signed region}

\begin{frame}{Accumulating a continuous quantity}
\Ex A house with solar panels sometimes will take energy
from the power grid
and sometimes instead supplies energy to the grid.
\begin{center}
  \vcenteredhbox{\includegraphics[height=0.25\textheight]{pix/rooftop-solar-panels.jpg}}
  \qquad
  \vcenteredhbox{\includegraphics{asy/definite_integral000.pdf}}
\end{center}
The units for the graph's vertical axis is kilowatts
and the units for the horizontal axis is hours,
so each box is a kilowatt-hour.
Boxes below the axis are negative for when the house takes in energy,
while those above the axis are positive for when the house produces. 
What is the total between $t=0$ and $t=x$?
\end{frame}


\begin{frame}{Approximating with boxes}
\Ex Approximate the area under the curve $f(x)=1+x^2$
between $x=1$ and $x=2$ by dividing the base into four same-length subintervals
$\closed{x_i}{x_{i+1}}$.
Over each, erect a rectangle of height $f(x_i)$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/definite_integral012.pdf}}%
\end{center}
\pause
\begin{center}\small
  \begin{tabular}{r|llcc}
    \multicolumn{1}{c}{\textit{Interval $i$}}
      &\multicolumn{1}{c}{\textit{$x_i$}}
      &\multicolumn{1}{c}{\textit{$x_{i+1}$}}
      &\multicolumn{1}{c}{\textit{$f(x_i)$}}
      &\multicolumn{1}{c}{\textit{Area, rounded}} \\ \hline
    $1$ &$1$     &$5/4$  &$2$     &$0.500$   \\
    $2$ &$5/4$   &$3/2$  &$41/16$ &$0.641$   \\
    $3$ &$3/2$   &$7/4$  &$13/4$  &$0.812$   \\
    $4$ &$7/4$   &$2$    &$65/16$ &$1.020$   \\
  \end{tabular}
\end{center}
The total is about $2.969$.
\end{frame}
% sage: def f(x):
% ....:     return 1+x^2
% ....: 
% sage: accumulated_area=0
% sage: for i in range(4):
% ....:     xi = 1+i/4
% ....:     xiplus = 1+(i+1)/4
% ....:     mi = (xi+xiplus)/2
% ....:     box_area = f(xi)*(1/4)
% ....:     accumulated_area = accumulated_area+box_area
% ....:     print("i=",i," xi=",xi," xiplus=",xiplus," f(xi)=",f(xi) ,"box_area=",
% ....: n(box_area,digits=3))
% ....: 
% i= 0  xi= 1  xiplus= 5/4  f(xi)= 2 box_area= 0.500
% i= 1  xi= 5/4  xiplus= 3/2  f(xi)= 41/16 box_area= 0.641
% i= 2  xi= 3/2  xiplus= 7/4  f(xi)= 13/4 box_area= 0.812
% i= 3  xi= 7/4  xiplus= 2  f(xi)= 65/16 box_area= 1.02
% sage: n(accumulated_area)
% 2.96875000000000


\begin{frame}
Get a better approximation with more intervals.
\begin{center}\small
  \begin{tabular}{r|cccc}
    \multicolumn{1}{c}{\textit{Interval $i$}}
      &\multicolumn{1}{c}{\textit{$x_i$}}
      &\multicolumn{1}{c}{\textit{$x_{i+1}$}}
      &\multicolumn{1}{c}{\textit{$f(x_i)$}}
      &\multicolumn{1}{c}{\textit{Area, rounded}} \\ \hline
    $1$ &$1$     &$9/8$   &$2$       &$0.250$   \\
    $2$ &$9/8$   &$5/4$   &$145/64$  &$0.283$   \\
    $3$ &$5/4$   &$11/8$  &$41/16$   &$0.320$   \\
    $4$ &$11/8$  &$3/2$   &$185/64$  &$0.361$   \\
    $5$ &$3/2$   &$13/8$  &$13/4$    &$0.406$   \\
    $6$ &$13/8$  &$7/4$   &$233/64$  &$0.455$   \\
    $7$ &$7/4$   &$15/8$  &$65/16$   &$0.508$   \\
    $8$ &$15/8$  &$2$     &$289/64$  &$0.564$   
  \end{tabular}
\end{center}
The total is about $3.148$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/definite_integral015.pdf}}%
\end{center}
\end{frame}
% sage: accumulated_area=0
% sage: for i in range(8):
% ....:     xi = 1+i/8
% ....:     xiplus = 1+(i+1)/8
% ....:     mi = (xi+xiplus)/2
% ....:     box_area = f(xi)*(1/8)
% ....:     accumulated_area = accumulated_area+box_area
% ....:     print("i=",i," xi=",xi," xiplus=",xiplus," f(xi)=",f(xi) ,"box_area=",
% ....: n(box_area,digits=3))
% ....: 
% i= 0  xi= 1  xiplus= 9/8  f(xi)= 2 box_area= 0.250
% i= 1  xi= 9/8  xiplus= 5/4  f(xi)= 145/64 box_area= 0.283
% i= 2  xi= 5/4  xiplus= 11/8  f(xi)= 41/16 box_area= 0.320
% i= 3  xi= 11/8  xiplus= 3/2  f(xi)= 185/64 box_area= 0.361
% i= 4  xi= 3/2  xiplus= 13/8  f(xi)= 13/4 box_area= 0.406
% i= 5  xi= 13/8  xiplus= 7/4  f(xi)= 233/64 box_area= 0.455
% i= 6  xi= 7/4  xiplus= 15/8  f(xi)= 65/16 box_area= 0.508
% i= 7  xi= 15/8  xiplus= 2  f(xi)= 289/64 box_area= 0.564
% sage: n(accumulated_area)
% 3.14843750000000


\begin{frame}\vspace*{-1ex}
\Ex Approximate the area under the curve $f(x)=1+x^2$
between $x=1$ and $x=2$ by dividing the base into four same-length subintervals.
Over each, erect a rectangle whose height is $f(m_i)$ where $m_i$ is the 
midpoint of the subinterval.
\begin{center}
  \includegraphics{asy/definite_integral013.pdf}
\end{center}
\pause
\begin{center}
  \begin{tabular}{r|ccccc}
    \multicolumn{1}{c}{\textit{Interval $i$}}
      &\multicolumn{1}{c}{\textit{$x_i$}}
      &\multicolumn{1}{c}{\textit{$x_{i+1}$}}
      &\multicolumn{1}{c}{\textit{$m_{i}$}}
      %&\multicolumn{1}{c}{\textit{$f(m_i)$}}
      &\multicolumn{1}{c}{\textit{Box area, rounded}} \\ \hline
    $1$ &$1$     &$5/4$  &$9/8$   &$0.566$   \\
    $2$ &$5/4$   &$3/2$  &$11/8$  &$0.723$   \\
    $3$ &$3/2$   &$7/4$  &$13/8$  &$0.910$   \\
    $4$ &$7/4$   &$2$    &$15/8$  &$1.130$   \\
  \end{tabular}       
\end{center}
The total is about $3.328$.
\end{frame}
% sage: accumulated_area=0
% sage: for i in range(4):
% ....:     xi = 1+i/4
% ....:     xiplus = 1+(i+1)/4
% ....:     mi = (xi+xiplus)/2
% ....:     box_area = f(mi)*(1/4)
% ....:     accumulated_area = accumulated_area+box_area
% ....:     print("i=",i," xi=",xi," xiplus=",xiplus," mi=",mi,"box_area=",n(box_a
% ....: rea,digits=3))
% ....: 
% i= 0  xi= 1  xiplus= 5/4  mi= 9/8 box_area= 0.566
% i= 1  xi= 5/4  xiplus= 3/2  mi= 11/8 box_area= 0.723
% i= 2  xi= 3/2  xiplus= 7/4  mi= 13/8 box_area= 0.910
% i= 3  xi= 7/4  xiplus= 2  mi= 15/8 box_area= 1.13
% sage: accumulated_area
% 213/64
% sage: n(accumulated_area)
% 3.32812500000000


\begin{frame}\vspace*{-1ex}
\Ex Again approximate the area under $f(x)=1+x^2$
between $x=1$ and $x=2$ by dividing the base into four same-length subintervals.
This time, erect a rectangle whose height is the point on the right side
of the subinterval, $f(x_{i+1})$.
\begin{center}
  \includegraphics{asy/definite_integral014.pdf}
\end{center}
\pause
\begin{center}
  \begin{tabular}{r|cccc}
    \multicolumn{1}{c}{\textit{Interval $i$}}
      &\multicolumn{1}{c}{\textit{$x_i$}}
      &\multicolumn{1}{c}{\textit{$x_{i+1}$}}
      &\multicolumn{1}{c}{\textit{$f(m_i)$}}
      &\multicolumn{1}{c}{\textit{Area, rounded}} \\ \hline
    $1$ &$1$    &$5/4$  &$41/64$  &$0.641$   \\
    $2$ &$5/4$  &$3/2$  &$13/16$  &$0.812$   \\
    $3$ &$3/2$  &$7/4$  &$97/16$  &$1.020$   \\
    $4$ &$7/4$  &$2$    &$137/16$ &$1.250$   \\
  \end{tabular}       
\end{center}
The total is about $3.72$.
\end{frame}
% sage: def f(x):
% ....:     return(1+x^2)
% ....: 
% sage: accumulated_area = 0
% sage: for i in range(4):
% ....:     xi = 1+i/4
% ....:     xiplus = 1+(i+1)/4
% ....:     box_area = f(xiplus)*(1/4)
% ....:     accumulated_area = accumulated_area+box_area
% ....:     print("i=",i," xi=",xi," xiplus=",xiplus,"box_area=",n(box_area,digits
% ....: =3))
% ....: 
% i= 0  xi= 1  xiplus= 5/4 box_area= 0.641
% i= 1  xi= 5/4  xiplus= 3/2 box_area= 0.812
% i= 2  xi= 3/2  xiplus= 7/4 box_area= 1.02
% i= 3  xi= 7/4  xiplus= 2 box_area= 1.25
% sage: accumulated_area
% 119/32
% sage: n(accumulated_area)
% 3.71875000000000




\begin{frame}[fragile]{Remark: computer code}
Approximating areas in this way is well suited to a computer.
This is the script that produced the prior table, in the computer algebra 
system \textit{Sage}.
\begin{lstlisting}
sage: def f(x):
....:     return(1+x^2)
....: 
sage: accumulated_area = 0
sage: for i in range(4):
....:     xi = 1+i/4
....:     xiplus = 1+(i+1)/4
....:     box_area = f(xiplus)*(1/4)
....:     accumulated_area = accumulated_area+box_area
....:     print("i=",i," xi=",xi," xiplus=",xiplus,"box_area=",n(box_area,digits=3))
....: 
i= 0  xi= 1  xiplus= 5/4 box_area= 0.641
i= 1  xi= 5/4  xiplus= 3/2 box_area= 0.812
i= 2  xi= 3/2  xiplus= 7/4 box_area= 1.02
i= 3  xi= 7/4  xiplus= 2 box_area= 1.25
sage: accumulated_area
119/32
sage: n(accumulated_area)
3.71875000000000
\end{lstlisting}
\end{frame}




\begin{frame}{Approximation scheme}
Divide the interval $\closed{a}{b}$
into many subintervals of the form $\closed{x_i}{x_{i+1}}$.
We make them of equal width, $\Delta x$. 
On each subinterval erect a rectangle, a box, from the axis to the function,
according to some plan.
We will often use either the left-hand plan making the height of the box
be~$f(x_i)$, or the midpoint plan making the height of the box be
$f((x_i+x_{i+1})/2)$, or the right-hand plan making the height of the
box be $f(x_{i+1})$.
(In general we can use the $c$-of-the-way plan where the height of the box is
$f(x_i+c\cdot(x_{i+1}-x_i))$.)

Then the sum of the box areas approximates the area of the entire region.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/definite_integral011.pdf}}%
\end{center}
\end{frame}


% \begin{frame}{Practice}
% Use $f(x)=25-3x^2$.
% Partition $\closed{0}{12}$ and approximate the region by dividing into
% four subintervals and using the given plan.
% \begin{enumerate}
% \item Use the left hand plan, $c=0$.
% \item Use the midpoint plan, $c=1/2$.
% \item Use the one-third-of-the-way plan, $c=1/3$.
% \end{enumerate}
% \end{frame}

% \begin{frame}
% This is the left-hand plan.
% \begin{center}
%   \begin{tabular}{r|cccc}
%     \multicolumn{1}{c}{\textit{Interval $i$}}
%       &\multicolumn{1}{c}{\textit{$x_i$}}
%       &\multicolumn{1}{c}{\textit{$x_{i+1}$}}
%       &\multicolumn{1}{c}{\textit{$f(x_i)$}}
%       &\multicolumn{1}{c}{\textit{Area, rounded}} \\ \hline
%     $1$ &$0$  &$3$   &$25$   &$75.0$   \\
%     $2$ &$3$  &$6$   &$-2$   &$-6.00$   \\
%     $3$ &$6$  &$9$   &$-83$  &$-249$   \\
%     $4$ &$9$  &$12$  &$-218$ &$-654$   \\
%   \end{tabular}       
% \end{center}
% The total is $-834$.
% % sage: def f(x):
% % ....:     return(25-3*x^2)
% % ....: 
% % sage: accumulated_area=0
% % sage: for i in range(4):
% % ....:     xi = 0+i*3
% % ....:     xiplus = 0+(i+1)*3
% % ....:     mi = (xi+xiplus)/2
% % ....:     box_area = f(xi)*(3)
% % ....:     accumulated_area = accumulated_area+box_area
% % ....:     print("i=",i," xi=",xi," xiplus=",xiplus," f(xi)=",f(xi) ,"box_area=",
% % ....: n(box_area,digits=3))
% % ....: 
% % i= 0  xi= 0  xiplus= 3  f(xi)= 25 box_area= 75.0
% % i= 1  xi= 3  xiplus= 6  f(xi)= -2 box_area= -6.00
% % i= 2  xi= 6  xiplus= 9  f(xi)= -83 box_area= -249.
% % i= 3  xi= 9  xiplus= 12  f(xi)= -218 box_area= -654.
% % sage: accumulated_area
% % -834

% \pause
% This is the midpoint plan.
% \begin{center}
%   \begin{tabular}{r|cccc}
%     \multicolumn{1}{c}{\textit{Interval $i$}}
%       &\multicolumn{1}{c}{\textit{$x_i$}}
%       &\multicolumn{1}{c}{\textit{$x_{i+1}$}}
%       &\multicolumn{1}{c}{\textit{$f(m_i)$}}
%       &\multicolumn{1}{c}{\textit{Area, rounded}} \\ \hline
%     $1$ &$0$  &$3$   &$73/4$     &$54.8$   \\
%     $2$ &$3$  &$6$   &$-143/4$   &$-107.$   \\
%     $3$ &$6$  &$9$   &$-575/4$   &$-431.$   \\
%     $4$ &$9$  &$12$  &$-1223/4$  &$-917.$   \\
%   \end{tabular}       
% \end{center}
% The total is $-1401$.
% % sage: def f(x):
% % ....:     return(25-3*x^2)
% % ....: 
% % sage: accumulated_area=0
% % sage: for i in range(4):
% % ....:     xi = 0+i*3
% % ....:     xiplus = 0+(i+1)*3
% % ....:     mi = (xi+xiplus)/2
% % ....:     box_area = f(mi)*(3)
% % ....:     accumulated_area = accumulated_area+box_area
% % ....:     print("i=",i," xi=",xi," xiplus=",xiplus," f(mi)=",f(mi) ,"box_area=",
% % ....: n(box_area,digits=3))
% % ....: 
% % i= 0  xi= 0  xiplus= 3  f(mi)= 73/4 box_area= 54.8
% % i= 1  xi= 3  xiplus= 6  f(mi)= -143/4 box_area= -107.
% % i= 2  xi= 6  xiplus= 9  f(mi)= -575/4 box_area= -431.
% % i= 3  xi= 9  xiplus= 12  f(mi)= -1223/4 box_area= -917.
% % sage: accumulated_area
% % -1401
% \end{frame}



% \begin{frame}
% This is the $c=1/3$-of-the-way plan.
% \begin{center}
%   \begin{tabular}{r|cccc}
%     \multicolumn{1}{c}{\textit{Interval $i$}}
%       &\multicolumn{1}{c}{\textit{$x_i$}}
%       &\multicolumn{1}{c}{\textit{$x_{i+1}$}}
%       &\multicolumn{1}{c}{\textit{$f(x_i+(1/3)(x_{i+1}-x_i))$}}
%       &\multicolumn{1}{c}{\textit{Area, rounded}} \\ \hline
%     $1$ &$0$  &$3$   &$22$    &$66.0$   \\
%     $2$ &$3$  &$6$   &$-23$   &$-69.0$   \\
%     $3$ &$6$  &$9$   &$-122$  &$-366.$   \\
%     $4$ &$9$  &$12$  &$-275$  &$-825.$   \\
%   \end{tabular}       
% \end{center}
% The total is $-1194$.
% % sage: accumulated_area=0
% % sage: for i in range(4):
% % ....:     xi = 0+i*3
% % ....:     xiplus = 0+(i+1)*3
% % ....:     ci = xi+(1/3)*(xiplus-xi)
% % ....:     box_area = f(ci)*(3)
% % ....:     accumulated_area = accumulated_area+box_area
% % ....:     print("i=",i," xi=",xi," xiplus=",xiplus," f(ci)=",f(ci) ,"box_area=",
% % ....: n(box_area,digits=3))
% % ....: 
% % ....: 
% % i= 0  xi= 0  xiplus= 3  f(ci)= 22 box_area= 66.0
% % i= 1  xi= 3  xiplus= 6  f(ci)= -23 box_area= -69.0
% % i= 2  xi= 6  xiplus= 9  f(ci)= -122 box_area= -366.
% % i= 3  xi= 9  xiplus= 12  f(ci)= -275 box_area= -825.
% % sage: accumulated_area
% % -1194
% \end{frame}



\section{Getting the exact answer}

\begin{frame}{Exact amount}\vspace*{-1ex}
We take that the approximation improves as $\Delta x$ gets smaller.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/definite_integral012.pdf}}%
  \qquad
  \vcenteredhbox{\includegraphics{asy/definite_integral015.pdf}}%
\end{center}
Consequently, we define the volume of the region
to be the limit of the approximating sum,
as the width of the subintervals goes to zero.
\begin{equation*}
  \text{Total signed area}
                   =\lim_{\Delta x\to 0}\;\biggl(\sum_{i}f(x_i)\cdot (x_{i+1}-x_i)\biggr)
\end{equation*}
We call this the \alert{definite integral}.                   
\begin{equation*}
  \int_{x=a}^b f(x)\, dx
                   =\lim_{\Delta x\to 0}\;\biggl(\sum_{i}f(x_i)\cdot (x_{i+1}-x_i)\biggr)
\end{equation*}
% This shows the left hand scheme, but
% it proves to not matter because any of our schemes give the same answer.
\end{frame}





\begin{frame}{Sweeping}
We will often picture what we are doing as here.
We want the area of the region below the function's graph and above the axis,
between $x=a$ and $x=b$.
We sweep out the red vertical columns from right to left.
\begin{center}
  \only<1>{\vcenteredhbox{\includegraphics{asy/definite_integral001.pdf}}}%
  \only<2>{\vcenteredhbox{\includegraphics{asy/definite_integral002.pdf}}}%
  \only<3>{\vcenteredhbox{\includegraphics{asy/definite_integral003.pdf}}}%
  \only<4>{\vcenteredhbox{\includegraphics{asy/definite_integral004.pdf}}}%
  \only<5>{\vcenteredhbox{\includegraphics{asy/definite_integral005.pdf}}}%
  \only<6>{\vcenteredhbox{\includegraphics{asy/definite_integral006.pdf}}}%
  \only<7>{\vcenteredhbox{\includegraphics{asy/definite_integral007.pdf}}}%
  \only<8>{\vcenteredhbox{\includegraphics{asy/definite_integral008.pdf}}}%
  \only<9>{\vcenteredhbox{\includegraphics{asy/definite_integral009.pdf}}}%
  \only<10->{\vcenteredhbox{\includegraphics{asy/definite_integral010.pdf}}}%
\end{center}
\pause
% Each column adds an infinitesimal area to the whole. 
\only<10->{We say that we have \alert{integrated} these columns to make the 
whole.
We have accumulated the continuous quantity of area.}
\end{frame}




\begin{frame}{Prototype}
We will often draw this picture.
Sometimes we show one column, which we call a 
\alert{slice},
or a \alert{Reimann box}.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/definite_integral016.pdf}}%
\end{center}  

As to the notation:
Leibniz thought of this as taking as an infinite sum of infinitesimal summands
so his notation for integrals uses a letter `S' 
and he wrote the area of each column as the product $f(x)\, dx$.
\begin{equation*}
  \text{Area}=\int_{x=a}^{b}f(x)\,dx
\end{equation*}
\end{frame}



\begin{frame}
\Ex Express as an integral the signed area between $f(x)=x^2+1$ and the $x$~axis,
from 
$x=1$ to $x=2$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/definite_integral022.pdf}}
  \qquad
  $\displaystyle \int_{x=1}^2x^2+1\,dx$
  \end{center}
\end{frame}

\begin{frame}
\Ex Express as an integral the signed area between $f(x)=e^x$ and the $x$~axis,
from 
$x=-1$ to $x=1$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/definite_integral023.pdf}}
  \qquad
  $\displaystyle \int_{x=-1}^1e^x\,dx$
\end{center}
\end{frame}


\begin{frame}
\Ex Express as an integral the signed area between $f(x)=1-x^2$ and the $x$~axis,
from 
$0$ to~$2$.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/definite_integral024.pdf}}
  \qquad
  $\displaystyle \int_{x=0}^21-x^2\,dx$
\end{center}
\end{frame}


\begin{frame}
\Ex Express as an integral the geometric area between $f(x)=1-x^2$
and the $x$~axis, from $0$ to~$2$.
``Geometric area'' means we want all areas to count as positive.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/definite_integral024.pdf}}
\end{center}
This is the integral of the absolute value.
\begin{align*}
  \int_{x=0}^2|f(x)|\,dx
  &=\int_{x=0}^11-x^2\,dx
   +\int_{x=1}^2-(1-x^2)\,dx  \\
  &=\int_{x=0}^11-x^2\,dx
   +\int_{x=1}^2x^2-1\,dx
\end{align*}
\end{frame}



\begin{frame}{Practice}
Sketch each region.
Express the signed area as an integral.
\begin{enumerate}
\item The region between $y=x^2$ and the $x$~axis,
from $x=-3$ to $x=-1$.

\pause
\begin{equation*}
  \int_{-3}^{-1}x^2\,dx
\end{equation*}

\item The region between $f(x)=\ln(x)$ and the $x$~axis,
from $x=1$ to $x=3$.

\pause
\begin{equation*}
  \int_{1}^{3}\ln(x)\,dx
\end{equation*}

\item The region between $y=4-x^2$ and the $x$~axis,
from $x=1$ to $x=5$.

\pause
\begin{equation*}
  \int_{1}^{5}4-x^2\,dx
\end{equation*}
\end{enumerate}
\end{frame}





\begin{frame}{Integration is a kind of addition}
Use these regions to answer the questions.
\begin{center}
   \vcenteredhbox{\includegraphics{asy/definite_integral021.pdf}}% 
\end{center}
\begin{enumerate}
\item $\displaystyle \int_{x=a}^c f(x)\,dx$
\pause\\
Signed area $A+B$.
\item $\displaystyle \int_{x=c}^e f(x)\,dx$
\pause\\
Signed area $C+D$.
\item Is $\displaystyle \int_{x=b}^c f(x)\,dx$ positive or negative?
\pause\\
Negative.
\end{enumerate}
\end{frame}


\begin{frame}
\begin{center}
   \vcenteredhbox{\includegraphics{asy/definite_integral021.pdf}}% 
\end{center}
\begin{enumerate} \setcounter{enumi}{3}
\item $\displaystyle \int_{x=a}^f f(x)\,dx$
\pause\\
Signed area $A+B+C+D+E$.
\item Signed area $C+D+E$
\pause\\
$\displaystyle \int_{x=c}^f f(x)\,dx$
\item $\displaystyle \int_{x=d}^c f(x)\,dx$
\pause\\
Negative of the signed area $C$.
\end{enumerate}
\end{frame}



\begin{frame}{Properties}
\begin{itemize}
\item $\displaystyle  \int_{x=a}^af(x)\,dx=0$
\item $\displaystyle \int_{x=b}^af(x)\,dx=-\int_{x=b}^af(x)\,dx$

\item $\displaystyle \int_{x=a}^cf(x)\,dx=\int_{x=a}^bf(x)\,dx+\int_{x=b}^cf(x)\,dx$
\item $\displaystyle \int_{x=a}^bf(x)+g(x)\,dx=\int_{x=a}^bf(x)\,dx+\int_{x=a}^bg(x)\,dx$
\item $\displaystyle \int_{x=a}^bf(x)-g(x)\,dx=\int_{x=a}^bf(x)\,dx-\int_{x=a}^bg(x)\,dx$
\item $\displaystyle \int_{x=a}^bk\cdot f(x)\,dx=k\cdot \int_{x=a}^bf(x)\,dx$
\end{itemize}

\end{frame}






% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 
