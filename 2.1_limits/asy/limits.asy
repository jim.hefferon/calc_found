// derivatives.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for graphic command
settings.render=0;

unitsize(1cm);

// Set LaTeX defaults
// texpreamble("\usepackage{ccfonts}");

string OUTPUT_FN = "limits%03d";

import graph;



// ================ 5-(1/2)*(x-3)^2 =======
real f(real x) {return 5-(1/2)*(x-3)^2;}

picture pic;
int picnum = 0;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

real f(real x) {return 5-(1/2)*(x-3)^2;}

// Label L_fcn = Label("$f(x)$", align=1.65*W, position=MidPoint, black,
// 		    filltype=Fill(white));
draw(pic, graph(f,-0.1,5.1), highlight_color);
filldraw(pic, circle((1,3),0.05), white, black);
label(pic,  "$(1,3)$", (1,3), SE);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ............................
// sage: def f(x):
// ....:     return 5-(1/2)*(x-3)^2
// ....: 
// sage: f(3)
// 5
// sage: f(1)
// 3
// sage: f(2)
// 9/2
pair anchor_pt = (1,3);
pair first_secant_pt = (2,4.5);
real first_secant(real x) {return ((4.5-3)/(2-1))*(x-1)+3;}


picture pic;
int picnum = 1;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}


draw(pic, graph(f,-0.1,5.1), black);
draw(pic, graph(first_secant,0.5,2.5), highlight_color);

filldraw(pic, circle(anchor_pt,0.05), white, black);
label(pic,  "$(1,3)$", anchor_pt, SE);
filldraw(pic, circle(first_secant_pt,0.05), white, black);
label(pic,  "$(2,4.5)$", first_secant_pt, SE);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ............................
// sage: def f(x):
// ....:     return 5-(1/2)*(x-3)^2
// ....: 
// sage: f(1.1)
// 3.19500000000000
pair anchor_pt = (1,3);
pair second_secant_pt = (1.1,3.195);
real second_secant(real x) {return ((3.195-3)/(1.1-1))*(x-1)+3;}


picture pic;
int picnum = 2;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}


draw(pic, graph(f,-0.1,5.1), black);
draw(pic, graph(second_secant,0.25,1.75), highlight_color);

filldraw(pic, circle(anchor_pt,0.05), white, black);
label(pic,  "$(1,3)$", anchor_pt, SE);
filldraw(pic, circle(second_secant_pt,0.05), white, black);
label(pic,  "$(1.1,3.195)$", second_secant_pt, (0.85,0.1));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ............................
// sage: def f(x):
// ....:     return 5-(1/2)*(x-3)^2
// ....: 
// sage: f(1.1)
// 3.19500000000000
pair anchor_pt = (1,3);
real tangent_line(real x) {return 2*(x-1)+3;}


picture pic;
int picnum = 3;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f,-0.1,5.1), black);
draw(pic, graph(tangent_line,0.25,1.75), highlight_color);

// label(pic,graphic("climbing.png","width=0.80cm,angle=10"),
//       anchor_pt-(0.1,-0.15));

filldraw(pic, circle(anchor_pt,0.05), white, black);
label(pic,  "$(1,3)$", anchor_pt, SE);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ Initial examples of limits =======
real f0(real x) {return x^2;}

picture pic;
int picnum = 4;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=6;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

// Label L_fcn = Label("$f(x)$", align=1.65*W, position=MidPoint, black,
// 		    filltype=Fill(white));
draw(pic, graph(f0,xleft-0.1,2.5), highlight_color);
// sage: n(sqrt(6))
// 2.44948974278318

filldraw(pic, circle((2,4),0.05), highlight_color, highlight_color);
label(pic,  "$(2,4)$", (2,4), SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ================ Second example of limits =======
real f1(real x) {return 5-(1/2)*(x-3)^2;}


picture pic;
int picnum = 5;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

// Label L_fcn = Label("$f(x)=\sqrt{x+7}$", align=3*N, position=Relative(0.2),
// 		    highlight_color,
// 		    filltype=Fill(white));
draw(pic, graph(f1,xleft-0.1,xright+0.1), // L=L_fcn,
     highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ================ Third example of limits =======
real f3(real x) {return x^3;}
real f4(real x) {return 3-x;}

picture pic;
int picnum = 6;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=-1; ytop=3;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

// Label L_fcn = Label("$f(x)=\sqrt{x+7}$", align=3*N, position=Relative(0.2),
// 		    highlight_color,
// 		    filltype=Fill(white));
draw(pic, graph(f3,xleft-0.1,1), // L=L_fcn,
     highlight_color);
draw(pic, graph(f4,1,xright+0.2), // L=L_fcn,
     highlight_color);

filldraw(pic, circle((1,1),0.05), white, highlight_color);
label(pic,  "$(1,1)$", (1,1), NW);
filldraw(pic, circle((1,2),0.05), highlight_color, highlight_color);
label(pic,  "$(1,2)$", (1,2), NE);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ One-sided limits =======
real f2(real x) {return sqrt(x-2);}

picture pic;
int picnum = 7;
size(pic,5cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=0; ytop=4;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

// Label L_fcn = Label("$f(x)=\sqrt{x+7}$", align=3*N, position=Relative(0.2),
// 		    highlight_color,
// 		    filltype=Fill(white));
draw(pic, graph(f2,2,xright+0.2), // L=L_fcn,
     highlight_color);

filldraw(pic, circle((2,0),0.05), highlight_color, highlight_color);
label(pic,  "$(2,0)$", (2,0), SE);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ 1/(x-2) =======
real f8(real x) {return 1/(x-2);}

picture pic;
int picnum = 8;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-4; ytop=4;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f8,xleft-0.1,1.76), // L=L_fcn,
     highlight_color);
draw(pic, graph(f8,2.24,xright+0.1), // L=L_fcn,
     highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ================ Random graph =======
picture pic;
int picnum = 9;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-2; ytop=4;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic,(xleft-0.2,1)--(1,1),highlight_color);
filldraw(pic, circle((1,1),0.05), white, highlight_color);
// label(pic,  "$(1,1)$", (1,1), NW);
filldraw(pic, circle((1,2),0.05), highlight_color, highlight_color);
draw(pic,(1,2)..{right}(2,3)::{down}(2.9,-2),highlight_color);
draw(pic,(3.1,-2){up}::(5,4),highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

