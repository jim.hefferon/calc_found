// second_derivative.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "second_derivative%03d";
real PI = acos(-1);

import graph;



// ==== inc/dec and concavity ====

// ....... inc and up ............
picture pic;
int picnum = 0;
size(pic,0,1cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf = (0.5,0.5){E}::(1,0.75)::(1.5,1.5);

draw(pic, grf, highlight_color);

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.25, xmax=xright+.25,
//   p=currentpen,
//       ticks=NoTicks,
//   arrow=Arrows(TeXHead));

// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.25, ymax=ytop+0.25,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... inc and down ............
picture pic;
int picnum = 1;
size(pic,0,1cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf = (0.5,0.5)::(1,1.25)::(1.5,1.5);

draw(pic, grf, highlight_color);

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.25, xmax=xright+.25,
//   p=currentpen,
//       ticks=NoTicks,
//   arrow=Arrows(TeXHead));

// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.25, ymax=ytop+0.25,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... dec and down ............
picture pic;
int picnum = 2;
size(pic,0,1cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf = (0.5,1.5)::(1,0.75)::(1.5,0.5);

draw(pic, grf, highlight_color);

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.25, xmax=xright+.25,
//   p=currentpen,
//       ticks=NoTicks,
//   arrow=Arrows(TeXHead));

// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.25, ymax=ytop+0.25,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ....... dec and up ............
picture pic;
int picnum = 3;
size(pic,0,1cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf = (0.5,1.5)::(1,1.25)::(1.5,0.5);

draw(pic, grf, highlight_color);

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.25, xmax=xright+.25,
//   p=currentpen,
//       ticks=NoTicks,
//   arrow=Arrows(TeXHead));

// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.25, ymax=ytop+0.25,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ======= concave up ======
real f4(real x) {return 2*(x-1)^2+1;}

picture pic;
int picnum = 4;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(f4, 0.5, 1.5), highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

real f5(real x) {return -2*(x-1)^2+1.5;}

// ......... concave down ...........
picture pic;
int picnum = 5;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(f5, 0.5, 1.5), highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ======= Second derivative test ======
real f6(real x) {return 2*(x-1)^2+1;}

picture pic;
int picnum = 6;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(f6, 0.5, 1.5), highlight_color);
filldraw(pic, circle((1,f6(1)), 0.025), highlight_color, highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ......... concave down ...........
real f7(real x) {return -2*(x-1)^2+1.5;}

picture pic;
int picnum = 7;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(f7, 0.5, 1.5), highlight_color);
filldraw(pic, circle((1,f7(1)), 0.025), highlight_color, highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ==== (1/3)x^3-(1/2)x^2-2x+3 ====

// ....... large scale behavior .......
picture pic;
int picnum = 8;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-5; ytop=5;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf_l = (-2,-3.5)..(-2.5,-4)..(-3,-5); 
path grf_r = (2,3.5)..(2.5,4)..(3,5); 

draw(pic, grf_l, highlight_color, Arrow);
draw(pic, grf_r, highlight_color, Arrow);

filldraw(pic, circle((0,3),0.07), highlight_color,  highlight_color);
label(pic,  "$(0,3)$",
        (0,3), W, filltype=Fill(white));

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// label(pic,  "\begin{tabular}[b]{l}$f(x)=x^{2k}$\\ $k\geq 1$\end{tabular}",
//         (0,1.5), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ....... graph .......
real f9(real x) {return (1/3)*x^3-(1/2)*x^2-2*x+3;}

picture pic;
int picnum = 9;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2.65; xright=3;
ybot=-2; ytop=4;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic,(-1,ybot)--(-1,ytop),dotted);
draw(pic,(1/2,ybot)--(1/2,ytop),dotted);
draw(pic,(2,ybot)--(2,ytop),dotted);

draw(pic, graph(f9,xleft-0.1,xright+0.1), highlight_color, Arrows);

filldraw(pic, circle((-1,25/6),0.05), highlight_color,  highlight_color);
filldraw(pic, circle((0,3),0.05), highlight_color,  highlight_color);
filldraw(pic, circle((1/2,23/12),0.05), highlight_color,  highlight_color);
filldraw(pic, circle((2,-1/3),0.05), highlight_color,  highlight_color);
//label(pic,  "$(0,3)$", (0,3), W, filltype=Fill(white));


xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",Step=1),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
      ticks=LeftTicks("%",Step=1),
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ===== 10x^3/(x^2-1) =========

// ....... large scale behavior .......
picture pic;
int picnum = 10;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-10; ytop=10;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }


filldraw(pic, circle((0,0),0.075), highlight_color,  highlight_color);
// label(pic,  "", (0,1.5), W, filltype=Fill(white));

path grf_left = (-2.5,-8)..(-2.75,-9)..(-3,-10);
path grf_right = (2.5,8)..(2.75,9)..(3,10);

draw(pic, grf_left, highlight_color, Arrow);
draw(pic, grf_right, highlight_color, Arrow);

draw(pic,(-1,ybot)--(-1,ytop),dotted);
draw(pic,(1,ybot)--(1,ytop),dotted);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",Step=1,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... graph .......
real f11(real x) {return 10*x^3/(x^2-1);}

picture pic;
int picnum = 11;
size(pic,0,6cm,keepAspect=true);
scale(pic, Linear(10), Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-40; ytop=40;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }


draw(pic, graph(pic, f11, xleft-0.1,-1-0.2), highlight_color);
draw(pic, graph(pic, f11, -1+0.1, 1-0.1), highlight_color);
draw(pic, graph(pic, f11, 1+0.2, xright+0.1), highlight_color);

draw(pic,Scale(pic,(-1,ybot))--Scale(pic,(-1,ytop)),dashed);
draw(pic,Scale(pic,(1,ybot))--Scale(pic,(1,ytop)),dashed);

dotfactor = 4;
dot(pic,Scale(pic,(-sqrt(3),-15*sqrt(3))), highlight_color);
  label(pic, "$(-\sqrt{3},-15\sqrt{3})$", Scale(pic,(-sqrt(3),-15*sqrt(3))), NW);
dot(pic,Scale(pic,(sqrt(3),15*sqrt(3))), highlight_color);
  label(pic, "$(\sqrt{3},15\sqrt{3})$", Scale(pic,(sqrt(3),15*sqrt(3))), SE);


xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks(Step=1,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks(Step=10,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");






// // ==== local and absolute max ====
// picture pic;
// int picnum = 0;
// size(pic,0,4cm,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=5;
// ybot=0; ytop=5;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// path grph = (xleft-0.1,1){N}..(2,ytop){E}..(3.5,ytop-2){E}
// ..(4,ytop-1.5){E}..(xright+0.1,2){SE};


// draw(pic, grph, highlight_color);

// filldraw(pic, circle((2,ytop),0.05), highlight_color,  highlight_color);
// label(pic,  "$A$", (2,ytop), S, filltype=Fill(white));
// filldraw(pic, circle((3.5,ytop-2),0.05), highlight_color,  highlight_color);
// label(pic,  "$B$", (3.5,ytop-2), S, filltype=Fill(white));
// filldraw(pic, circle((4,ytop-1.5),0.05), highlight_color,  highlight_color);
// label(pic,  "$C$", (4,ytop-1.5), N, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// // ==== parabola ====
// real f1(real x) {return x^2;}

// picture pic;
// int picnum = 1;
// size(pic,4cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=-2; xright=2;
// ybot=0; ytop=4;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// draw(pic, graph(f1,xleft-0.1,xright+0.1), highlight_color);

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// // label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=x^2$", (0,3), W, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

