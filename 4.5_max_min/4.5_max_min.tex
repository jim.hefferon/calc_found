\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 4.5\ \ Absolute maxima and minima}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}{Absolute optima}
Let $f$ be a function.
Recall that the number~$c$ is a \alert{local maximum} if 
$f(c)\geq f(x)$ for inputs $x$ near to $c$,
and it is a \alert{local minimum} if 
$f(c)\leq f(x)$ for inputs $x$ near to $c$.

\Df 
A number~$c$ is a \alert{global maximum} or \alert{absolute maximum}
if $f(c)\geq f(x)$ for all inputs~$x$ in $f$'s domain, 
and a \alert{global minimum}
if $f(c)\leq f(x)$ for all inputs~$x$. 
\end{frame}




\section{Some functions that never attain optima}

\begin{frame}{$f(x)=x^3$}
This function has a critical point, but no
maximum or minimum.
\begin{center}
  \includegraphics{asy/max_min007.pdf}    
\end{center}
\end{frame}

\begin{frame}{$f(x)=(x-1)\cdot x\cdot(x+1)=x^3-x$}
The derivative is $f'(x)=3x^2-1$ so the critical points are $x=\pm \sqrt{1/3}$.
The graph shows that this function has a local maximum and a local minimum
at those critical numbers
But it has no absolute maximum or minimum.
\begin{center}
  \includegraphics{asy/max_min008.pdf}    
\end{center}
\end{frame}

\begin{frame}{$f(x)=1/(x^2+1)$}
The function achieves its maximum at $x=0$.
It never achieves a minimum.
\begin{center}
  \includegraphics{asy/max_min010.pdf}    
\end{center}
\end{frame}

\begin{frame}{$f(x)=1/x$ for $x>0$}
The function's values never reach infinity.
Also, its values never reach $y=0$.
\begin{center}
  \includegraphics{asy/max_min009.pdf}    
\end{center}
\end{frame}


\begin{frame}{$f(x)=1/x$ for the open interval $\open{1}{3}$}
This failure to have optima isn't just about infinity.
The interval $\open{1}{3}$ is bounded but on this interval also, 
the values of the function $f(x)=1/x$ do not quite get up to~$1$, 
nor do they get all the way down to~$1/3$.
\begin{center}
  \includegraphics{asy/max_min017.pdf}    
\end{center}
\end{frame}


\section{Ensuring that a function attains its optima}

\begin{frame}{First way: have a closed and bounded domain}
\Tm For any continuous function, 
on a closed and bounded interval $\closed{a}{b}$ 
it attains its absolute
maximim and its absolute minimum.

\medskip
\alert{Strategy for finding optima}\hspace{0.618em}
Given a continuous function $f$ on a closed and bounded interval $\closed{a}{b}$.
\begin{enumerate}
\item Find the critical points and evaluate the function at all of them.
  That is, compute $f(c)$ for all such~$c$.
\item Also evaluate the function at the interval's endpoints, $f(a)$ and $f(b)$.
\item The largest such value is the absolute maximum 
  and the smallest is the absolute minimum  (there could be ties).
\end{enumerate}
\end{frame}


\begin{frame}
\Ex Optimize $-x^2+3x-2$ over $\closed{1}{3}$

\pause
The derivative is $-2x+3$.
It is always defined, and is zero at $x=3/2$.
So there is a single critical number.
\begin{center}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{\textit{Number to test}}
      &\multicolumn{1}{c}{\textit{Value of function}}  \\
    \hline
    End number $1$         &$f(1)=0$ \\
    End number $3$         &$f(3)=-2$ \\
    Critical number $3/2$ &$f(3/2)=1/4$ \\
  \end{tabular}
\end{center}
\begin{center}
  \includegraphics{asy/max_min011.pdf}    
\end{center}
\end{frame}



\begin{frame}
\Ex Optimize $x^2-3x^{2/3}$ over $\closed{0}{2}$

\pause
The derivative is $x^2-2x^{-1/3}=(2x^{4/3}-2)/x^{1/3}$.
It is not defined at $x=0$. 
It is zero when $0=2x^{4/3}-2$, so at $x=\pm 1$.
We won't evaluate $x=-1$ because it is not in the interval.
\begin{center}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{\textit{Number to test}}
      &\multicolumn{1}{c}{\textit{Value of function}}  \\
    \hline
    End and critical number $0$         &$f(0)=0$ \\
    End number $2$         &$f(2)=4-3(2)^{2/3}\approx -0.762$ \\
    Critical number $1$    &$f(1)=-2$ \\
  \end{tabular}
\end{center}
\begin{center}
  \includegraphics{asy/max_min012.pdf}    
\end{center}
\end{frame}

\begin{frame}
\Ex Optimize $f(x)=2x^3-15x^2+24x+7$ on $\closed{0}{6}$

\pause The derivative is 
$f'(x)=6x^2-30x+24$.
Setting it equal to zero gives that the critical numbers are $x=1$ and $x=4$.
\begin{center}
  \begin{tabular}{l|l}
    \multicolumn{1}{c}{\textit{Number to test}}
      &\multicolumn{1}{c}{\textit{Value of function}}  \\
    \hline
    End number $0$         &$f(0)=7$ \\
    End number $6$         &$f(6)=43$ \\
    Critical number $1$    &$f(1)=18$ \\
    Critical number $4$    &$f(4)=-9$ \\
  \end{tabular}            \\[1ex]
  \includegraphics{asy/max_min018.pdf}
\end{center}
\end{frame}
% sage: def f(x):
% ....:     return(2*x^3-15*x^2+24*x+7)
% ....: 
% sage: f(0)
% 7
% sage: f(6)
% 43
% sage: f(1)
% 18
% sage: f(4)
% -9




\begin{frame}{Practice}
Find the absolute minimum and maximum on the interval.
\begin{enumerate}
\item $f(x)=x^2$ on $\closed{-2}{1}$
\pause
\item $f(x)=x^2$ on $\closed{2}{5}$
\pause
\item $100-x^2$ on $\closed{0}{10}$
\pause
\item $f(x)=3e^{x}$ on $\closed{-1}{1}$
\pause
\item $x^2-6x+7$ on $\closed{0}{10}$
\end{enumerate}
\end{frame}




\begin{frame}{Second way: second derivative test}

\Tm Let $f$ be continuous on the interval $\closed{a}{b}$
and suppose that $c$ is a critical number with $f'(c)=0$.
\begin{itemize}
\item If $f''(c)>0$ then $f(c)$ is a local minimum on that interval.
\item If $f''(c)<0$ then $f(c)$ is a local maximum on the interval.
\end{itemize}
If $f''(c)=0$ or $f''(c)$ does not exist then the test does not apply.

In particular, let the function have only one critical number in $\open{a}{b}$.
\begin{itemize}
\item
If $f''(c)>0$ then $f(c)$ is an absolute minimum on that interval, and
\item
If $f''(c)<0$ then $f(c)$ is an absolute maximum on the interval.
\end{itemize}
Either $a$ or $b$ or both can be infinite.
\end{frame}



\begin{frame}
\Ex Find two numbers whose sum is $19$ and whose product is a maximum.

\pause
Call the numbers $x$ and~$y$.
We want to maximize $p(x,y)=x\cdot y$ subject to $x+y=19$.

\pause
Substitute $y=19-x$ into the $p$ expression to get $P(x)=x\cdot(19-x)=19x-x^2$.
This is a concave down parabola, so we know it has a single maximum.

Take the derivative $P'(x)=19-2x$ and set it to zero, $19-2x=0$, giving that
$x=19/2=9.5$ and the maximum is at $(9.5,90.25)$.  
\end{frame} 
% sage: def f(x):
% ....:     return (19*x-x^2)
% ....: 
% sage: f(19/2)
% 361/4
% sage: n(f(19/2))
% 90.2500000000000
\begin{frame}
\begin{center}
  \includegraphics{asy/max_min020.pdf}    
\end{center}
\end{frame}



\begin{frame}
\Ex
Look for an absolute maximum or minimum on $\open{0}{\infty}$.
\begin{equation*}
  f(x)=x+\frac{4}{x}
\end{equation*}
\pause
Here are the derivatives.
\begin{equation*}
  f'(x)=1-\frac{4}{x^2}=\frac{x^2-4}{x^2}=\frac{(x-2)(x+2)}{x^2}
  \qquad
  f''(x)=\frac{8}{x^3}
\end{equation*}
The only critical number in $\open{0}{\infty}$ is $c=2$.
Because $f''(2)=1$ is positive, it is the absolute minimum of $f$ on 
$\open{a}{b}$.

Because $f(x)=(x^2+4)/x$ we have 
$\lim_{x\to\infty}f(x)=\infty$, so there is no absolute maximum. 
\begin{center}
    \includegraphics{asy/max_min019.pdf}
\end{center}
\end{frame}



\begin{frame}{Practice}
Find any absolute extremum on $\open{-\infty}{\infty}$.
\begin{enumerate}
\item $x^2+2x+1$
\pause
\item $6-x^3$
\pause
\item $-x^2+6x+1$
\pause
\item $x+(9/x)$
\end{enumerate}
\end{frame}


% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
