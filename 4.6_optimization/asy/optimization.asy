// optimization.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "optimization%03d";
real PI = acos(-1);

import graph;

// ===== farmer enclosing three-sided field =======
picture pic;
int picnum = 0;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits 
xleft=0; xright=4;
ybot=0; ytop=2;

path fence = (xleft,ybot){N}--(xleft,ytop)--(xright,ytop)--(xright,ybot);
draw(pic, fence, highlight_color);
label(pic, "$x$", (xleft,ytop/2), W);
label(pic, "$y$", ((xleft+xright)/2,ytop), N);

label(pic,graphic("../pix/cow.png","width=0.35cm,angle=0"),
      (xleft+1.0,ytop-0.5));
label(pic,reflect((0,2),(0,3))*graphic("../pix/cow-silhouette.png","width=0.40cm,angle=0"),
      (xleft+3,ybot+0.85));

path river = (xleft,ybot)--(xright,ybot);
draw(pic, river, verydark_color+linewidth(0.35cm)+extendcap);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== minimal area can ====
real f1(real x) {return 2*PI*x^2+(20/x);}

picture pic;
int picnum = 1;
size(pic,0,3.5cm,keepAspect=true);
scale(pic, Linear(10), Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=0; ytop=50;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(pic, f1,xleft+0.4,xright-0.33), highlight_color);

real solution = (5/PI)^(0.3333);
filldraw(pic, circle(Scale(pic,(solution,f1(solution))),0.25), highlight_color,  highlight_color);
// label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=x^2$", (0,3), W, filltype=Fill(white));

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-2.5, ymax=ytop+2.5,
      p=currentpen,
      ticks=LeftTicks(Step=10,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ===== minimal path =======
picture pic;
int picnum = 2;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits 
xleft=0; xright=10;
ybot=0; ytop=4;
real offset = 0.25;

real split = 3.578; 

path base = (xleft,ybot){W}--(xright,ybot);
draw(pic,base,black);

draw(pic, "$x$", (xleft,ybot-offset)--(split,ybot-offset), black, Bars);
draw(pic, "$10-x$", (split,ybot-offset)--(xright,ybot-offset), black, Bars);

path left = (xleft,ybot)--(xleft,ytop);
draw(pic,left, black);
draw(pic, Label("$4$"), (xleft-offset, ybot)--(xleft-offset,ytop), W, black, Bars);

draw(pic, (xright,ybot)--(split,ybot)--(xleft,ytop), highlight_color+linewidth(3pt));

label(pic, "A", (xright,ybot), 2*N, filltype=Fill(white));
label(pic, "B", (xleft,ytop), 2*NE, filltype=Fill(white));
label(pic, "C", (split,ybot), 2*NE, filltype=Fill(white));


shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== parabola ====
real f3(real x) {return 50*x-x^2;}

picture pic;
int picnum = 3;
scale(pic, Linear(10),Linear);
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=50;
ybot=0; ytop=630;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(pic, f3,xleft-0.1,xright+0.1), highlight_color);

dotfactor = 4;
dot(pic, Scale(pic,(25,625)), highlight_color);
// filldraw(pic, circle((,0),0.05), highlight_color,  highlight_color);
// // label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=x^2$", (0,3), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-4.75, xmax=xright+4.75,
  p=currentpen,
      ticks=RightTicks(Step=10,step=5,OmitTick(0),Size=2pt,size=1pt),
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-9.75, ymax=ytop+9.75,
  p=currentpen,
      ticks=LeftTicks(Step=100,step=10,OmitTick(0),Size=2pt,size=1pt),
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ==== parabola ====
real f4(real x) {return 2400*x-2*x^2;}

picture pic;
int picnum = 4;
scale(pic, Linear(500),Linear);
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=1200;
ybot=0; ytop=720000;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(pic, f4,xleft-0.1,xright+0.1), highlight_color);

dotfactor = 4;
dot(pic, Scale(pic,(600,720000)), highlight_color);
// filldraw(pic, circle((,0),0.05), highlight_color,  highlight_color);
// // label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=x^2$", (0,3), W, filltype=Fill(white));

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-90, xmax=xright+90,
      p=currentpen,
      ticks=RightTicks(Step=1000,step=100,OmitTick(0),Size=2pt,size=1pt),
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-40000, ymax=ytop+20000,
      p=currentpen,
      ticks=LeftTicks(Step=100000,step=50000,OmitTick(0),Size=2pt,size=1pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// // ==== local and absolute max ====
// picture pic;
// int picnum = 0;
// size(pic,0,4cm,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=5;
// ybot=0; ytop=5;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// path grph = (xleft-0.1,1){N}..(2,ytop){E}..(3.5,ytop-2){E}
// ..(4,ytop-1.5){E}..(xright+0.1,2){SE};


// draw(pic, grph, highlight_color);

// filldraw(pic, circle((2,ytop),0.05), highlight_color,  highlight_color);
// label(pic,  "$A$", (2,ytop), S, filltype=Fill(white));
// filldraw(pic, circle((3.5,ytop-2),0.05), highlight_color,  highlight_color);
// label(pic,  "$B$", (3.5,ytop-2), S, filltype=Fill(white));
// filldraw(pic, circle((4,ytop-1.5),0.05), highlight_color,  highlight_color);
// label(pic,  "$C$", (4,ytop-1.5), N, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// // ==== parabola ====
// real f1(real x) {return x^2;}

// picture pic;
// int picnum = 1;
// size(pic,4cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=-2; xright=2;
// ybot=0; ytop=4;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// draw(pic, graph(f1,xleft-0.1,xright+0.1), highlight_color);

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// // label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=x^2$", (0,3), W, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

