\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 5.2:\ \ Substitution}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}{Recall: in the Chain Rule, a new factor appears}

Recall the Chain Rule: if $F(x)=f\circ g\,(x)$ then the derivative is
$F'(x)=f'(\,g(x)\,)\cdot g'(x)$.

\Ex If $f(x)=x^3$ and $g(x)=7x-3$ then the derivative of the composition
$F(x)=(7x-3)^3$ is this.
\begin{equation*}
  F'(x)=3(7x-3)^2\cdot 7
\end{equation*}

In words, the rule is that you take the derivative of the outer function,
substitute the inner function, and multiply by the derivative of the
inner function. 

\Ex The derivative of $F(x)=e^{5x}$ is this.
\begin{equation*}
  F'(x)=e^{5x}\cdot 5
\end{equation*}

\Ex The derivative of $F(x)=\ln(x^2)$ is this.
\begin{equation*}
  F'(x)=\ln(x^2)\cdot 2x
\end{equation*}
\end{frame}


\begin{frame}{Every derivative rule is an antiderivative rule}
Applied to find antiderivatives, the Chain Rule is  
the \alert{Method of Substitution}.

\Tm Let $F$ be an antiderivative of $f$.
Where $u(x)$ is a differentiable function with range that is some interval on
which~$f$ is continuous, this holds.
\begin{equation*}
  \int f(\,u(x)\,)\cdot u'(x)\,dx
  =F(u(x))+C
\end{equation*}

\Ex The antiderivative of $F'(x)=3(7x-3)^2\cdot 7$ is $F(x)=(7x-3)^3$ 

\Ex The antiderivative of $F'(x)=e^{5x}\cdot 5$ is $F(x)=e^{5x}$.

\Ex The antiderivative of $\ln(x^2)\cdot 2x$
is $\ln(x^2)$.

Notice how in Substitution the mutiplied factor ``disappears.''
Of course, that is because in the Chain Rule derivative it ``appears,''
so when doing the Chain Rule in reverse, it ``disappears.''
\end{frame}


\section{For indefinite integrals}
\begin{frame}
\Ex Evaluate this indefinite integral.
\begin{equation*}
\int 2x\cdot \sqrt{1+x^2}\,dx  
\end{equation*}
\pause
A clue is that $\sqrt{1+2x^2}$ is a composition, so define this.
\begin{equation*}
  u=u(x)  = 1+x^2  
\end{equation*}
As a consequence, we get this.
\begin{align*}
  \frac{du}{dx} &= 2x  \\
  du    &= 2x\,dx 
\end{align*}
In the above problem substitution gives 
$\int \sqrt{u}\, du=\int u^{1/2}\,du$.
The answer is $(2/3)u^{3/2}+C$.
Translated back in terms of $x$'s this is the answer.
\begin{equation*}
  \int 2x\cdot \sqrt{1+x^2}\,dx
  =\frac{2}{3}(1+x^2)^{3/2}+C
\end{equation*}
The substitution method has allowed us to 
recognize that the starting integrand 
is the outcome of a Chain Rule applied
to $(1+x^2)^{3/2}$.
\end{frame}


\begin{frame}
\Ex $\displaystyle \int 2\cdot(2t+5)^3\,dt$

\pause
Use this substitution.
\begin{align*}
  u             &=2t+3  \\
  \frac{du}{dt} &=2  \\
  du            &=2\,dt
\end{align*}
The variables change to this.
\begin{equation*}
  \int 2\cdot(2t+5)^3\,dt
  =\int u^3\,du
  =\frac{1}{4}u^4+C
\end{equation*}
Translating back to the original variable,~$t$, gives the solution.
\begin{equation*}
  \int 2\cdot(2t+5)^3\,dt=\frac{1}{4}(2t+5)^4+C
\end{equation*}
\end{frame}


\begin{frame}{Formulas}
\begin{enumerate}
\item $\displaystyle \int u^n\,du=\frac{1}{n+1}u^{n+1}+C$ for $n\neq -1$

\item $\displaystyle \int e^u\,du=e^u+C$

\item $\displaystyle \int \frac{1}{u}\,du=\ln |u|+C$
\end{enumerate}

\pause\medskip
Strategy:~if there is a composition then
it may call for the method of substitution.  
\begin{itemize}
\item Try setting $u$ to be the inside function.   
  Compute $du$.
\item Re-express the original problem so it involves only $u$'s.
  Evaluate that integral.
\item Translate back to the original variable.
\end{itemize}
\end{frame}



\begin{frame}{Practice}
State the substitution, and then give the indefinite integral.
\begin{enumerate}
\item $\displaystyle \int e^{t^2}\cdot (2t)\,dt$

\pause Use $u=t^2$ so $du=2t\,dt$.
Then the integral is $\int e^u\,du=e^u+C$, which gives the solution to 
the original problem as $e^{t^2}+C$.

\item $\displaystyle \int \frac{3}{3x-5}\,dx$

\pause Use $u=3x-5$ so $du=3\,dx$.
Then the integral is $\int 1/u\,du=\ln|u|+C$. 
The solution to 
the original problem is $\ln|3x-5|+C$.

\item $\displaystyle \int \sqrt{3s^2+8}\cdot 6s\,ds$

\pause Use $u=3s^2+8$ so $du=6s\,ds$.
The integral is $\int \sqrt{u}\,du=\int u^{1/2}\,du=(2/3)u^{3/2}+C$. 
The solution to 
the original problem is $(2/3)(3s^2+8)^{3/2}+C$.
\end{enumerate}
\end{frame}
% \begin{frame}
% \begin{enumerate}\setcounter{enumi}{3}
% \item $\displaystyle \int 3e^{3x}\,dx$

% \pause Use $u=3x$ so $du=3\,dx$.
% The integral is $\int e^u\,du=e^u+C$. 
% The original problem's solution is $e^{3x}+C$.

% \item $\displaystyle \int \frac{2y}{y^2+9}\,dy$

% \pause Use $u=y^2+9$ so that $du=2y\,dy$.
% The $u$~integral is $\int 1/u\,du=\ln|u|+C$. 
% The starting problem has the solution $\ln|y^2+9|+C$.
% \end{enumerate}
% \end{frame}



\begin{frame}{Fooling with constant factors}
\Ex $\displaystyle \int e^{10x}\,dx$

\pause
Let $u=10x$.
Then $du/dx=10$, giving $dx=(1/10)du$.
\begin{equation*}
  \int e^{10x}\,dx
  =\int e^u\frac{1}{10}\,du
  =\frac{1}{10}\int e^u\,du
  =\frac{e^u}{10}+C
  =\frac{e^{10x}}{10}+C  
\end{equation*}
(Note that we write $(1/10)\cdot C$ as $C$.
Both stand for unknown constants.)

\pause
\Ex $\displaystyle \int \frac{1}{5x+2}\,dx$

\pause
Let $u=5x+2$ so that $du=5\,dx$.
Since what we have is $dx$, rewrite as $dx=(1/5)\,du$.
\begin{align*}
  \int \frac{1}{5x+2}\,dx
  &=\int \frac{1}{u}\cdot\frac{1}{5}\,du  \\
  &=\frac{1}{5}\cdot \int \frac{1}{u}\,du  \\
  &=\frac{1}{5}[\ln|u| +C] 
  =\frac{1}{5}\ln|5x+2|+C     
\end{align*}
\end{frame}


\begin{frame}{Practice}
Use substitution.
\begin{enumerate}
\item $\displaystyle \int xe^{-x^2}\,dx$ 

\pause
Take $u=-x^2$ so $du=-2x\,dx$.
The integral becomes $\int e^u\cdot(-1/2)\,du=-(1/2)\int e^u\,du$,
which gives $-(1/2)e^u+C=(-1/2)e^{-x^2}+C$.

\item $\displaystyle \int (5-3x)^{10}\,dx$ 

\pause
Use $u=5-3x$ so $du=-3\,dx$.
Then the integral is 
$\int u^{10}\cdot(-1/3)\,du=-(1/3)\int u^{10}\,du$.
The answer is $-(1/3)\cdot(1/11)u^{11}+C=-(1/33)\cdot(5-3x)^{11}+C$.

\item $\displaystyle \int \frac{2z^2}{z^3+3}\,dz$  

\pause
Take $u=z^3+3$, making $du=3z^2\,dz$.
The integral is 
$\int (2/3)(1/u)\,du=(2/3)\int (1/u)\,du$.
The answer is $(2/3)\ln|u|+C=(2/3)\cdot\ln|z^3+3|+C$.
\end{enumerate}\end{frame}
% \begin{frame}\begin{enumerate}\setcounter{enumi}{3}
% \item $\displaystyle \int (3t-1)^{50}\,dt$ 

% \pause
% Take $u=3t-1$, making $du=3\,dt$.
% The integral is 
% $\int u^{50}(1/3)\,du$, giving
% $(1/3)(1/51)u^{51}+C=(1/153)\cdot (3t-1)^{51}+C$.

% \item $\displaystyle \int \frac{x}{x^2+1}\,dx$

% \pause
% Use $u=x^2+1$ and so $du=2x\,dx$.
% The integral is 
% $\int (1/u)\cdot (1/2)\,du$, which gives
% $(1/2)\ln(u)+C=(1/2)\cdot \ln(x^2+1)+C$.

% \item  $\displaystyle \int e^{x^3}(9x^2)\,dx$

% \pause
% Use $u=x^3$, giving $du=3x^2\,dx$.
% Then the integral is 
% $3\cdot \int e^u\cdot \,du$, which gives
% $3e^u+C=3\cdot e^{x^3}+C$.
 
% \end{enumerate}
% \end{frame}




% \section{For definite integrals}
% \begin{frame}
% With definite integrals, 
% when we change variables, such as from $x$ to~$u$, we have to change
% the limits of integration.

% \Tm
% $\displaystyle \int_{x=a}^bf(u(x))\cdot u'(x)\,dx
%   =\int_{u=u(a)}^{u(b)}f(u)\,du $ 

% \Ex $\displaystyle \int_{x=0}^2x^2\sqrt{x^3+1}\,dx$

% \pause
% Take $u(x)=u=x^3+1$ so that $du=3x^2\,dx$.
% As $x$ varies from $0$ to~$2$, we have that $u$ varies from $1$ to~$9$.
% \begin{equation*}
%   \int_{x=0}^2x^2\sqrt{x^3+1}\,dx
%   =\int_{u=1}^9\sqrt{u}\cdot\frac{1}{3}\,du
%   =\frac{1}{3}\int_{u=1}^9u^{1/2}\,du
%   =\Bigl. \frac{1}{3}\cdot\frac{2}{3}u^{3/2} \Bigr|_{u=1}^{9}
%   =\frac{52}{9}
% \end{equation*}
% \end{frame}

% \begin{frame}{Practice}
% Evaluate.
% \begin{enumerate}
% \item $\displaystyle \int xe^{-x^2}\,dx$ (use $u=-x^2$) 
% \pause
% \item $\displaystyle \int (5-3x)^{10}\,dx$ 
% \pause
% \item $\displaystyle \int \frac{2z^2}{z^3+3}\,dz$  
% \pause
% \item $\displaystyle \int_{t=0}^1 (3t-1)^{50}\,dt$ 
% \pause
% \item $\displaystyle \int_0^4 \frac{x}{x^2+1}\,dx$ 
% \end{enumerate}

  
% \end{frame}



% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 
