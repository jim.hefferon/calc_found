// growth.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="lualatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "growth%03d";

import graph;


// ==== y=ln(x) ====
real f1(real x) {return log(x);}

picture pic;
int picnum = 1;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=5;
ybot=-3; ytop=2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f1,0.05,xright+0.1), highlight_color);

filldraw(pic, circle((1,0),0.05), highlight_color,  highlight_color);
label(pic,  "$(1,0)$", (1,0), NW, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ================ y=e^x with tangent lines =======
real f2(real x) {return exp(x);}
real tangent_line2a(real x) {return exp(1)*(x-1)+exp(1);}
real tangent_line2b(real x) {return exp(-2)*(x+2)+exp(-2);}

picture pic;
int picnum = 2;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=2;
ybot=0; ytop=5;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f2,xleft-0.1,1.61), black);
// sage: n(ln(5))
// 1.60943791243410
draw(pic, graph(tangent_line2a,1-0.5,1+0.5), highlight_color);
filldraw(pic,circle((1,exp(1)),0.05),highlight_color,highlight_color);
draw(pic, graph(tangent_line2b,-2-1,-2+1), highlight_color);
filldraw(pic,circle((-2,exp(-2)),0.05),highlight_color,highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ================ compounding periods =======
// real f3(real x, real n) {return exp(n*log(x));}
real f3(real x) {return (1+(1/x))^(x);}

picture pic;
int picnum = 3;
scale(pic, Linear, Linear(30));
size(pic,10cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=375;
ybot=0; ytop=3;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path f = graph(pic, f3, xleft+0.01, xright, n=1000);
draw(pic, f, gray(0.7));

dotfactor = 4;
int[] compoundings={1,4,12,52,365};
int[] compoundingsa={4,12,52,365};
for(int k : compoundings) {
  pair pt = (k, f3(k));
  dot(pic, Scale(pic,pt), highlight_color);
}

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%", compoundings, Size=2pt),
      arrow=Arrows(TeXHead));
for (int k : compoundingsa) {
  labelx(pic, format("$%d$", k), k);
}

real[] T3 = {1, 2, 2.718, 3};
real[] T3a = {1, 2, 3};
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.25, ymax=ytop+0.25,
      p=currentpen,
      ticks=LeftTicks("%", T3, Size=2pt),
      arrow=Arrows(TeXHead));
for (real k : T3a) {
  labely(pic, format("$%f$", k), k);
}
labely(pic, "$e$", 2.718);
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ================ simple interest =======

real f4(real P, real r) {return P*r;}

picture pic;
int picnum = 4;
scale(pic, Linear(1500), Linear);
size(pic,0,4cm,keepAspect=true);

real r = 0.05;  // rate per period
real P = 15000; // principle
real t = 3;     // life of loan

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=3;
ybot=12000; ytop=18000;

dotfactor = 4;
pair pnt;
real current_value = P;
for(int k=0; k < t; ++k) {
  pnt = (k, current_value);
  draw(pic, Scale(pic,pnt)--Scale(pic,(k+1,current_value)), gray(0.7));
  dot(pic, Scale(pic,pnt), highlight_color);
  current_value = current_value+f4(P,r);
}
// current_value = current_value+f4(P,r);
pnt = (t, current_value);
draw(pic, Scale(pic,pnt)--Scale(pic,(t+0.75,current_value)), gray(0.7));
dot(pic, Scale(pic,pnt), highlight_color);

xaxis(pic, "Period",  // label
      axis=YEquals(ybot+1000),
      xmin=xleft-0.25, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks(Step=1, OmitTick(0), Size=2pt),
      arrow=Arrows(TeXHead));

// Label L = shift(0,ytop)*rotate(0)*Label("Value",W);  // ???
yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot+500, ymax=ytop+500,
      p=currentpen,
      ticks=LeftTicks("%.0f",Step=5000, step=1000, OmitTick(0), Size=2pt, size=1pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ==== y=e^x ====
real f5(real x) {return exp(x);}

picture pic;
int picnum = 5;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=0; ytop=10;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f5,xleft-0.1,2.31), highlight_color);

// filldraw(pic, circle((0,1),0.05), highlight_color,  highlight_color);
// label(pic,  "$(0,1)$", (0,1), SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ........ y=e^{-x} .........
real f6(real x) {return exp(-x);}

picture pic;
int picnum = 6;
size(pic,0,3cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=0; ytop=10;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

draw(pic, graph(f6,-2.31,xright+0.1), highlight_color);

// filldraw(pic, circle((0,1),0.05), highlight_color,  highlight_color);
// label(pic,  "$(0,1)$", (0,1), SE, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

