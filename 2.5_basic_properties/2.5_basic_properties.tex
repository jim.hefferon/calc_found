\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title[Basic Properties] % (optional, use only with long paper titles)
{Section 2.5\ \ Basic Properties of Derivatives}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................



\begin{frame}{Some differentiation rules}
\begin{enumerate}
\item The derivative of a constant function $f(x)=k$ is $f'(x)=0$.
\item The derivative of a power $f(x)=x^k$ is $f'(x)=kx^{k-1}\!$.
\item The derivative of a constant multiple of a function $k\cdot u(x)$ 
  is $k\cdot u'(x)$.
\item The derivative of a sum is the sum of the derivatives.
\begin{equation*}    
  \text{If $h(x)=f(x)+g(x)$ then $h'(x)=f'(x)+g'(x)$}
\end{equation*}
\item The derivative of a difference is the difference of the derivatives.
\begin{equation*}    
  \text{If $h(x)=f(x)-g(x)$ then $h'(x)=f'(x)-g'(x)$}
\end{equation*}
\end{enumerate}

\pause
\textit{Caution:} We do not yet know how to do the product of functions,
$f(x)\cdot g(x)$ 
or the quotient, $f(x)/g(x)$.
We also do not know how to do the composition.
\end{frame}


\begin{frame}{Same rules, in the other notation}
\begin{enumerate}
\item $\displaystyle \frac{d\,k}{dx}=0$ 
\item $\displaystyle \frac{d\,x^k}{dx}=k\cdot x^{k-1}$ 
\item $\displaystyle \frac{d\,k\cdot f(x)}{dx}=k\cdot\frac{d\,f(x)}{dx}$ 
\item $\displaystyle \frac{d\,(f(x)+g(x))}{dx}=\frac{d\,f(x)}{dx}+\frac{d\,g(x)}{dx}$
\item $\displaystyle \frac{d\,(f(x)-g(x))}{dx}=\frac{d\,f(x)}{dx}-\frac{d\,g(x)}{dx}$
\end{enumerate}
\end{frame}

\begin{frame}{Examples}
Here is the derivative of a number of functions.
\begin{enumerate}
\item $\displaystyle f(x)=5$
\pause gives $\displaystyle f'(x)=0$
\item $\displaystyle f(x)=x^4$
\pause gives $\displaystyle f'(x)=4x^3$
\item $\displaystyle f(x)=x^{-2}$
\pause gives $\displaystyle f'(x)=-2x^{-3}$
\item $\displaystyle f(x)=x^{1.5}$
\pause gives $\displaystyle f'(x)=1.5\cdot x^{0.5}$
\item $\displaystyle f(x)=5x^2$
\pause gives $\displaystyle f'(x)=10\cdot x^{1}=10x$
\item $\displaystyle f(x)=3x^2-4x+7$
\pause gives $\displaystyle f'(x)=6\cdot x-4$
\item $\displaystyle f(x)=16-4x^3$
\pause gives $\displaystyle f'(x)=-12\cdot x^2$
\end{enumerate}
\end{frame}


\begin{frame}{Practice}
Find the derivative of each, using the other notation.
\begin{enumerate}
\item $\displaystyle f(x)=3x+1$
\pause \\ $\displaystyle \frac{df}{dx}=3$
\item $\displaystyle g(t)=-t^2$
\pause \\ $\displaystyle \frac{dg}{dt}=-2t^{1}=-2t$
\item $\displaystyle s(x)=3x^3+4x$
\pause \\ $\displaystyle ds/dx=9x^2+4$
\item $\displaystyle f(x)=2x-6x^5$
\pause \\ $\displaystyle df/dx=2-30x^4$
\item $\displaystyle g(v)=(1/2)v+15v^4$
\pause \\ $\displaystyle dg/dv=(1/2)+60v^3$
\item $\displaystyle f(x)=2+(1/5)x^{3.1}$
\pause \\ $\displaystyle df/dx=(3.1/5)x^{2.1}$
\item $\displaystyle h(t)=1+t+t^2+t^3$
\pause \\ $\displaystyle dh/dt=0+1+2t+3t^2=1+2t+3t^2$
\end{enumerate}
\end{frame}


\begin{frame}{Tangent line}\vspace*{-1ex}
Find the equation of the line tangent to $f(x)=5-(1/2)x^2$
at $(2,3)$.

\pause
The derivative is $f'(x)=-x$.
At the point $(2,3)$ the slope is $f'(2)=-2$.
This is the equation of the line.
\begin{equation*}
  y-3=(-2)\cdot (x-2)
\end{equation*}
If you prefer the ``$mx+b$'' form for the line then this is the answer.
\begin{equation*}
  y=(-2)\cdot (x-2)+3=-2x+4+3=-2x+7
\end{equation*}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/basic_properties000.pdf}}
\end{center}
\end{frame}


% \begin{frame}{Practice with tangent lines}
% Find the equation of the line tangent to each at the given point.
% Remember that where a line has slope~$m$ and passes through $(x_0,y_0)$
% then it has the equation $y-y_0=m\cdot (x-x_0)$.
% \begin{enumerate}
% \item $f(x)=x^2+3$ at $(a,f(a))=(1,4)$
%   \pause\\  We have $f'(x)=2x$ so the slope of the tangent line is
%      $f'(a)=f'(1)=2$.  The tangent line is $y-4=2\cdot (x-1)$.
% % \item $f(x)=x^2+3$ at $(a,f(a))=(0,3)$
% \item $f(x)=9-x-x^2$ at $a=2$
%   \pause\\  Here $f'(x)=-1-2x$ so the slope of the tangent line is
%      $f'(a)=f'(2)=-5$.  The point on the curve is $(a,f(a))=(2,3)$.
%     The tangent line is $y-3=-5\cdot (x-2)$.
% \end{enumerate}
% \end{frame}

% \begin{frame}{Interpretation}
% In our gourmet food store, the relationship between the price of 
% chocolate in dollars~$p$ and number of pounds sold~$x$ follows this model.
% \begin{equation*}
%   x(p)=10+\frac{180}{p}=10+180\cdot p^{-1}
%   \qquad 2\leq p<10
% \end{equation*}
% Find the demand, and the instantaneous rate of change of demand,
% when the price is five dollars.
% Briefly interpret.

% \pause
% At $\$5$ the demand is this.
% \begin{equation*}
%   x(5)=10+\frac{180}{5}=46
% \end{equation*}
% As to the rate of change, we have this.
% \begin{equation*}
%   \frac{d\,x}{dp}=180\cdot -1p^{-2}=\frac{-180}{p^2}
% \end{equation*}
% Plugging in $p=5$ gives the instantaneous rate of change as
% $-7.2$.
% The interpretation is that at a price of $\$5$, every increase in
% price changes the demand by about $-7.2$ pounds per dollar. 
% So a price increase causes a decreased demand.
% \end{frame}

% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
