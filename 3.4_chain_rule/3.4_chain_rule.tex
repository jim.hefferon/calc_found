\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 3.4\ \ Derivatives of compositions; the Chain Rule}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

% Change the default thickness of the underbars.
% https://tex.stackexchange.com/a/559081/339
\usepackage{xpatch}
\MHInternalSyntaxOn
\xpatchcmd\upbracketfill
  {\sbox\z@{$\braceld$}\edef\l_MT_bracketheight_fdim{\the\ht\z@}}
  {\edef\l_MT_bracketheight_fdim{.6pt}}
  {}{\fail}

\xpatchcmd\downbracketfill
  {\sbox\z@{$\braceld$}\edef\l_MT_bracketheight_fdim{\the\ht\z@}}
  {\edef\l_MT_bracketheight_fdim{.6pt}}
  {}{\fail}
\MHInternalSyntaxOff


\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................


\section{Composition of functions}

\begin{frame}{Definition of composition}
Consider $F(x)=\sqrt{2x^2+1}$.
Given some number~$a$, to evaluate $F(a)$ you need to do two steps: 
(1)~find $2a^2+1$ and (2)~find the square root of the result.

We say that $F$ is the \alert{composition} of the two functions $g(x)=2x^2+1$
and $f(x)=\sqrt{x}$.
We write $F(x)=f(\,g(x)\,)$ or $F(x)=f\circ g\,(x)$.

\pause\medskip
Compose the functions to get $F(x)=f(\,g(x)\,)=f\circ g\,(x)$.
\begin{enumerate}
\item $\displaystyle g(x)=2x+3$ and $\displaystyle f(x)=x^3$
\pause \\  $\displaystyle F(x)=(2x+3)^3$ 
\item $\displaystyle g(h)=2+h$ and $\displaystyle f(x)=x^3+x$
\pause \\  $\displaystyle F(x)=(2+h)^3+(2+h)$ 
% \item $\displaystyle g(x)=\sqrt{x}$ and $\displaystyle f(x)=e^x$
% \pause \\ $\displaystyle F(x)=e^{\sqrt{x}}$ 
\item $\displaystyle g(x)=\ln(x)$ and $\displaystyle f(x)=x^3+3x$
\pause \\ $\displaystyle F(x)=(\ln x)^3+3\ln x$ 
\end{enumerate}
\end{frame}



\begin{frame}{Matters of order}
Let $g(x)=2x^2+1$ and $f(x)=\sqrt{x}$.
Then $F(x)=f\circ g\,(x)$ is $F(x)=\sqrt{2x^2+1}$.

In the expression $f\circ g\,(x)$ we write $f$ first.
But it is the function that is done second\Dash if you plug in $x=5$
then you take $5^2+1$ before you square root.  
More clear than ``first'' or ``second'' is to call $f$ the 
\alert{outer} functions and to call $g$ the \alert{inner}.  

\pause\medskip
Write each as the composition of an outer and inner function.
\begin{enumerate}
% \item $\displaystyle H(x)=(3x+2)^2$
% \pause \\ $\displaystyle g(x)=3x+2$ and $\displaystyle f(x)=x^2$ 
\item $\displaystyle F(h)=(1+h)^2$
\pause \\ Inner function is $\displaystyle g(h)=1+h$ and 
  outer is $\displaystyle f(x)=x^2\!$.
\item $\displaystyle H(x)=e^{x-x^2}$
\pause \\ Inner is $\displaystyle g(x)=x-x^2$ and outer is $\displaystyle f(x)=e^x\!$.
% \item $\displaystyle G(t)=\ln(t^2+9)$
% \pause \\ $\displaystyle g(t)=t^2+9$ and $\displaystyle f(t)=\ln(t)$
\item $\displaystyle H(t)=200e^{3t}$
\pause \\ Inner is $\displaystyle g(t)=3t$ and outer is $\displaystyle f(t)=200e^t\!$.
\end{enumerate}
\end{frame}


\section{Derivative of a composition}

\begin{frame}{Chain Rule}
If $g$ is differentiable at~$x$ and $f$ is differentiable
at $g(x)$ then $F=f\circ g$ is differentiable at~$x$ and $F'$ is this.
\begin{equation*}
  F'(x)=f'(\,g(x)\,)\cdot g'(x)
  \qquad
  \frac{d\,F(x)}{dx}=\frac{d\,f(\,g(x)\,)}{dx}\cdot \frac{d\,g}{dx}
\end{equation*}

\pause
\textsc{Example}
The derivative of $(x^2+4)^3$ is $3(x^2+4)^2\cdot 2x$.

For teaching purposes, I sometimes write underbrackets.
They help with the presentation.
You don't need to write them and your text, etc., will not
write them.
\begin{equation*}
  \frac{d\,(x^2+4)^3}{dx}=
  \underbracket{3(x^2+4)^2}\cdot\underbracket{2x}
\end{equation*}

\Ex
Find the derivative of $y(t)=e^{4t}\!$.
\pause
\begin{equation*}
  \underbracket{e^{4t}}\cdot\underbracket{4}
\end{equation*}

The underbraces help us see that the Chain Rule is:~take 
the derivative of the outer function, copy in the 
inner function, and then multiply by the derivative of the inner.
\end{frame}

\begin{frame}
\Ex
Consider $F(x)=\sqrt{2x^2-1}$.
Write it as a composition, and then take the derivative.

\pause
It is the composition $f\circ g$ where the outer function is 
$f(x)=\sqrt{x}=x^{1/2}$
and the inner function is $g(x)=2x^2-1$.
The derivatives of those functions are here.
\begin{equation*}
  f'(x)=\frac{1}{2}\cdot x^{-1/2}
  \qquad
  g'(x)=4x
\end{equation*}

\pause
This is the derivative of the composition.
\begin{equation*}
  F'(x)=\underbracket{\frac{1}{2}\frac{1}{\sqrt{2x^2-1}}}\cdot\underbracket{4x}
       =\frac{2x}{\sqrt{2x^2-1}}
\end{equation*}
\end{frame}



\begin{frame}{Don't make this mistake}
We just did  $F=f\circ g$ where $f(x)=\sqrt{x}=x^{1/2}$
with $g(x)=2x^2-1$.
The derivatives are $f'(x)=1/(2\,\sqrt{x})$ and $4x$.

For the derivative of the composition
do not just write the product.
\begin{equation*}
  \underbracket{\frac{1}{2\,\sqrt{x}}}\cdot\underbracket{4x}
  \tag{Wrong!}
\end{equation*}
Instead, this is the right answer.  
\begin{equation*}
  F'(x)=\underbracket{\frac{1}{2\,\sqrt{2x^2-1}}}\cdot\underbracket{4x}
       % =\frac{2x}{\sqrt{2x^2-1}}
\end{equation*}
\end{frame}


\begin{frame}{Practice}
\begin{enumerate}
\item $\displaystyle \frac{d\,(3-x^2)^3}{dx}$
\pause
\begin{equation*}
  \underbracket{3(3-x^2)^2}\cdot \underbracket{(-2x)}
  =-6x(3-x^2)^2
\end{equation*}

\item $\displaystyle \frac{d\, \sqrt{5x^2+1}}{dx}$
\pause 
\begin{equation*}
  \underbracket{\frac{1}{2}(5x^2+1)^{-1/2}}\cdot\underbracket{(10x)}
  =\frac{5x}{\sqrt{5x^2+1}}
\end{equation*}

\item $\displaystyle \frac{d\, e^{2x-1}}{dx}$
\pause
\begin{equation*}
  \underbracket{e^{2x-1}}\cdot\underbracket{2}
  =2e^{2x-1}  
\end{equation*}

\item $\displaystyle 5x^2e^{3x}$
\textit{Hint:} it is the product of two functions so start with the
Product Rule.
\pause
\begin{equation*}
  5x^2\cdot\underbracket{e^{3x}}\cdot\underbracket{3}
  +e^{3x}\cdot 10x
  =5xe^{3x}\cdot(x+2)
\end{equation*}

% \item $\displaystyle \frac{d\,\frac{1}{(2x^4-x^2+1)^3}}{dx}$
% \pause $=\displaystyle \frac{6x(1-4x^2)}{(2x^4-x^2+1)^4}$

% \item $\displaystyle \frac{d\,(\tan x+x)^9}{dx}$
% \pause \\  $\displaystyle 9(\tan x +x)^8\cdot (\sec^2x+1)$
% \item Find the line tangent to $\displaystyle y=3\cos(x^2)$ at $x=\sqrt{\pi/2}$.
% \pause \\[1ex] The derivative is 
%   $\displaystyle 3\cdot(-\sin(x^2))\cdot 2x=-6x\sin(x^2)$.
%   So the slope is $-6\sqrt{\pi/2}$.
%   The line goes through the point $(\sqrt{\pi/2},0)$. 
\end{enumerate}
\end{frame}

% \begin{frame}
% Find the line tangent to $\displaystyle y=(1/10)e^{x^2}$ at $x=1$.
% \pause \\[1ex] The derivative is this.
% \begin{equation*}
%   \underbracket{\frac{1}{10}e^{x^2}}\cdot\underbracket{2x}
% \end{equation*}
% So at $x=1$ the function's rate of change is $2e/10\approx 0.272$.
% The line goes through the point $(1,e/10)$.   
% \begin{equation*}
%   y-e/10=(2e/10)\cdot(x-1)
% \end{equation*}
% \begin{center}
%   \includegraphics{asy/chain_rule000.pdf}
% \end{center}
% \end{frame}
% % sage: n(e/10)
% % 0.271828182845905
% % sage: n(e/10)*2
% % 0.543656365691809


\begin{frame}{Compositions of more than two functions}
The Chain Rule is:~take 
the derivative of the outer function, copy in the 
inner function, and then multiply by the derivative of the inner.
The same applies when the composition is nested more than two-deep.

\Ex This one is three-deep.
\begin{align*}
  \frac{d\,e^{\sqrt{x^2+1}}}{dx} &=\underbracket{e^{\sqrt{x^2+1}}}\cdot\underbracket{(1/2)(x^2+1)^{-1/2}}\cdot\underbracket{2x}  \\
      &=\frac{xe^{\sqrt{x^2+1}}}{\sqrt{x^2+1}}
\end{align*}

\pause
\Ex Here is another.
\begin{align*}
  \frac{d\,\ln\left((x^2+4)^{5/2}\right)}{dx}
  &=\underbracket{\frac{1}{(x^2+4)^{5/2}}}\cdot\underbracket{\frac{5}{2}(x^2+4)^{3/2}}\cdot\underbracket{(2x)}   \\
  &=\frac{5x}{x^2+4}
\end{align*}
\end{frame}

\begin{frame}{Practice}
\Ex Find the derivative of $(\ln(3x^3-x))^2\!$.
\pause
\begin{equation*}
  \underbracket{2(\ln(3x^3-x))}\cdot\underbracket{\frac{1}{3x^3-x}}\cdot\underbracket{(9x^2-1)}
\end{equation*}
It doesn't simplify much.
\begin{equation*}
  =\frac{2(9x^2-1)\ln(3x^3-x)}{3x^3-x}
\end{equation*}

\pause
\Ex Find the rate of change of this function at $x=1$.
\begin{equation*}
  y(x)=e^{(x^2+3x+1)^3}
\end{equation*}
\pause
Here is $y'(x)$.
\begin{equation*}
  \underbracket{e^{(x^2+3x+1)^3}}\cdot \underbracket{3(x^2+3x+1)^2}\cdot \underbracket{(2x+3)}
\end{equation*}
Plug in $x=1$ to get $y'(1)=e^{125}\cdot 75\cdot 5=375e^{125}\!$.

% \begin{enumerate}
% % \item $\displaystyle (9-5x)^2$
% % \pause
% % \begin{equation*}
% %   \underbracket{2(9-5x)}\cdot\underbracket{(-5)}
% %   =-90+50x
% % \end{equation*}

% \item $\displaystyle e^{(x^2+3x+1)^3}$
% \pause
% \begin{equation*}
%   \underbracket{e^{(x^2+3x+1)^3}}\cdot \underbracket{3(x^2+3x+1)^2}\cdot \underbracket{(2x+3)}
% \end{equation*}

% \end{enumerate}  
\end{frame}


% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
