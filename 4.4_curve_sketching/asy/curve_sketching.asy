// curve_sketching.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "curve_sketching%03d";
real PI = acos(-1);

import graph;



// ==== inc/dec and concavity ====

// ....... inc and up ............
picture pic;
int picnum = 0;
size(pic,0,1cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf = (0.5,0.5){E}::(1,0.75)::(1.5,1.5);

draw(pic, grf, highlight_color);

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.25, xmax=xright+.25,
//   p=currentpen,
//       ticks=NoTicks,
//   arrow=Arrows(TeXHead));

// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.25, ymax=ytop+0.25,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... inc and down ............
picture pic;
int picnum = 1;
size(pic,0,1cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf = (0.5,0.5)::(1,1.25)::(1.5,1.5);

draw(pic, grf, highlight_color);

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.25, xmax=xright+.25,
//   p=currentpen,
//       ticks=NoTicks,
//   arrow=Arrows(TeXHead));

// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.25, ymax=ytop+0.25,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... dec and down ............
picture pic;
int picnum = 2;
size(pic,0,1cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf = (0.5,1.5)::(1,0.75)::(1.5,0.5);

draw(pic, grf, highlight_color);

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.25, xmax=xright+.25,
//   p=currentpen,
//       ticks=NoTicks,
//   arrow=Arrows(TeXHead));

// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.25, ymax=ytop+0.25,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ....... dec and up ............
picture pic;
int picnum = 3;
size(pic,0,1cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf = (0.5,1.5)::(1,1.25)::(1.5,0.5);

draw(pic, grf, highlight_color);

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.25, xmax=xright+.25,
//   p=currentpen,
//       ticks=NoTicks,
//   arrow=Arrows(TeXHead));

// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.25, ymax=ytop+0.25,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ======= concave up ======
real f4(real x) {return 2*(x-1)^2+1;}

picture pic;
int picnum = 4;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(f4, 0.5, 1.5), highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

real f5(real x) {return -2*(x-1)^2+1.5;}

// ......... concave down ...........
picture pic;
int picnum = 5;
size(pic,3cm,0,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=0; xright=2;
ybot=0; ytop=2;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(f5, 0.5, 1.5), highlight_color);

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.25, xmax=xright+.25,
  p=currentpen,
      ticks=NoTicks,
  arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.25, ymax=ytop+0.25,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ==== (1/3)x^3-(1/2)x^2-2x+3 ====

// ....... large scale behavior .......
picture pic;
int picnum = 6;
size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-5; ytop=5;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

path grf_l = (-2,-3.5)..(-2.5,-4)..(-3,-5); 
path grf_r = (2,3.5)..(2.5,4)..(3,5); 

draw(pic, grf_l, highlight_color, Arrow);
draw(pic, grf_r, highlight_color, Arrow);

filldraw(pic, circle((0,3),0.07), highlight_color,  highlight_color);
label(pic,  "$(0,3)$",
        (0,3), W, filltype=Fill(white));

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// label(pic,  "\begin{tabular}[b]{l}$f(x)=x^{2k}$\\ $k\geq 1$\end{tabular}",
//         (0,1.5), W, filltype=Fill(white));

xaxis(pic, L="",  // label
  axis=YZero,
  xmin=xleft-0.75, xmax=xright+.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
  ticks=NoTicks,
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ....... graph .......
real f7(real x) {return (1/3)*x^3-(1/2)*x^2-2*x+3;}

picture pic;
int picnum = 7;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2.65; xright=3;
ybot=-2; ytop=4;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic,(-1,ybot)--(-1,ytop),dotted);
draw(pic,(1/2,ybot)--(1/2,ytop),dotted);
draw(pic,(2,ybot)--(2,ytop),dotted);

draw(pic, graph(f7,xleft-0.1,xright+0.1), highlight_color, Arrows);

filldraw(pic, circle((-1,25/6),0.05), highlight_color,  highlight_color);
filldraw(pic, circle((0,3),0.05), highlight_color,  highlight_color);
filldraw(pic, circle((1/2,23/12),0.05), highlight_color,  highlight_color);
filldraw(pic, circle((2,-1/3),0.05), highlight_color,  highlight_color);
//label(pic,  "$(0,3)$", (0,3), W, filltype=Fill(white));


xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",Step=1),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
  axis=XZero,
  ymin=ybot-0.75, ymax=ytop+0.75,
  p=currentpen,
      ticks=LeftTicks("%",Step=1),
  arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ===== 10x^3/(x^2-1) =========

// ....... large scale behavior .......
picture pic;
int picnum = 8;
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-10; ytop=10;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }


filldraw(pic, circle((0,0),0.075), highlight_color,  highlight_color);
// label(pic,  "", (0,1.5), W, filltype=Fill(white));

path grf_left = (-2.5,-8)..(-2.75,-9)..(-3,-10);
path grf_right = (2.5,8)..(2.75,9)..(3,10);

draw(pic, grf_left, highlight_color, Arrow);
draw(pic, grf_right, highlight_color, Arrow);

draw(pic,(-1,ybot)--(-1,ytop),dotted);
draw(pic,(1,ybot)--(1,ytop),dotted);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks("%",Step=1,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... graph .......
real f9(real x) {return 10*x^3/(x^2-1);}

picture pic;
int picnum = 9;
size(pic,0,6cm,keepAspect=true);
scale(pic, Linear(10), Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-40; ytop=40;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }


draw(pic, graph(pic, f9, xleft-0.1,-1-0.2), highlight_color);
draw(pic, graph(pic, f9, -1+0.1, 1-0.1), highlight_color);
draw(pic, graph(pic, f9, 1+0.2, xright+0.1), highlight_color);

draw(pic,Scale(pic,(-1,ybot))--Scale(pic,(-1,ytop)),dashed);
draw(pic,Scale(pic,(1,ybot))--Scale(pic,(1,ytop)),dashed);

dotfactor = 4;
dot(pic,Scale(pic,(-sqrt(3),-15*sqrt(3))), highlight_color);
  label(pic, "$(-\sqrt{3},-15\sqrt{3})$", Scale(pic,(-sqrt(3),-15*sqrt(3))), NW);
dot(pic,Scale(pic,(sqrt(3),15*sqrt(3))), highlight_color);
  label(pic, "$(\sqrt{3},15\sqrt{3})$", Scale(pic,(sqrt(3),15*sqrt(3))), SE);


xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=RightTicks(Step=1,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks(Step=10,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ============= (2x-4)/(x+2) ============
real f10(real x) {return (2x-4)/(x+2);}

picture pic;
int picnum = 10;
size(pic,0,4cm,keepAspect=true);
// scale(pic, Linear(10), Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-4; xright=4;
ybot=-4; ytop=4;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }


draw(pic, (-2,ybot)--(-2,ytop), dashed);
label(pic, "$x=-2$", (-2,-1), W);

draw(pic, (xleft,2)--(xright,2), dashed);
label(pic, "$y=2$", (3,2), N);

path grf_left = (-3,2.5)..(-3.25,2.25)..(-4,2.15); 
draw(pic, grf_left, highlight_color, Arrow);
path grf_right = (3,1.5)..(3.25,1.75)..(4,1.85); 
draw(pic, grf_right, highlight_color, Arrow);

path grf_top = (-2.5,3.5)..(-2.25,3.75)..(-2.1,4); 
draw(pic, grf_top, highlight_color, Arrow);
path grf_bot = (-1.5,-3.5)..(-1.75,-3.75)..(-1.9,-4); 
draw(pic, grf_bot, highlight_color, Arrow);

dotfactor = 4;
dot(pic, (2,0), highlight_color);
label(pic, "$(2,0)$", (2,0), S);
dot(pic, (0,-2), highlight_color);
label(pic, "$(0,-2)$", (0,-2), SE);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


//  ..... graph ........
picture pic;
int picnum = 11;
size(pic,0,5cm,keepAspect=true);
scale(pic, Linear(8), Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-4; xright=4;
ybot=-20; ytop=20;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }


draw(pic, Scale(pic,(-2,ybot))--Scale(pic,(-2,ytop)), dashed);
draw(pic, Scale(pic,(xleft,2))--Scale(pic,(xright,2)), dashed);
label(pic, "$y=2$", Scale(pic,(2.75,2)), NE);

draw(pic, graph(pic,f10, xleft-0.1, -2-0.4), highlight_color);
draw(pic, graph(pic,f10, -2+0.4, xright+0.1), highlight_color);

dotfactor = 4;
dot(pic, Scale(pic,(2,0)), highlight_color);
// label(pic, "$(2,0)$", Scale(pic,(2,0)), N);
dot(pic, Scale(pic,(0,-2)), highlight_color);
// label(pic, "$(0,-2)$", Scale(pic,(0,-2)), SE);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-2.75, ymax=ytop+2.75,
      p=currentpen,
      ticks=LeftTicks(Step=10,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ============= 3+7*e^{-0.2x} ============
real f12(real x) {return 3+7*exp(-0.2*x);}

picture pic;
int picnum = 12;
size(pic,0,4cm,keepAspect=true);
scale(pic, Linear, Linear(0.5));

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=0; ytop=15;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }


path grf_left = Scale(pic,(-2,12.5))..Scale(pic,(-2.25,13))..Scale(pic,(-3,15)); 
draw(pic, grf_left, highlight_color, Arrow);
path grf_right = Scale(pic,(2,4))..Scale(pic,(2.25,3.5))..Scale(pic,(3,3.15)); 
draw(pic, grf_right, highlight_color, Arrow);

draw(pic, Scale(pic,(xleft,3))--Scale(pic,(xright,3)), dashed);

dotfactor = 4;
dot(pic, Scale(pic,(0,10)), highlight_color);
label(pic, "$(0,10)$", Scale(pic,(0,10)), NW);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


//  ..... graph ........
picture pic;
int picnum = 13;
size(pic,0,6cm,keepAspect=true);
scale(pic, Linear(5), Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-5; xright=5;
ybot=0; ytop=20;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }


draw(pic, graph(pic,f12, xleft-0.1, xright+0.1), highlight_color);

dotfactor = 4;
dot(pic, Scale(pic,(0,10)), highlight_color);

draw(pic, Scale(pic,(xleft,3))--Scale(pic,(xright,3)), dashed);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-2.75, ymax=ytop+2.75,
      p=currentpen,
      ticks=LeftTicks(Step=10,step=1,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// =========== x^2-4x+3 ========
real f14(real x) {return x^2-4*x+3;}

picture pic;
int picnum = 14;
size(pic,0,5cm,keepAspect=true);
scale(pic, Linear, Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-1; xright=4;
ybot=-1; ytop=5;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(pic,f14, xleft+0.6, xright+0.1), highlight_color);

dotfactor = 4;
dot(pic, Scale(pic,(1,0)), highlight_color);
dot(pic, Scale(pic,(3,0)), highlight_color);
dot(pic, Scale(pic,(2,-1)), highlight_color);
label(pic, "$(2,-1)$", Scale(pic,(2,-1)), S);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// =========== -2x^2+5x-3 ========
real f15(real x) {return -2*x^2+5*x-3;}

picture pic;
int picnum = 15;
size(pic,0,5cm,keepAspect=true);
scale(pic, Linear, Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-0.8; xright=3;
ybot=-9; ytop=0;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(pic,f15, xleft-0.1, xright+0.1), highlight_color);
dotfactor = 4;
dot(pic, Scale(pic,(1,0)), highlight_color);
dot(pic, Scale(pic,(1.5,0)), highlight_color);
dot(pic, Scale(pic,(5/4,1/8)), highlight_color);
label(pic, "$(5/4,1/8)$", Scale(pic,(5/4,1/8)), NE);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.1, xmax=xright+0.25,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// =========== (2+x)/(3-x) ========
real f16(real x) {return (2+x)/(3-x);}

picture pic;
int picnum = 16;
size(pic,0,5cm,keepAspect=true);
scale(pic, Linear(2), Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-5; xright=7;
ybot=-10; ytop=10;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(pic, f16, xleft-0.1, 3-0.5), highlight_color);
draw(pic, graph(pic, f16, 3+0.5, xright+0.1), highlight_color);

draw(pic, Scale(pic,(xleft,-1))--Scale(pic,(xright,-1)), dashed);
draw(pic, Scale(pic,(3,ybot))--Scale(pic,(3,ytop)), dashed);

// dotfactor = 4;
// dot(pic, Scale(pic,(2,-1)), highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.25, xmax=xright+0.25,
      p=currentpen,
      ticks=RightTicks("%",Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks(Step=5,step=1,OmitTick(0),Size=2pt,size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");






// =========== 4e^{0.5x}-1 ========
real f17(real x) {return 4*exp(0.5x)-1;}

picture pic;
int picnum = 17;
size(pic,0,5cm,keepAspect=true);
scale(pic, Linear, Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-6; xright=2;
ybot=-1; ytop=10;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(pic, f17, xleft-0.1, xright+0.1), highlight_color);

draw(pic, (xleft-0.1,-1)--(xright+0.1,-1), dashed);
dotfactor = 4;
dot(pic, Scale(pic,(-2.77,0)), highlight_color);

// dotfactor = 4;
// dot(pic, Scale(pic,(2,-1)), highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks("%",Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=LeftTicks(Step=5,step=1,OmitTick(0),Size=2pt,size=1.5pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// =========== x^5-5x+7 ========
real f19(real x) {return x^5-5x+7;}

picture pic;
int picnum = 19;
size(pic,0,5cm,keepAspect=true);
scale(pic, Linear, Linear);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=2;
ybot=-20; ytop=20;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(pic, f19, xleft-0.1, xright+0.1), highlight_color);

// draw(pic, Scale(pic,(-2,ybot-0.1))--Scale(pic,(-2,ytop+0.1)), dashed);
// dotfactor = 4;
// dot(pic, Scale(pic,(0,log(4))), highlight_color);

// dotfactor = 4;
// dot(pic, Scale(pic,(2,-1)), highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks("%",Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// =========== 2x^3-3x^2-12x+12 ========
real f20(real x) {return 2*x^3-3*x^2-12*x+12;}

picture pic;
int picnum = 20;
scale(pic, Linear(4), Linear);
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-2; xright=3;
ybot=-10; ytop=20;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(pic, f20, xleft-0.1, xright+0.1), highlight_color);

// draw(pic, Scale(pic,(-2,ybot-0.1))--Scale(pic,(-2,ytop+0.1)), dashed);
dotfactor = 4;
dot(pic, Scale(pic,(-1,19)), highlight_color);
dot(pic, Scale(pic,(2,-8)), highlight_color);

// dotfactor = 4;
// dot(pic, Scale(pic,(2,-1)), highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks("%",Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-2.5, ymax=ytop+2.5,
      p=currentpen,
      ticks=LeftTicks(Step=10,step=5,OmitTick(0),Size=2pt,size=1pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// =========== x^4-6x^2 ========
real f21(real x) {return x^4-6*x^2;}

picture pic;
int picnum = 21;
scale(pic, Linear(6), Linear);
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-3; xright=3;
ybot=-10; ytop=35;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(pic, f21, xleft-0.1, xright+0.1), highlight_color);

dotfactor = 4;
dot(pic, Scale(pic,(0,f21(0))), highlight_color);
dot(pic, Scale(pic,(-sqrt(6),f21(-sqrt(6)))), highlight_color);
dot(pic, Scale(pic,(sqrt(6),f21(sqrt(6)))), highlight_color);
dot(pic, Scale(pic,(-sqrt(3),f21(-sqrt(3)))), highlight_color);
dot(pic, Scale(pic,(sqrt(3),f21(sqrt(3)))), highlight_color);


xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=5,step=1,OmitTick(0),Size=2pt,size=1pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// =========== x/(x-1)^2 ========
real f22(real x) {return x/(x-1)^2;}

picture pic;
int picnum = 22;
scale(pic, Linear(5), Linear);
size(pic,0,5cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
xleft=-5; xright=5;
ybot=-1; ytop=20;

// Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

draw(pic, graph(pic, f22, xleft-0.1, 1-0.2), highlight_color);
draw(pic, graph(pic, f22, 1+0.25, xright+0.1), highlight_color);

dotfactor = 6;
dot(pic, Scale(pic,(0,f22(0))), highlight_color);
dot(pic, Scale(pic,(-1,f22(-1))), highlight_color);
dot(pic, Scale(pic,(-2,f22(-2))), highlight_color);

draw(pic, Scale(pic,(1,ybot))--Scale(pic,(1,ytop)), dashed);


xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+0.75,
      p=currentpen,
      ticks=RightTicks(Step=1,step=0,OmitTick(0),Size=2pt),
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=LeftTicks(Step=5,step=1,OmitTick(0),Size=2pt,size=1pt),
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// // ==== local and absolute max ====
// picture pic;
// int picnum = 0;
// size(pic,0,4cm,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=5;
// ybot=0; ytop=5;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// path grph = (xleft-0.1,1){N}..(2,ytop){E}..(3.5,ytop-2){E}
// ..(4,ytop-1.5){E}..(xright+0.1,2){SE};


// draw(pic, grph, highlight_color);

// filldraw(pic, circle((2,ytop),0.05), highlight_color,  highlight_color);
// label(pic,  "$A$", (2,ytop), S, filltype=Fill(white));
// filldraw(pic, circle((3.5,ytop-2),0.05), highlight_color,  highlight_color);
// label(pic,  "$B$", (3.5,ytop-2), S, filltype=Fill(white));
// filldraw(pic, circle((4,ytop-1.5),0.05), highlight_color,  highlight_color);
// label(pic,  "$C$", (4,ytop-1.5), N, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// // ==== parabola ====
// real f1(real x) {return x^2;}

// picture pic;
// int picnum = 1;
// size(pic,4cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=-2; xright=2;
// ybot=0; ytop=4;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// draw(pic, graph(f1,xleft-0.1,xright+0.1), highlight_color);

// filldraw(pic, circle((0,0),0.05), highlight_color,  highlight_color);
// // label(pic,  "minimum", (0,0), SE, filltype=Fill(white));
// label(pic,  "$f(x)=x^2$", (0,3), W, filltype=Fill(white));

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

