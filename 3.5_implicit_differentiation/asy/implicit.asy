// implicit.asy

cd("../../asy");
import settexpreamble;
cd("");
settexpreamble();
cd("../../asy");
import jh;
cd("");

import settings;
settings.outformat="pdf";
settings.tex="pdflatex";  // for graphic command
settings.render=0;

unitsize(1cm);

string OUTPUT_FN = "implicit%03d";

import graph;



// ================ unit circle =======
picture pic;
int picnum = 0;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
real xleft=-2; xright=2;
real ybot=-2; ytop=2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path unit_circle = circle((0,0), 1);
draw(pic, unit_circle, highlight_color);

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .. tangent line ........
real tangent_line1(real x) {return -1*(x-(sqrt(2)/2))+(sqrt(2)/2);}

picture pic;
int picnum = 1;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
real xleft=-2; xright=2;
real ybot=-2; ytop=2;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path unit_circle = circle((0,0), 1);
draw(pic, unit_circle, black);

draw(pic, graph(tangent_line1,0.25,1.25), highlight_color);
filldraw(pic, circle((sqrt(2)/2,sqrt(2)/2),0.05), highlight_color, highlight_color);
label(pic,  "$(\sqrt{2}/2,\sqrt{2}/2)$", (sqrt(2)/2,sqrt(2)/2), NE, filltype=Fill(white));

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.75, xmax=xright+.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.75, ymax=ytop+0.75,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ================ arc of unit circle =======
picture pic;
int picnum = 2;

size(pic,0,4cm,keepAspect=true);

real xleft, xright, ybot, ytop; // limits of graph
real xleft=0; xright=1;
real ybot=0; ytop=1;

// Draw graph paper
for(real i=xleft; i <= xright; ++i) {
  if (abs(i)>.01) { 
    draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
  }
}
for(real j=ybot; j <= ytop; ++j) {
  if (abs(j)>.01) { 
    draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
  }
}

path unit_circle_arc = arc((0,0), 1, 20, 70);
draw(pic, unit_circle_arc, highlight_color);
filldraw(pic, circle((sqrt(2)/2,sqrt(2)/2),0.03), highlight_color, highlight_color);
label(pic,  "$(\sqrt{2}/2,\sqrt{2}/2)$", (sqrt(2)/2,sqrt(2)/2), 2*SW, filltype=Fill(white));

xaxis(pic, L="",  // label
      axis=YZero,
      xmin=xleft-0.5, xmax=xright+.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));

yaxis(pic, L="",  // label
      axis=XZero,
      ymin=ybot-0.5, ymax=ytop+0.5,
      p=currentpen,
      ticks=NoTicks,
      arrow=Arrows(TeXHead));
  
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");






// // ================ sin(x) =======
// real f0(real x) {return sin(x);}

// picture pic;
// int picnum = 0;

// size(pic,5cm,0,keepAspect=true);

// real xleft, xright, ybot, ytop; // limits of graph
// xleft=0; xright=7;
// ybot=-1; ytop=1;

// // Draw graph paper
// for(real i=xleft; i <= xright; ++i) {
//   if (abs(i)>.01) { 
//     draw(pic,(i,ybot-0.25)--(i,ytop+0.25),GRAPHPAPERPEN);
//   }
// }
// for(real j=ybot; j <= ytop; ++j) {
//   if (abs(j)>.01) { 
//     draw(pic,(xleft-0.25,j)--(xright+0.25,j),GRAPHPAPERPEN);
//   }
// }

// draw(pic, graph(f0,xleft-0.1,xright+0.1), highlight_color);

// // filldraw(pic, circle((1,6),0.05), highlight_color, highlight_color);
// // label(pic,  "$(a,f(a))$", (2,4), SE, filltype=Fill(white));

// real pi=3.14159;
// real[] T={pi/2, pi, 3*pi/2, 2*pi};

// xaxis(pic, L="",  // label
//   axis=YZero,
//   xmin=xleft-0.75, xmax=xright+.75,
//   p=currentpen,
//       ticks=RightTicks("%", T, Size=2pt),
//   arrow=Arrows(TeXHead));
// labelx(pic,"$\frac{\pi}{2}$",pi/2,S);
// labelx(pic,"{\tiny $\pi$}",pi,0.5*S);
// labelx(pic,"$\frac{3\pi}{2}$",3*pi/2,S);
// labelx(pic,"{\tiny$ 2\pi$}",2*pi,0.5*S);
  
// yaxis(pic, L="",  // label
//   axis=XZero,
//   ymin=ybot-0.75, ymax=ytop+0.75,
//   p=currentpen,
//   ticks=NoTicks,
//   arrow=Arrows(TeXHead));
  
// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

