\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title{Section 4.3\ \ L'H\^{o}pital's Rule}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................






\begin{frame}{Limits of fractions}
We have already looked at fraction limits, 
those of the form $\lim_{x\to k}n(x)/d(x)$.
We described ones where $n(x)\to 0$ and $d(x)\to 0$
as ``$0/0$~type.''
These are of that type.
\begin{equation*}
  \lim_{x\to 2} \frac{(x-2)^2}{x-2}
  \qquad
  \lim_{x\to 1^+} \frac{\ln(x)}{\sqrt{x-1}}
  \qquad
  \lim_{x\to \infty} \frac{e^{-x}}{x^{-3}}
\end{equation*}
We also described some limits as ``$\infty/\infty$~type''.
\begin{equation*}
  \lim_{x\to\infty}\frac{e^x}{x^2}
  \qquad
  \lim_{x\to 0^+}\frac{-\ln(x)}{x^{-2}}
\end{equation*}
Both of these types are \alert{indeterminate forms} 
in that we cannot tell the value of the limit
from the form.

We said we'd put off indeterminate forms for 
later.  
Later is now.
\end{frame}



\begin{frame}{A contest}\vspace*{-1ex}
Consider a fraction limit.
\begin{equation*}
  \lim_{x\to x_0}\frac{n(x)}{d(x)}
\end{equation*}
If $n(x)\to 0$ then that tends to bring the fraction as a whole
to zero.
And $d(x)\to 0$ tends to bring the fraction as a whole to infinity.
So in a $0/0$ limit there is a contest between the two.

Either can win.
There are $0/0$ limits that go to~$0$, 
there are $0/0$ limits that go to~$\infty$, 
and there are $0/0$ limits that go to any number in between
so the contest can end in a tie.
\begin{center}
  \begin{tabular}{cc|c}
    \multicolumn{1}{c}{$n(x)$}
       &\multicolumn{1}{c}{$d(x)$}
       &\multicolumn{1}{c}{$\lim_{x\to 0}n(x)/d(x)$}  \\ \hline
    \rule{0pt}{10pt}
    $x^2$  &$x$  &$0$  \\ 
    $x$  &$x^2$  &$\infty$  \\ 
    $5x$  &$x$  &$5$  \\ 
  \end{tabular}
\end{center}
That's why the $0/0$~type is called ``indeterminate.''

\pause
Similarly,
there are $\infty/\infty$ limits that go to~$0$, 
there are $\infty/\infty$ limits that go to~$\infty$, 
and there are $\infty/\infty$ limits that go to any number in between.
% \begin{center}
%   \begin{tabular}{cc|c}
%     \multicolumn{1}{c}{$n(x)$}
%        &\multicolumn{1}{c}{$d(x)$}
%        &\multicolumn{1}{c}{$\lim_{x\to 0}n(x)/d(x)$}  \\ \hline
%     \rule{0pt}{10pt}
%     $1/x$  &$1/x^2$  &$0$  \\ 
%     $1/x^2$  &$1/x$  &$\infty$  \\ 
%     $1/x$  &$1/5x$  &$5$  \\ 
%   \end{tabular}
% \end{center}
\end{frame}


\begin{frame}{Motivation: $0/0$-type limits}
Consider this one.
\begin{equation*}
  \lim_{x\to 0^+}\frac{x^2}{x}
\end{equation*}
Obviously, we could cancel the $x$'s to get $\lim_{x\to 0+}x=0$.
But we will instead pause to reflect on the relationship 
between the numerator and denominator.
\pause
\begin{center}\small
  \begin{tabular}{ll|r}
    \multicolumn{1}{c}{$d(x)=x$}
      &\multicolumn{1}{c}{$n(x)=x^2$}
      &\multicolumn{1}{c}{\textit{Ratio} $x^2/x$}  \\ \hline
    \rule{0em}{10pt}%
    $0.1$   &$0.01$       &$1/10$ \\
    $0.01$  &$0.000\,1$   &$1/100$ \\
    $0.001$ &$0.000\,001$ &$1/1\,000$ \\
  \end{tabular}
  \qquad
  \vcenteredhbox{\includegraphics{asy/lhopital000.pdf}}
\end{center}
The fraction as a whole goes to zero.
So in this contest the winner is the numerator,~$x^2\!$.
Both go to zero but $x^2$ is racing there ahead of~$x$.
\end{frame}


\begin{frame}{Wait a minute professor \ldots}
\begin{center}
  \includegraphics[height=0.3\textheight]{pix/professor.jpg}    
\end{center}
``The table starts 
at a height of $0.1$.
How can $n(x)$ `race ahead'?
Isn't there no room for a race?''

\pause
This is a place where the table of numbers tells the story better than the 
graph picture.
The table says:~first $x^2$ is at $1/10$-th the height.
Then it is at $1/100$-th the height, and
then $1/1000$-th.
They both go to zero but in a way such that the numerator
forever gains on the denominator. 
\begin{center}\small
  \begin{tabular}{ll|r}
    \multicolumn{1}{c}{$x$}
      &\multicolumn{1}{c}{$x^2$}
      &\multicolumn{1}{c}{\textit{Ratio} $x^2/x$}  \\ \hline
    \rule{0em}{10pt}
    $0.1$   &$0.01$       &$1/10$ \\
    $0.01$  &$0.000\,1$   &$1/100$ \\
    $0.001$ &$0.000\,001$ &$1/1\,000$ \\
  \end{tabular}
\end{center}
\end{frame}



\begin{frame}{Motivation: $\infty/\infty$}
Next consider this.
\begin{equation*}
  \lim_{x\to \infty}\frac{x^2}{x}
\end{equation*}
Again we could cancel the $x$'s to get $\lim_{x\to \infty}x=\infty$
but again we will instead reflect on the relationship 
between the two functions.
In a fraction, $n(x)\to\infty$ tends to make the fraction as a whole go to
infinity.
And $d(x)\to\infty$ tends to make the fraction go to zero.
\pause
\begin{center}\small
  \begin{tabular}{ll|r}
    \multicolumn{1}{c}{$d(x)=x$}
      &\multicolumn{1}{c}{$n(x)=x^2$}
      &\multicolumn{1}{c}{\textit{Ratio} $x^2/x$}  \\ \hline
    \rule{0em}{10pt}
    $10$     &$100$         &$10$ \\
    $100$    &$10\,000$     &$100$ \\
    $1\,000$ &$1\,000\,000$ &$1\,000$ \\
  \end{tabular}
\end{center}
In this contest, 
the winner is the numerator, $x^2\!$.
Both functions go to infinity but $x^2$ is racing ahead of~$x$.
(And with infinite limits there is no intuitive unease about `room'.)
\end{frame}




\begin{frame}{Picking a winner}
We have the intuition that these contests are
decided by comparing the rates of growth of $n(x)$ and $d(x)$.  
How to use the rate information, $n'(x)$ and $d'(x)$, to figure out the
limits?

\pause
\Ex
Here's a suggestive example:~take $n(x)=5x$ and $d(x)=x$.
Obviously $\lim_{x\to\infty}n(x)/d(x)=5$.
Obviously also $n'(x)=5$ and $d'(x)=1$.
At least in this one case we have this.
\begin{equation*}
  \lim_{x\to\infty}\frac{n(x)}{d(x)}
  =
  \lim_{x\to\infty}\frac{n'(x)}{d'(x)}
\end{equation*}
\end{frame}


\begin{frame}{L'H\^{o}pital's Rule}
Suppose that $n(x)$ and $d(x)$ are differentiable in some neighborhood of
the number~$a$, and that $d'(x)\neq 0$ in that interval
except possibly at~$a$.
If $\lim_{x\to a}n(x)=0$ and  $\lim_{x\to a}d(x)=0$ then 
\begin{equation*}
  \lim_{x\to a}\frac{n(x)}{d(x)}=\lim_{x\to a}\frac{n'(x)}{d'(x)}
  \tag{$*$}
\end{equation*}  
(provided that the latter exists).
Similarly, if 
$\lim_{x\to a}n(x)=\infty$ and  $\lim_{x\to a}d(x)=\infty$ then ($*$) holds.
This result also holds for one-sided limits $x\to a^+$ or $x\to a^-$,
and for infinite limits $x\to\infty$ or $x\to-\infty$. 


\Ex Find $\displaystyle \lim_{x\to -3}\frac{x^2-9}{x+3}$

\pause
This is an $0/0$~type limit.
\begin{equation*}
  \lim_{x\to -3}\frac{x^2-9}{x+3}
  =\lim_{x\to -3}\frac{2x}{1}
  =-6
\end{equation*}
% \pause
% Here is an argument for $0/0$, giving some sense of how derivatives
% can appear.  
% The derivative gives a linear approximation,
% $n(x)\approx n(a)+n'(x)\cdot (x-a)$
% and $d(x)\approx n(a)+d'(x)\cdot (x-a)$.
% Write the fraction and take the limit.
% \begin{equation*}
%   \lim_{x\to a}\frac{n(x)}{d(x)}
%   =
%   \lim_{x\to a}\frac{n(a)+n'(x)\cdot (x-a)}{d(a)+d'(x)\cdot (x-a)}
%   =
%   \lim_{x\to a}\frac{n'(x)(x-a)}{d'(x)(x-a)}
%   =
%   \lim_{x\to a}\frac{n'(x)}{d'(x)}
% \end{equation*}
\end{frame}


\begin{frame}{Practice}
\Ex Find $\displaystyle \lim_{x\to\infty}\frac{6x-7}{7x-6}$

\pause
This is an $\infty/\infty$~type limit.
\begin{equation*}
  \lim_{x\to\infty}\frac{6x-7}{7x-6}
  =\lim_{x\to\infty}\frac{6}{7}
  =6/7
\end{equation*}

\Ex This is a $0/0$-type limit:
$\displaystyle \lim_{x\to 4}\frac{x-4}{x^2-16}$. 

\pause
\begin{equation*}
  \lim_{x\to 4}\frac{x-4}{x^2-16}
  =
  \lim_{x\to 4}\frac{1}{2x}
  =1/8
\end{equation*}

\Ex This is another $0/0$-type limit. 
Note that it is not $x\to 0$, it is $x\to 1$.
\begin{equation*}
  \lim_{x\to 1}\frac{\ln x}{x-1}
  =
  \lim_{x\to 1}\frac{1/x}{1}
  =
  \lim_{x\to 1}\frac{1}{x}=1
\end{equation*}
\end{frame}

\begin{frame}{Multiple applications}
\Ex This is an $\infty/\infty$-type limit. 
\begin{equation*}
  \lim_{x\to \infty}\frac{e^x}{x^2}
  =
  \lim_{x\to \infty}\frac{e^x}{2x}
\end{equation*}
We are not done since that is also an $\infty/\infty$ limit.
Apply the rule again.
\begin{equation*}
  \lim_{x\to \infty}\frac{e^x}{2x}
  =
  \lim_{x\to \infty}\frac{e^x}{2}=\infty
\end{equation*}
\end{frame}


\begin{frame}{Don't be fooled!  Check the limit type}

This is wrong:
\begin{equation*}
  \lim_{x\to 1}\frac{\ln x}{x}
  =
  \lim_{x\to 1}\frac{1/x}{1}
\end{equation*}
The initial limit is of type $0/1$ so L'H\^{o}pital's Rule does not apply.  
Instead the answer by the Limit Laws is $0$.
\end{frame}


\begin{frame}{Practice}
Always check the type, and for some of them you may have to iterate
applying L'H\^{o}pital's Rule.
\begin{enumerate}
\item $\displaystyle \lim_{x\to -2} \frac{x^3+8}{x+2}$ 

\pause
\item $\displaystyle \lim_{x\to \infty} \frac{5-4x^3}{1+7x^3}$ 

\pause
\item $\displaystyle \lim_{x\to \infty} \frac{x+x^2}{1-2x^2}$ 

\pause
\item $\displaystyle \lim_{x\to 0} \frac{3x+1-e^{3x}}{x^2}$ 

\pause
\item $\displaystyle \lim_{x\to 0^-} \frac{\ln(1+x)}{\sqrt{x}}$ 

\pause
\item $\displaystyle \lim_{x\to 3} \frac{x^3+3x^2-x-3}{x^2+6x+9}$ 

\end{enumerate}
\end{frame}



% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 
