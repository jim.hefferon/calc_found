\documentclass[10pt,t,serif,professionalfont]{beamer}
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\PassOptionsToPackage{usenames,dvipsnames}{color}
\usepackage{../../presentation}

\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\addheadbox{filler}{\ }  % create extra space at top of slide 
%\setbeameroption{show notes}
% \setbeamertemplate{background canvas}[vertical shading][bottom=red!20,top=yellow!30]
\hypersetup{colorlinks=true,linkcolor=blue} 

\title[Limits] % (optional, use only with long paper titles)
{Section 2.2\ \ Infinite limits}

\author{J Hef{}feron}
\institute{
  Mathematics and Statistics\\
  University of Vermont\\[1ex]
  \texttt{James.Hefferon@uvm.edu}
}
\date{}

\usepackage{siunitx}

\subject{Limits}
% This is only inserted into the PDF information catalog. Can be left
% out. 

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

%........................................................

\section{Vertical infinite limits}


\begin{frame}{Graphs of $1/(x-2)$ and $1/(x-2)^2$}
\begin{center}
  \vcenteredhbox{\includegraphics{asy/infinite_limits000.pdf}}%    
  \hspace*{1cm}
  \vcenteredhbox{\includegraphics{asy/infinite_limits001.pdf}}%    
\end{center}
On the left we say
$\lim_{x\to 2^+}f(x)=\infty$.
In this class we take `$\infty$' as an abbreviation for 
``does not exist, because it grows positively without bound.''
We also say $\lim_{x\to 2^-}f(x)=-\infty$. 
Overall, the limit $\lim_{x\to 2}f(x)$ does not exist.

On the right, both $\lim_{x\to 2^-}f(x)=\infty$ 
and
$\lim_{x\to 2^+}f(x)=\infty$.
So we say
$\lim_{x\to 2}f(x)=\infty$.
\end{frame}


\begin{frame}{Vertical asymptote}
The vertical line $x=a$ is a 
\alert{vertical asymptote}
for the graph of~$f$ if 
as $x$ approaches~$a$ (from either above or below), 
then either $f$ approaches~$+\infty$ or~$-\infty$.   
\begin{center}
  \vcenteredhbox{\includegraphics{asy/infinite_limits002.pdf}}%    
  \hspace*{1cm}
  \vcenteredhbox{\includegraphics{asy/infinite_limits003.pdf}}%    
\end{center}
\end{frame}




\begin{frame}{Multiple vertical asymptotes}
Functions can have more than one vertical asymptote.
This function has two.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/infinite_limits005.pdf}}% 
  \qquad
  \begin{minipage}{0.4\linewidth}
    \begin{equation*}
      f(x)=\frac{1}{(x-1)(x-3)^2}
    \end{equation*}
  \end{minipage}
\end{center}  
\end{frame}



\begin{frame}{Finding vertical asymptotes}
Polynomial functions do not have vertical asymptotes.

For rational functions $f(x)=n(x)/d(x)$, there is a vertical asymptote where
the numerator is nonzero but the denominator is zero.

If both the numerator and denominator are zero then this is an 
\alert{indeterminate form} and we will have to do more thinking.
 
\pause
Find the vertical asymptotes.
\begin{enumerate}
\item $\displaystyle \frac{3x+2}{x-4}$

\pause
There is a vertical asymptote at $4$.
\item $\displaystyle \frac{x}{x^2-4}$

\pause
There is a vertical asymptote at $+2$ and $-2$.
\item $\displaystyle \frac{5x}{(x-4)^2}$

\pause
There is a vertical asymptote at $4$.
\end{enumerate}
\end{frame}


\begin{frame}{Difference between DNE and $\pm\infty$}

On the left the limit as $x\to 1$ does not exist.
There is no infinity involved.
\begin{center}
  \vcenteredhbox{\includegraphics{asy/infinite_limits004.pdf}}% 
  \qquad   
  \vcenteredhbox{\includegraphics{asy/infinite_limits001.pdf}}%    
\end{center}
On the right we say $\lim_{x\to 2^{-}}f(x)=\infty$.
The ``$\infty$'' is an improvement over a plain DNE because it
adds information.  
\end{frame}




\section{Horizontal infinite limits}
\begin{frame}{Limits at infinity}
This is $f(x)=x^3$.  
\begin{center}
  \vcenteredhbox{\includegraphics{asy/infinite_limits006.pdf}}%    
\end{center}
As the input $x$ grows without bound, so does $f(x)$.
As $x$ gets more and more negative, $f(x)$ also gets more and more negative.
We write \alert{$x\to\infty$}, as in $\lim_{x\to\infty}f(x)=\infty$
and $\lim_{x\to-\infty}f(x)=-\infty$.
\end{frame}

\begin{frame}
This is $f(x)=x^2$.  
\begin{center}
  \vcenteredhbox{\includegraphics{asy/infinite_limits007.pdf}}%    
\end{center}
We write $\lim_{x\to\infty}f(x)=\infty$
and $\lim_{x\to-\infty}f(x)=\infty$.
\end{frame}

\begin{frame}{The horizontal limit could be finite}
These are $f(x)=1/x$ and $g(x)=(3x^2+1)/(2x^2-1)$.  
\begin{center}
  \vcenteredhbox{\includegraphics{asy/infinite_limits008.pdf}}%    
  \qquad
  \vcenteredhbox{\includegraphics{asy/infinite_limits009.pdf}}%    
\end{center}
We have $\lim_{x\to\infty}f(x)=0$ on the left
and $\lim_{x\to\infty}g(x)=3/2$ on the right.

A \alert{horizontal asymptote} is a horizontal line $y=k$ where the graph 
of the function approaches that line as $x\to\infty$ or  $x\to-\infty$. 
\end{frame}



\begin{frame}{Finding horizontal asymptotes: leading term}
The \alert{leading term}
of the polynomial $a_nx^n+a_{n-1}x^{n-1}+\cdots+a_1x+a_0$ is 
the one associated with the largest power, $a_mx^m$.

The leading term of $4x^3-3x+5x+6$ is $4x^3$.

\pause
Find the leading term of each.
\begin{enumerate}
\item $-x^5+2x^3+9x$
\pause
\item $10-x^6+7x^3$
% \pause
% \item $5x+x^3-8x^2$
\end{enumerate}
\end{frame}


\begin{frame}{How polynomials behave for large inputs}
What happens to a polynomial as $x\to\infty$ or $x\to-\infty$ depends only 
on the leading term.

If $p(x)$ is a polynomial and $\ell(x)$ is its leading term
then $\lim_{x\to\infty}p(x)=\lim_{x\to\infty} \ell(x)$.
The same holds for negative infinity.
  

\pause
Find each.
\begin{enumerate}
\item $\lim_{x\to\infty}-x^5+2x^3+9x$
\pause
\item $\lim_{x\to\infty}10-x^6+7x^3$
\pause
\item $\lim_{x\to-\infty}-x^5+2x^3+9x$\ \ (Notice the $-\infty$.)
% \pause 
% \item $\lim_{x\to-\infty}10-x^6+7x^3$
\end{enumerate}
\end{frame}



\begin{frame}{How rational functions behave for large inputs}
As with polynomials, for a fraction of two polynomials,
pay attention only to the
leading terms.

There are three cases and we do the same in all three.
The first is that the numerator has a higher degree than the denominator.
\begin{equation*}
  \lim_{x\to\infty}\frac{x^3-2x+5}{2x^2+14}
  =\lim_{x\to\infty}\frac{x^3}{2x^2}  
\end{equation*}
Cancel the $x$'s.
\begin{equation*}
 =\lim_{x\to\infty}\frac{x}{2}=\infty  
\end{equation*}
Because it is not a finite limit, there is no horizontal asymptote.

\pause
The second case 
is that the degree of the numerator is less than the degree of the
denominator.
\begin{equation*}
  \lim_{x\to\infty}\frac{5x^2+3x}{x^3+9}
  =\lim_{x\to\infty}\frac{5x^2}{x^3}  
  =\lim_{x\to\infty}\frac{5}{x}  
  =0
\end{equation*}
The line $y=0$ is a horizontal asymptote.
\end{frame}


\begin{frame}
The third case is that numerator and denominator have the same degree.
\begin{equation*}
  \lim_{x\to\infty}\frac{2x^2-5}{x^2+4}
  =\lim_{x\to\infty}\frac{2x^2}{x^2}  
  =\lim_{x\to\infty}\frac{2}{1}
  =2  
\end{equation*}
The line $y=2$ is a horizontal asymptote.

\pause\medskip
Find each.
\begin{enumerate}
\item $\displaystyle\lim_{x\to\infty}\frac{3x^3-x}{-x^2}$
\pause
\item $\displaystyle\lim_{x\to\infty}\frac{x}{x^2-4}$
\pause
\item $\displaystyle\lim_{x\to\infty}\frac{-3x^5+1}{4x-2x^5}$
\end{enumerate}

\end{frame}



% ---------------------
% \begin{frame}
% \frametitle{}
% \end{frame}
\end{document}
